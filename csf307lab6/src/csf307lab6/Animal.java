package csf307lab6;

import java.util.Collections;
import java.util.List;

public class Animal {
	private final String type;
	private final int age;
	private final List<Animal> descendants;

	public Animal(String type, int age, List<Animal> descendants) {
		this.type = type;
		this.age = age;
		this.descendants = descendants;
	}

	public String getType() {
		return type;
	}

	public int getAge() {
		return age;
	}

	public List<Animal> getDescendants() {
		return Collections.unmodifiableList(descendants);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + ((descendants == null) ? 0 : descendants.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Animal other = (Animal) obj;
		if (age != other.age)
			return false;
		if (descendants == null) {
			if (other.descendants != null)
				return false;
		} else if (!descendants.equals(other.descendants))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

}
