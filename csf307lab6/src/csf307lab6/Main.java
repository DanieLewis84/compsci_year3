package csf307lab6;

import java.util.HashSet;
import java.util.Set;

public class Main {
	public static void main(String[] args) {
		Set<Animal> animals = new HashSet<>();
		Animal aardvark=new Animal("Aardvark",12,null);
		Animal baboon=new Animal("Baboon",32,null);
		animals.add(aardvark);
		animals.add(baboon);
		System.out.println("animals "+(animals.contains(aardvark)?"does":"doesn't")+" contain aardvark.");
		animals.remove(aardvark);
		System.out.println("animals "+(animals.contains(aardvark)?"does":"doesn't")+" contain aardvark.");
		
		
		EvilHashCode evil = new EvilHashCode(1);
		Set<EvilHashCode> evilHash = new HashSet<>();
		evilHash.add(evil);
		System.out.println("evilHash "+(evilHash.contains(evil)?"does":"doesn't")+" contain evil.");
		evil.setVariable(666);
		System.out.println("evilHash "+(evilHash.contains(evil)?"does":"doesn't")+" contain evil.");
		evilHash.add(new EvilHashCode(12));
	}
}
