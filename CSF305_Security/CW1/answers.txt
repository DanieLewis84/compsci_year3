%% Please ensure all answers are written on one line using the following format.
%%
%% Question X
%% ----------
%% Answer(Plaintext): your answer here
%%
%% Do not change the format of this file as it will be automatically checked by a program.

Your Full Name: Daniel Lewis
Student Number: 223851


Question 1
----------
Chosen Plaintext: Daniel Lewis likes to eat apples and oranges but not watermelons
Answer(Ciphertext): LIVQMT TMEQA TQSMA BW MIB IXXTMA IVL WZIVOMA JCB VWB EIBMZUMTWVA


Question 2
----------
Answer(Plaintext): CENTURION HIPPOPOTAMUS TO ATTACK GAULISH VILLAGE AT DUSK


Question 4
----------
Answer(Ciphertext): AEKAERBWEDLIELOSTMESTPNGNENIISAANTTOLLOPDEORSNWETSAUAL


Question 5
----------
Answer(Plaintext): YOUAREGOINGTONEEDABIGGERBOAT


Question 6
----------
Answer(Ciphertext): SZCDJWBJVDMWDCXGLMTZJNOHBJNNTFUISHGXGNJRTMRASRJDTWMHM


Question 8
----------
Answer(Plaintext): THE BRAIN IS A WONDERFUL ORGAN; IT STARTS THE MINUTE YOU GET UP IN THE MORNING AND DOES NOT STOP UNTIL YOU GET TO CLASS


Question 10
----------
Answer(Plaintext): secret