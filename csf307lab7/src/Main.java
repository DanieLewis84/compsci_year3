import java.lang.reflect.Field;

public class Main {

	public static void main(String[] args) {
		// Task 1
		BigSecret bigSecret=new BigSecret();
		Class<?> bigSecretClass = bigSecret.getClass();
		long secret=1;
		Field fieldLocalSecretCode;
		try {
			fieldLocalSecretCode = bigSecretClass.getDeclaredField("localSecretCode");
			fieldLocalSecretCode.setAccessible(true);
			secret= (long) fieldLocalSecretCode.get(bigSecret);
			
		} catch (ReflectiveOperationException e) {
			e.printStackTrace();
		} 
		System.out.println("The big secret is:"+bigSecret.unlockAndGetMessage(secret));
		
		// Task 2
		Field ultimateKeyLenField;
		Field ultimateKeyField;
		int ultimateKeyLength=0;
		byte[] ultimateKey;
		try {
			ultimateKeyLenField = bigSecretClass.getDeclaredField("ULTIMATE_SECRET_LENGTH");
			ultimateKeyLenField.setAccessible(true);
			ultimateKeyLength= (int) ultimateKeyLenField.get(bigSecret);
			
			ultimateKeyField = bigSecretClass.getDeclaredField("ultimateKey");
			ultimateKeyField.setAccessible(true);
			ultimateKey=(byte[]) ultimateKeyField.get(bigSecret);
			for (int i=0;i<ultimateKeyLength;i++) {
				ultimateKey[i]=0;
			}
			
		} catch (ReflectiveOperationException e) {
			e.printStackTrace();
		} 
		System.out.println("The ultimate secret is:"+bigSecret.decodeAndGetUltimateSecret());
		
		//Task 3
		
	}

}
