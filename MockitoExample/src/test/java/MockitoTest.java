import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

public class MockitoTest {

	@Mock
	FetchPriceDatabase fetchDataMock;
	@Mock
	CoffeeMachine coffeeMachineMock;
	@Mock
	BasicCoffeeMachine basicCoffeeMachineMock;	
	@Mock
	AldisOverpricedMachine aldisOverpricedMachine;
	
	@Rule 
	public MockitoRule mockitoRule = MockitoJUnit.rule();
	
	@Test
	public void testFetchDatabase() {
		CoffeeApp t = new CoffeeApp(coffeeMachineMock, fetchDataMock);
		String check = t.prepareCoffee();
		assertEquals("Coffee is ready",check);
		verify(fetchDataMock).fetchData();
	}
	
	@Test
	public void testBrew() {
		CoffeeApp t = new CoffeeApp(coffeeMachineMock, fetchDataMock);
		String check = t.prepareCoffee();
		assertEquals("Coffee is ready",check);
		verify(coffeeMachineMock).brew();
	}
	
	@Test
	public void testBrewBasicCoffee() {
		CoffeeApp t = new CoffeeApp(basicCoffeeMachineMock, fetchDataMock);
		String check = t.prepareCoffee();
		assertEquals("Coffee is ready",check);
		verify(basicCoffeeMachineMock).brew();
	}
	
	@Test
	public void testBrewAldiCoffee() {
		CoffeeApp t = new CoffeeApp(aldisOverpricedMachine, fetchDataMock);
		String check = t.prepareCoffee();
		assertEquals("Coffee is ready",check);
		verify(aldisOverpricedMachine).brew();
	}
	
	
}
