using Dapper;
using SD.Core.DAL;
using StudentPortal.Core.DomainModels;
using StudentPortal.Core.Interfaces.Repositories;
using StudentPortal.Infrastructure.Data.Connections;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StudentPortal.Infrastructure.Data.Repositories
{
    public class StudentQuestionAnswerRepository : IStudentQuestionAnswerRepository
    {
        private readonly IUnitOfWork<IntuitConnection> _sitsUow;

        public StudentQuestionAnswerRepository(IUnitOfWork<IntuitConnection> sitsUow)
        {
            _sitsUow = sitsUow;
        }

        private string GetAnswersTableName(StudentQuestionAnswerType type)
        {
            switch (type)
            {
                case StudentQuestionAnswerType.CONSENT:
                case StudentQuestionAnswerType.DECLARATION:
                    return "ENROL_DECLARATION_ANSWERS";
                case StudentQuestionAnswerType.WORK_EXPERIENCE:
                    return "ENROL_WORK_EXP_ANSWERS";
                case StudentQuestionAnswerType.CAREERS_POSITION:
                    return "ENROL_CAREER_REG_ANSWERS";
                default:
                    throw new ArgumentException($"GetAnswersTableName -> couldn't find table name for StudentQuestionAnswerType of {type}");
            };
        }

        private string GetQuestionsTableName(StudentQuestionAnswerType type)
        {
            switch (type)
            {
                case StudentQuestionAnswerType.CONSENT:
                case StudentQuestionAnswerType.DECLARATION:
                    return "ENROL_DECLARATION_QUESTIONS";
                case StudentQuestionAnswerType.WORK_EXPERIENCE:
                    return "ENROL_WORK_EXP_QUESTIONS";
                case StudentQuestionAnswerType.CAREERS_POSITION:
                    return "ENROL_CAREER_REG_QUESTIONS";
                default:
                    throw new ArgumentException($"GetQuestionsTableName -> couldn't find table name for StudentQuestionAnswerType of {type}");
            };
        }

        private IEnumerable<StudentQuestionAnswer> GetStudentQuestionAnswers(StudentQuestionAnswerType questionType, string scjCode, string academicYear, string language, DateTime declarationsAtDate)
        {
            var questionTableName = GetQuestionsTableName(questionType);
            var answerTableName = GetAnswersTableName(questionType);
            var showRequired = questionType == StudentQuestionAnswerType.DECLARATION;
            var sql = $@"
SELECT
    EDA.SCJ_CODE      StudentCourseJoinCode,
    EDA.AYR           AcademicYear,
    EDQ.ENTRY_NAME    QuestionCode,
    EDQ.ENTRY_DESC    QuestionLabel,
    EDQ.LANG          Language,
    EDA.LAST_MODIFIED DateSelected,
    EDQ.REQUIRED      Required
FROM
    STUDENT_PROFILE.{questionTableName} EDQ
LEFT JOIN
    STUDENT_PROFILE.{answerTableName} EDA
    ON (EDQ.ENTRY_NAME=EDA.ENTRY_NAME)
        AND (EDQ.LANG=EDA.LANG)
        AND (EDQ.END_DATE IS NULL OR EDA.LAST_MODIFIED BETWEEN EDQ.START_DATE AND EDQ.END_DATE)
        AND EDA.SCJ_CODE = :scjCode
        AND EDA.AYR = :academicYear
WHERE
    EDQ.LANG = :language AND
    (
        (:declarationsAtDate BETWEEN EDQ.START_DATE AND EDQ.END_DATE) OR
        (EDQ.START_DATE < :declarationsAtDate AND EDQ.END_DATE IS NULL)
    )
    {(showRequired ? " AND EDQ.REQUIRED = 'Y'" : "")}
    {(!showRequired ? " AND EDQ.REQUIRED <> 'Y'" : "")}
ORDER BY
    EDQ.LANG,
    LENGTH(EDQ.ENTRY_NAME),
    EDQ.ENTRY_NAME
";
            var res = _sitsUow.Cnn.Query<StudentQuestionAnswer>(
                sql, 
                new { scjCode, academicYear, language, declarationsAtDate }, 
                _sitsUow.Transaction
            );
            
            return res;
        }

        public IEnumerable<string> GetStudentAnswers(StudentQuestionAnswerType questionType, string scjCode, string academicYear, string language, DateTime declarationsAtDate)
        {
            var data = GetStudentQuestionAnswers(questionType, scjCode, academicYear, language, declarationsAtDate);

            return data.Where(x => x.DateSelected != null).Select(x => x.QuestionCode);
        }

        public IEnumerable<StudentQuestionAnswer> GetStudentQuestions(StudentQuestionAnswerType questionType, string language, DateTime declarationsAtDate)
        {
            var data = GetStudentQuestionAnswers(questionType, scjCode: "", academicYear: "", language, declarationsAtDate);
            return data;
        }

        public bool DeleteStudentAnswers(StudentQuestionAnswerType questionType, string scjCode, string academicYear, string language, DateTime declarationsAtDate)
        {
            var answerTableName = GetAnswersTableName(questionType);
            var answers = GetStudentAnswers(questionType, scjCode, academicYear, language, declarationsAtDate);
            var sql = $@"
DELETE FROM STUDENT_PROFILE.{answerTableName}
WHERE SCJ_CODE=:scjCode AND AYR=:academicYear AND LANG=:language AND ENTRY_NAME=:questionCode
";
            _sitsUow.Cnn.Execute(sql, answers.Select(x => new { scjCode, academicYear, language, QuestionCode = x }), _sitsUow.Transaction);
            return true;
        }

        public bool InsertStudentAnswers(StudentQuestionAnswerType questionType, string scjCode, string academicYear, string language, IEnumerable<string> codes, DateTime dateOfAcceptance)
        {
            if(codes == null)
            {
                return false;
            }

            var answerTableName = GetAnswersTableName(questionType);
            var sql = $@"
INSERT INTO
    STUDENT_PROFILE.{answerTableName} 
(
    SCJ_CODE, 
    AYR, 
    ENTRY_NAME, 
    LAST_MODIFIED, 
    LANG
)
VALUES 
(
    :scjCode, 
    :academicYear, 
    :QuestionCode, 
    :dateOfAcceptance, 
    :language
)
";
            codes = codes.Where(x => !string.IsNullOrWhiteSpace(x));
            var result = _sitsUow.Cnn.Execute(sql, codes.Select(x=> new { scjCode, academicYear, language, dateOfAcceptance, QuestionCode  = x}), _sitsUow.Transaction) > 0;

            var shouldUpdateAcceptedRegulations =
                new[] { StudentQuestionAnswerType.CONSENT, StudentQuestionAnswerType.DECLARATION }
                .Contains(questionType);

            if (result && shouldUpdateAcceptedRegulations)
            {
                UpdateRegulationsAcceptedDate(scjCode, academicYear, dateOfAcceptance);
            }

            return result;
        }

        private void UpdateRegulationsAcceptedDate(string scjCode, string academicYear, DateTime dateOfAcceptance)
        {
            var updateEnrolmentRecordSql = @"
UPDATE
    INTUIT.SRS_SCE 
SET
    SCE_AREG='Y', 
    SCE_REGD=:dateOfAcceptance
WHERE
    SCE_SCJC=:scjCode 
    AND SCE_AYRC=:academicYear
";
            _sitsUow.Cnn.Execute(
                     updateEnrolmentRecordSql,
                     new
                     {
                         scjCode,
                         academicYear,
                         dateOfAcceptance,
                     },
                     _sitsUow.Transaction
                 );
        }
    }
}
