package csf303lab7;

public class Main {
	public  static  void  main ( String [ ]  args ){
		double amount = 1000;
		double taxPercentage = 65;
		double bills = 400;
		System.out.println("Gross Pay: " + amount ) ;
		double afterTax = deductTax (amount ,  taxPercentage ) ;
		System.out.println ("Net Pay: " + afterTax ) ;
		if ( afterTax<bills ){
			double  loan = getLoan ( afterTax ,  bills ) ;
			System.out.println("You need a  loan  of :  " + loan);
		} else {
			System.out.println("Bills are covered") ;
		}
	}
	
	public  static  double  deductTax ( double amount ,  double  taxPercentage ){
		double  total = amount-(amount/100*taxPercentage ) ;
		return  total ;
	}
	
	public  static  double  getLoan ( double  net ,  double  bills ){
		double  loanAmount = bills-net ;
		return  loanAmount ;
	}
}
