import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class DES {

	static Cipher cipher;
	
	public DES() throws Exception {
		cipher = Cipher.getInstance("DES");
	}
	
	public SecretKey generateRandomKey() throws NoSuchAlgorithmException {
		KeyGenerator keyGenerator = KeyGenerator.getInstance("DES");
		SecretKey secretKey = keyGenerator.generateKey();
		return secretKey;
	}
	
	public String encrypt(String plaintext, SecretKey secretKey) throws Exception {
		byte[] plaintextByte = plaintext.getBytes();
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		byte[] ciphertextByte = cipher.doFinal(plaintextByte);
		Base64.Encoder encoder = Base64.getEncoder();
		String ciphertext = encoder.encodeToString(ciphertextByte);
		return ciphertext;
	}
	
	public String decrypt(String ciphertext, SecretKey secretKey) throws Exception {
		Base64.Decoder decoder= Base64.getDecoder();
		byte[] ciphertextByte = decoder.decode(ciphertext);
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		byte[] plaintextByte = cipher.doFinal(ciphertextByte);
		String plaintext = new String(plaintextByte);
		return plaintext;
	}
	
}
