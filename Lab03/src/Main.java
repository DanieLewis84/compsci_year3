

import javax.crypto.SecretKey;

public class Main {

	public static void main(String[] args) {
		String plaintext = "Hello";
		String password = "swansea";
		try {
			AES aes = new AES();
			SecretKey keyFromPassword = aes.generateKeyFromPassword(password);
			String ciphertext = aes.encrypt(plaintext,keyFromPassword);
			System.out.println("Ciphertext: "+ciphertext);
			String message = aes.decrypt(ciphertext,  keyFromPassword);
			System.out.println("Plaintext: "+message);
		}
		catch (Exception e)
		{
			System.out.println("Exception occured");
		}
		String filePath1="plane.jpg";
		String filePath2="ship.jpg";
		try {
			MDFive mdFive = new MDFive();
			String file1Digest = mdFive.computeDigest(filePath1);
			String file2Digest = mdFive.computeDigest(filePath2);
			System.out.println("Plane: " + file1Digest);
			System.out.println("Ship: " + file2Digest);
		}
		catch (Exception e)
		{
			System.out.println("Exception occured");
		}
	}

}
