import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;

public class MDFive {
	public String computeDigest(String filePath) throws Exception {
		MessageDigest md = MessageDigest.getInstance("MD5");
		Path path = Paths.get(filePath);
		byte[] data = Files.readAllBytes(path);
		byte[] mdBytes = md.digest(data);
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < mdBytes.length; i++){
			sb.append(String.format("%02x", mdBytes[i]));
		}
		return sb.toString();
	}
}