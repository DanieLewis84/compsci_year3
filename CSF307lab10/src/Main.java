import java.util.WeakHashMap;

public class Main {
	public static void main(String[] args) {
		WeakHashMap<Integer, Integer> squares = new WeakHashMap<Integer, Integer>();
		for (int i = 0; i < 30000; i++) {
			squares.put(i, i * i);
		}
		System.out.printf("%d\n",squares.size());
		for (int i = 0; i < 30000; i=i+999) {
			System.out.printf("%d^2=%d\n",i,squares.get(i));
		}
	}
}
