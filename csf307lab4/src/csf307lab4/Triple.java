package csf307lab4;

public class Triple<A extends Number, B extends Number, C extends Number> implements Comparable<Triple<?,?,?>> {
	private A one;
	private B two;
	private C three;

	public Triple(A one, B two, C three) {
		this.one = one;
		this.two = two;
		this.three = three;
	}

	public A getone() {
		return one;
	}

	public B gettwo() {
		return two;
	}

	public C getthree() {
		return three;
	}

	public void setone(A one) {
		this.one = one;
	}

	public void settwo(B two) {
		this.two = two;
	}

	public void setthree(C three) {
		this.three = three;
	}

	public double maxProductTwist() {
		double x = one.doubleValue()*two.doubleValue()+three.doubleValue();
		double y = one.doubleValue()+two.doubleValue()*three.doubleValue();
		double z = one.doubleValue()*three.doubleValue()+two.doubleValue();
		x=Math.max(x, y);
		x=Math.max(x, z);
		return x;
	}

	@Override
	public int compareTo(Triple<?,?,?> comparitor) {
		double comp=this.maxProductTwist()-comparitor.maxProductTwist();
		if (comp>0) return 1;
		if (comp<0) return -1;
		return 0;
	}
}
