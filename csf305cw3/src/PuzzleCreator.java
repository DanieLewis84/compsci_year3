import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

/**
 * @author Dan Lewis - 223851
 *
 */
public class PuzzleCreator {

	private ArrayList<Puzzle> puzzles;
	private Random randomGenerator;
	private static final int KEY_VARIABLE_DIGITS = 2;

	/* The number of Puzzles that will be created by this class */ 
	public static final int PUZZLES_TO_CREATE = 4096;
	/* The encryption algorithm to be used for encrypting the puzzles prior to sending */ 
	public static final String ENCRYPTION_ALGORITHM = "DES";

	/**
	 * Constructor for the PuzzleCreator object
	 */
	public PuzzleCreator() {
		this.puzzles = new ArrayList<Puzzle>();
		this.randomGenerator = new Random();
	}

	/**
	 * @return A list of all Puzzles created by this method
	 */
	public ArrayList<Puzzle> createPuzzles() {
		this.puzzles = new ArrayList<Puzzle>();
		try {
			for (int puzzleNumber = 1; puzzleNumber <= PUZZLES_TO_CREATE; puzzleNumber++) {
				SecretKey secretKey = CryptoLib.createKey(createRandomKey());
				Puzzle puzzle = new Puzzle(puzzleNumber, secretKey);
				this.puzzles.add(puzzle);
			}
			return this.puzzles;
		} catch (NoSuchAlgorithmException | InvalidKeyException | InvalidKeySpecException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * @return a byte array containing a random key, suitable for encoding a Puzzle object.
	 */
	public byte[] createRandomKey() {
		byte[] randomBytes = new byte[KEY_VARIABLE_DIGITS];
		this.randomGenerator.nextBytes(randomBytes);
		byte[] key = new byte[Puzzle.BYTE_ARRAY_KEY_LENGTH];
		// copy randomBytes into the key 
		System.arraycopy(randomBytes, 0, key, 0, KEY_VARIABLE_DIGITS);
		return key;
	}

	/**
	 * @param key    the key to encrypt the puzzle with
	 * @param puzzle the puzzle to be encrypted
	 * @return the encrypted cipher-text representing the puzzle 
	 */
	public byte[] encryptPuzzle(byte[] key, Puzzle puzzle) {
		try {
			SecretKey secretKey = CryptoLib.createKey(key);
			Cipher cipher = Cipher.getInstance(ENCRYPTION_ALGORITHM);

			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			byte[] ciphertextByte = cipher.doFinal(puzzle.getPuzzleAsBytes());
			return ciphertextByte;
		} catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | IllegalBlockSizeException
				| BadPaddingException | InvalidKeySpecException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * @param fileName
	 */
	public void encryptPuzzlesToFile(String fileName) {
		try {
			final FileOutputStream fileOutputStream = new FileOutputStream(fileName);
			Collections.shuffle(this.puzzles);
			for (Puzzle puzzle : this.puzzles) {
				byte[] ciphertext = encryptPuzzle(createRandomKey(), puzzle);
				fileOutputStream.write(ciphertext);
			}
			fileOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param puzzleNumber
	 * @return
	 */
	public SecretKey findKey(final int puzzleNumber) {
		// Have to search, as list may be shuffled.
		for (Puzzle puzzle : this.puzzles) {
			if (puzzle.getPuzzleNumber() == puzzleNumber) {
				return puzzle.getKey();
			}
		}
		return null;
	}
}
