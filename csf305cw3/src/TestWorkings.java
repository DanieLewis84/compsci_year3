
import static org.junit.Assert.*;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import org.junit.Test;

public class TestWorkings {

	@Test
	public void ZeroBit() {
		try {
			KeyGenerator keyGenerator = KeyGenerator.getInstance("DES");
			SecretKey secretKey = keyGenerator.generateKey();
			Puzzle puzzle0 = new Puzzle(4095, secretKey);
			byte[] pb = puzzle0.getPuzzleAsBytes();
			assertTrue(pb[0] == 0);
		} catch (Exception e) {
			fail("Exception thrown:" + e.getMessage());
		}
	}

	@Test
	public void Bit15() {
		try {
			KeyGenerator keyGenerator = KeyGenerator.getInstance("DES");
			SecretKey secretKey = keyGenerator.generateKey();
			Puzzle puzzle0 = new Puzzle(4095, secretKey);
			byte[] pb = puzzle0.getPuzzleAsBytes();
			assertTrue(pb[15] == 0);
		} catch (Exception e) {
			fail("Exception thrown:" + e.getMessage());
		}
	}

	@Test
	public void Bit16() {
		try {
			KeyGenerator keyGenerator = KeyGenerator.getInstance("DES");
			SecretKey secretKey = keyGenerator.generateKey();
			Puzzle puzzle0 = new Puzzle(4095, secretKey);
			byte[] pb = puzzle0.getPuzzleAsBytes();
			assertTrue(pb[16] != 0);
		} catch (Exception e) {
			fail("Exception thrown:" + e.getMessage());
		}
	}

	
}
