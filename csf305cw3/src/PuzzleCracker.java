import javax.crypto.BadPaddingException;
import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import java.util.Arrays;
import java.io.FileInputStream;
import java.util.ArrayList;

// 
// Decompiled by Procyon v0.5.36
// 

public class PuzzleCracker
{
    private ArrayList<byte[]> puzzles;
    private Puzzle puzzle;
    
    public PuzzleCracker(final String fileName) {
        this.puzzles = new ArrayList<byte[]>();
        this.puzzle = null;
        this.readPuzzlesFromFile(fileName);
    }
    
    private void readPuzzlesFromFile(final String fileName) {
        try {
            final FileInputStream fileInputStream = new FileInputStream(fileName);
            while (fileInputStream.available() >= 32) {
                final byte[] data = new byte[32];
                fileInputStream.read(data);
                this.puzzles.add(data);
            }
            fileInputStream.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public Puzzle crack(final int puzzleNum) {
        Puzzle p = null;
        for (int i = 0; i < 65536; ++i) {
            try {
                final byte[] keyBytes = this.createKeyFromInt(i);
                final SecretKey key = CryptoLib.createKey(keyBytes);
                final byte[] attemptBytes = this.tryDecrypt(key, this.puzzles.get(puzzleNum));
                if (attemptBytes != null) {
                    final byte[] zeroHeader = new byte[16];
                    final byte[] testHeader = Arrays.copyOfRange(attemptBytes, 0, 16);
                    if (Arrays.equals(zeroHeader, testHeader)) {
                        final byte[] puzzleNumberBytes = Arrays.copyOfRange(attemptBytes, 16, 18);
                        final int puzNum = CryptoLib.byteArrayToSmallInt(puzzleNumberBytes);
                        final byte[] puzKeyBytes = Arrays.copyOfRange(attemptBytes, 18, 26);
                        final SecretKey puzKey = CryptoLib.createKey(puzKeyBytes);
                        p = new Puzzle(puzNum, puzKey);
                        return this.puzzle = p;
                    }
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
    private byte[] createKeyFromInt(final int i) {
        final byte[] ib = CryptoLib.smallIntToByteArray(i);
        final byte[] key = new byte[8];
        key[0] = ib[0];
        key[1] = ib[1];
        return key;
    }
    
    private byte[] tryDecrypt(final SecretKey secretKey, final byte[] puzzleBytes) {
        try {
            final Cipher cipher = Cipher.getInstance("DES");
            cipher.init(2, secretKey);
            byte[] plaintext = null;
            plaintext = cipher.doFinal(puzzleBytes);
            return plaintext;
        }
        catch (BadPaddingException e2) {
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public void decryptMessage(final byte[] message) {
        try {
            final Cipher cipher = Cipher.getInstance("DES");
            cipher.init(2, this.puzzle.getKey());
            final byte[] plaintext = cipher.doFinal(message);
            System.out.println("Bob: Recieved message and decrypted: " + new String(plaintext));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}