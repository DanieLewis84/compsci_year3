import javax.crypto.SecretKey;

/**
 * @author Dan Lewis - 223851
 *
 */
public class Puzzle {
	private int puzzleNumber;
	private SecretKey secretKey;

	/*
	 * The number of whole bytes of blank space used as a header for the puzzle
	 */
	public static final int BYTE_ARRAY_HEADER_LENGTH = 16;
	/*
	 * The length of the byte array used to serialise the puzzle's unique
	 * identifier
	 */
	public static final int BYTE_ARRAY_PUZZLE_NUMBER_LENGTH = 2;
	/* The length of the byte array used to serialise the puzzle's secret key */
	public static final int BYTE_ARRAY_KEY_LENGTH = 8;
	/*
	 * The length of the byte array used to serialise the complete puzzle
	 * object, including secret key
	 */
	public static final int BYTE_ARRAY_PUZZLE_LENGTH = BYTE_ARRAY_HEADER_LENGTH + BYTE_ARRAY_PUZZLE_NUMBER_LENGTH
			+ BYTE_ARRAY_KEY_LENGTH;

	/**
	 * Constructor for the Puzzle object
	 * @param puzzleNumber
	 *            an integer unique identifier for this puzzle
	 * @param secretKey
	 *            the key which will be saved with this puzzle
	 */
	public Puzzle(int puzzleNumber, SecretKey secretKey) {
		this.puzzleNumber = puzzleNumber;
		this.secretKey = secretKey;
	}

	/**
	 * @return the unique identifier for this puzzle
	 */
	public int getPuzzleNumber() {
		return this.puzzleNumber;
	}

	/**
	 * @return the secret key saved with this puzzle
	 */
	public SecretKey getKey() {
		return this.secretKey;
	}

	/**
	 * @return the byte array representation of this puzzle's secret key
	 */
	private byte[] getKeyBytes() {
		return this.secretKey.getEncoded();
	}

	/**
	 * @return the byte array representation of the complete Puzzle object
	 */
	public byte[] getPuzzleAsBytes() {
		byte[] puzzleBytes = new byte[BYTE_ARRAY_PUZZLE_LENGTH];
		byte[] puzzleNumberBytes = CryptoLib.smallIntToByteArray(this.puzzleNumber);
		// Copy puzzleNumber into puzzleBytes
		System.arraycopy(puzzleNumberBytes, 0, puzzleBytes, BYTE_ARRAY_HEADER_LENGTH, BYTE_ARRAY_PUZZLE_NUMBER_LENGTH);
		byte[] puzzleKeyBytes = this.getKeyBytes();
		// Copy puzzleKeyBytes into puzzleBytes
		System.arraycopy(puzzleKeyBytes, 0, puzzleBytes, BYTE_ARRAY_HEADER_LENGTH + BYTE_ARRAY_PUZZLE_NUMBER_LENGTH,
				BYTE_ARRAY_KEY_LENGTH);
		return puzzleBytes;
	}

}
