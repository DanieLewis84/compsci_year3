import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Random;

import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

public class TestPuzzle {
	
	public static void main(String[] args) throws NoSuchAlgorithmException, NoSuchPaddingException {
		PuzzleCreator puzzleCreator = new PuzzleCreator();
		System.out.println(CryptoLib.getHexStringRepresentation(puzzleCreator.createRandomKey()));
		System.out.println(CryptoLib.getHexStringRepresentation(puzzleCreator.createRandomKey()));
	}

}
