import java.security.NoSuchAlgorithmException;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

/**
 * @author Dan Lewis - 223851
 *
 */
public class Merkle {
	
	private static final String FILE_NAME="someFile.txt";
	private static final String TEST_TEXT="Testing Merkles Puzzles!";
	
	/**
	 * @param message The message to encrypt
	 * @param key     The key to encrypt it with
	 * @return        A cipher-text representing the encrypted message 
	 */
	public static byte[] encryptMessage(final byte[] message, SecretKey key) {
        try {
            final Cipher cipher = Cipher.getInstance(PuzzleCreator.ENCRYPTION_ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            final byte[] plaintext = cipher.doFinal(message);
            return plaintext;
        }
        catch (Exception e) {
            e.printStackTrace();
            return message;
        }
    }
	
	/**
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 */
	public static void main(String[] args) throws NoSuchAlgorithmException, NoSuchPaddingException {
		//1
		PuzzleCreator puzzleCreator = new PuzzleCreator();
		puzzleCreator.createPuzzles();
        puzzleCreator.encryptPuzzlesToFile(FILE_NAME);
        //2
        PuzzleCracker puzzleCracker = new PuzzleCracker(FILE_NAME);
        Random randomGenerator = new Random();
        int randomPuzzleNumber = randomGenerator.nextInt(PuzzleCreator.PUZZLES_TO_CREATE);
		Puzzle bobsPuzzle = puzzleCracker.crack(randomPuzzleNumber);
        //3
        SecretKey alicesKey = puzzleCreator.findKey(bobsPuzzle.getPuzzleNumber());
        //4
        byte[] ciphertext = encryptMessage(TEST_TEXT.getBytes(),alicesKey);
        puzzleCracker.decryptMessage(ciphertext);
	}

}
