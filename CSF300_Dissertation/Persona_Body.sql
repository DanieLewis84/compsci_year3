CREATE OR REPLACE PACKAGE BODY STUDENT_PROFILE.PERSONAS AS

  FUNCTION MoveDateToYearOffset(i_Date date,i_YearOffset int) return date AS
  v_out date;
  BEGIN
    v_out:=add_months(i_Date,12*(EXTRACT(year from sysdate)-EXTRACT(year from i_Date)));
    v_out:=add_months(v_out,12*i_YearOffset);
    RETURN v_out;
  END MoveDateToYearOffset;
  
  FUNCTION To_Ayr(i_Date date) return varchar2 AS
  v_out VARCHAR2(10);
  BEGIN
    SELECT AYR_CODE
      INTO v_out
      FROM ins_ayr
     WHERE TRUNC (i_Date) BETWEEN ayr_begd AND ayr_endd;
    RETURN v_out;
  END To_Ayr;
  
  FUNCTION AyrEndYear(i_Date date) return int AS
  v_out int;
  BEGIN
    SELECT EXTRACT(year from ayr_endd)
      INTO v_out
      FROM ins_ayr
     WHERE TRUNC (i_Date) BETWEEN ayr_begd AND ayr_endd;
    RETURN v_out;
  END AyrEndYear;
  
  FUNCTION BuildDate(i_day int,i_month int, i_year int) return date AS
  v_out date;
  BEGIN
    RETURN TO_DATE(TO_CHAR(i_day)||'/'||TO_CHAR(i_month)||'/'||TO_CHAR(i_year)||' 00:00:00', 'DD/MM/YYYY HH24:MI:SS');
  END BuildDate;

  FUNCTION DateOnly(i_Date date) return date AS
  BEGIN
    RETURN TO_DATE(to_char(i_Date, 'DD/MM/YYYY')||' 00:00:00', 'DD/MM/YYYY HH24:MI:SS');
  END DateOnly;
  
  FUNCTION TimeOnly(i_Date date) return date AS
  BEGIN
    RETURN TO_DATE('01/01/1900 '||to_char(i_Date, 'HH24:MI:SS'), 'DD/MM/YYYY HH24:MI:SS');
  END TimeOnly;
  

  PROCEDURE AddBaseRecord(i_stu Student, i_stu_begin_date INTUIT.INS_STU.STU_BEGD%type, i_add_home ContactAddress, i_add_uni ContactAddress,
	i_sponsor_ref INTUIT.INS_STU.STU_SSIN%type, i_yr_last_school INTUIT.INS_STU.STU_YLAS%type, i_ucas_num INTUIT.INS_STU.STU_UCID%type) AS
  BEGIN
    Insert into INTUIT.INS_STU
       (STU_CODE, STU_SRTN, STU_UPDD, STU_UCID, STU_STA1, 
        STU_STA2, STU_TITL, STU_SURN, STU_PRVS, STU_FNM1, 
        STU_FNM2, STU_FUSD, STU_INIT, STU_NAME, STU_DOB, 
        STU_GEND, STU_BEGD, STU_MARC, STU_NATC, STU_COBC, 
        STU_ETHC, STU_LEAC, STU_DSBC, STU_HQLC, STU_SCLC, 
        STU_ADID, STU_NOKN, STU_NOKR, 
        STU_NOKT, STU_HAD1, STU_HAD2, STU_HAD3, STU_HAD4, STU_HAD5, STU_HAPC, 
        STU_UHOC, STU_HTEL, STU_HAT3, STU_HAEM, 
        STU_CAD1, STU_CAD2, STU_CAD3, STU_CAD4, STU_CAD5, STU_CAPC, STU_CTEL, 
        STU_CAT3, STU_CAEM, STU_ESID, STU_CODC, STU_TTAC, 
        STU_YLAS, STU_NDEP, STU_SSIN, 
        STU_WELS, STU_NID1, STU_NID2, STU_DEPS, STU_UDF5, STU_UDF6, 
        STU_UDFA, STU_WEYN, STU_UKBA, STU_RELB, STU_SXOR, 
        STU_GNID, STU_DAVF, STU_UDF2, STU_UDFJ, STU_MVLN, STU_UDFK
        )
     Values
       (i_stu.code, i_stu.srtn, SYSDATE, i_ucas_num, 'A', 
        'C',  UPPER(i_stu.title),  UPPER(i_stu.surname),  UPPER(i_stu.surname), UPPER(i_stu.forname), 
        UPPER(i_stu.midname), UPPER(i_stu.forname), i_stu.initials, i_stu.fullname,i_stu.birthday, 
        i_stu.gender, i_stu_begin_date, i_stu.maritial_status, i_stu.nationality, i_stu.country_of_birth, 
        i_stu.ethnic_origin, i_stu.lea, i_stu.disability, i_stu.highest_qual, i_stu.prev_school, 
        i_stu.code, 'DAD', 'FATHER', 
        i_add_home.phone, i_add_home.add1, i_add_home.add2, i_add_home.add3, i_add_home.add4, i_add_home.add5, i_add_home.postcode, 
        i_stu.home_overseas, i_add_home.phone, i_add_uni.phone, i_add_home.email, 
        i_add_uni.add1, i_add_uni.add2, i_add_uni.add3, i_add_uni.add4, i_add_uni.add5, i_add_uni.postcode, i_add_uni.phone, 
        i_add_uni.phone, i_add_uni.email, i_stu.ext_id, i_stu.domicile_code, i_add_uni.tta_code, 
        i_yr_last_school, i_stu.dependants, i_sponsor_ref, 
        i_stu.speaks_welsh, i_stu.national_id, 'X', '03', '1000001000', 'N', 
        'VOTE', i_stu.pref_welsh_comm, i_stu.uk_vi, i_stu.religion, i_stu.sex_orient, 
        i_stu.gender_id, 'N', i_stu.welsh_mentor_pref, i_stu.gp_location, i_stu.car_access, i_stu.prev_care);
  END AddBaseRecord;
  
  
  PROCEDURE AddCourseJoin(i_stu Student, i_scj_seq INTUIT.SRS_SCJ.SCJ_SEQ2%type, i_ayr_cur INTUIT.SRS_SCJ.SCJ_AYRC%type, i_course Course,
  					      i_stu_begin_date INTUIT.SRS_SCJ.SCJ_BEGD%type, i_expect_end_date INTUIT.SRS_SCJ.SCJ_EEND%type,  i_age_on_entry INTUIT.SRS_SCJ.SCJ_AGOE%type, 
						  i_expiry_date INTUIT.SRS_SCJ.SCJ_EEND%type,i_home_postcode INTUIT.SRS_SCJ.SCJ_HAPC%type,i_ucas_num INTUIT.SRS_SCJ.SCJ_UCAP%type, 
                          i_begin_ayr INTUIT.INS_SPR.SPR_AYRS%type, i_end_ayr INTUIT.INS_SPR.SPR_AYRS%type) AS
  BEGIN
    Insert into INTUIT.SRS_SCJ
       (SCJ_CODE, SCJ_STUC, SCJ_SEQ2, SCJ_SPRC, SCJ_SRTN, 
        SCJ_AYRC, SCJ_CRSC, SCJ_BLOK, SCJ_OCCL, SCJ_CGPC, 
        SCJ_MOAC, SCJ_RFTC, SCJ_BEGD, SCJ_QENC, 
        SCJ_PRSC, SCJ_STAC, SCJ_EEND, SCJ_ESTC,  
        SCJ_HSIN, SCJ_AGOE, SCJ_CODC, 
        SCJ_UDF1, SCJ_UDF9, SCJ_EXPD, SCJ_NOTE, SCJ_HAPC,
        SCJ_WELB, SCJ_SECL, SCJ_SCLC, SCJ_UCAP,
        SCJ_UDFH, SCJ_UDFI
        )
     Values
       (i_stu.scjc, i_stu.code, i_scj_seq, i_stu.scjc, i_stu.srtn, 
        i_ayr_cur, i_course.code, '1', i_course.occurrence, i_course.cgroup, 
        i_course.attendance_method_code, '11', i_stu_begin_date,  '40', 
        '044413', 'C', i_expect_end_date, '1', 
        '01',  i_age_on_entry, '516', 
        '28/JUN/2006', 'UW', i_expiry_date, '',i_home_postcode,
        '3','3', '10797',i_ucas_num,
        TO_CHAR(SYSDATE-30,'DD/Mon/YYYY'),TO_CHAR(SYSDATE+30,'DD/Mon/YYYY')
        );
        
    INSERT INTO INTUIT.INS_SPR
       (SPR_CODE,SPR_SNAM,SPR_NAME,SPR_FNM1,SPR_FNM2,SPR_INIT,
        SPR_STUC,STS_CODE,LCA_CODE,PRG_CODE,ROU_CODE,--SPR_BATCH,
        MOA_CODE,AYR_CODE,--PSL_CODE,
        SPR_SDATE,SPR_EDATE,SPR_GEND,
        SPR_DOB,SPR_SURN,
        SPR_APRG,SPR_AROU,SPR_LEVC,SPR_AYRS,--SPR_PSLS,SPR_PSLE,
        SPR_AYRE,SPR_DPTC,SPR_FACC)
     Values
       (i_stu.scjc,i_stu.forname,i_stu.surname,i_stu.forname,i_stu.midname,i_stu.initials,
        i_stu.code,'C',i_course.loc_of_study,i_course.code,i_course.route,--'W',
        i_course.attendance_method_code,i_ayr_cur,--null,
        i_stu_begin_date,i_expect_end_date,i_stu.gender,
        i_stu.birthday,i_stu.srtn,
        i_course.code,i_course.route,i_course.level_code,i_begin_ayr,--'TB1','TB2',
        i_end_ayr,i_course.dept,i_course.faculty);
        
    INSERT INTO INTUIT.SRS_RDS 
       (RDS_SCJC,RDS_SEQ2,RDS_STUC,RDS_SRTN,RDS_RPJC,
        RDS_RPSC,RDS_NRVD,RDS_IDRD,RDS_OERD,RDS_CERD,
        RDS_INCC,RDS_NOMI,RDS_NOMA,RDS_STAF,RDS_RSTC,
        RDS_SUBC,RDS_SUB1,RDS_DPTC,RDS_FACC,RDS_MOAC,
        RDS_SPYN,RDS_ESB1,RDS_ESB2,RDS_ESB3,RDS_RCST,
        RDS_RCSI,RDS_INS1,RDS_INS2,RDS_TFMD,RDS_TTOD) 
     values 
       (i_stu.scjc,i_scj_seq,i_stu.code,i_stu.srtn,null,
        null,i_expect_end_date,null,null,null,
        '',null,null,null,null,
        null,null,null,null,null,
        'N',null,null,null,'99',
        null,null,null,null,null);

  END AddCourseJoin; 
   
  PROCEDURE AddCourseEnrolment(i_stu student, i_sce_seq INTUIT.SRS_SCE.SCE_SEQ2%type, i_ayr_cur INTUIT.SRS_SCE.SCE_AYRC%type, 
                                i_course Course,
                                i_fee_due INTUIT.SRS_SCE.SCE_FEED%type, 
                                i_next_ayr INTUIT.SRS_SCE.SCE_NAYC%type, 
                                i_begin_date INTUIT.SRS_SCE.SCE_BEGD%type,
                                i_last_print_date INTUIT.SRS_SCE.SCE_LEPD%type,
                                i_transfer_date INTUIT.SRS_SCE.SCE_TTOD%type) AS
  BEGIN
    Insert into INTUIT.SRS_SCE
       (SCE_SCJC, SCE_SEQ2, SCE_STUC, SCE_AYRC, SCE_CRSC, 
        SCE_BLOK, SCE_OCCL, SCE_CGPC, SCE_SRTN, SCE_ESB1, 
        SCE_EBAL, SCE_STYC, SCE_MOAC, SCE_SSFC, SCE_FEED, 
        SCE_STAC, SCE_NAYC, SCE_NCRC, SCE_NBLK, SCE_NOCL, 
        SCE_BEGD, SCE_FSTC, SCE_ELSC, SCE_STA1, SCE_PGSC, 
        SCE_RSYN, SCE_AS1R, SCE_PRGC, SCE_ROUC, SCE_DPTC, 
        SCE_RESC, SCE_HESA, SCE_LEPD, SCE_FACC, SCE_RECS, 
        SCE_1STR, SCE_2NDR, SCE_MOA1, SCE_STA2, SCE_PRG1, 
        SCE_ROU1, SCE_BTCH, SCE_UFIP, SCE_TTAC, SCE_FSYN, 
        SCE_SDYN, SCE_TTOD, SCE_STNP, SCE_MFSC, SCE_FPTC)
     Values
       (i_stu.scjc, i_sce_seq, i_stu.code, i_ayr_cur, i_course.code, 
        i_course.cblock, i_course.occurrence, i_course.cgroup, i_stu.srtn, i_course.ext_code, 
        '0', '9', i_course.attendance_method_code, '02', i_fee_due, 
        'NE', i_next_ayr, i_course.code, '2', 'A', 
        i_begin_date, i_course.fee_status, i_course.loc_of_study, 'CONF', '', 
        'Y', 'O', i_course.code, i_course.route, i_course.dept, 
        'P', 9, i_last_print_date, i_course.faculty, 'DRCT', 
        'S', 'S', '11', 'NE', i_course.code, 
        i_course.route, 'CONFCL', '0', '4', 'N', 
        'N',i_transfer_date, '12345678901234567890', '999', i_course.fee_profile);
  END AddCourseEnrolment;
  
  PROCEDURE CloseScj(i_stu student, i_days int) AS
  BEGIN
    UPDATE SRS_SCJ SET SCJ_ENDD=SCJ_BEGD+i_Days where SCJ_CODE=i_stu.scjc;
  END CloseScj;
  
  PROCEDURE AddAddressHome(i_stu_code INTUIT.INS_STU.STU_CODE%type, i_add_home ContactAddress, i_stu_mobile INTUIT.MEN_ADD.ADD_TELN%type) AS
  BEGIN
    Insert into INTUIT.MEN_ADD
       (ADD_AENT, ADD_ADID, ADD_SEQN, ADD_ATYC, 
        ADD_ADD1, ADD_ADD2, ADD_ADD3, ADD_ADD4, ADD_ADD5,
        ADD_PCOD, ADD_TELN, ADD_TEL3, 
        ADD_EMAD, ADD_DETS, ADD_TEMP, ADD_ACTV, ADD_UPDD, 
        ADD_USRC)
     Values
       ('STU', i_stu_code, '0001', 'H', 
       i_add_home.add1, i_add_home.add2, i_add_home.add3, i_add_home.add4, i_add_home.add5,
       i_add_home.postcode, i_add_home.phone, i_stu_mobile, 
        i_add_home.email, 'C', 'N', 'C', add_months(SYSDATE,-1), 
        'INTUIT');
  END AddAddressHome;
  
  PROCEDURE AddAddressTerm(i_stu_code INTUIT.INS_STU.STU_CODE%type, i_add_uni ContactAddress) AS
  BEGIN
	Insert into INTUIT.MEN_ADD
       (ADD_AENT, ADD_ADID, ADD_SEQN, ADD_ATYC, 
        ADD_ADD1, ADD_ADD2, ADD_ADD3, ADD_ADD4, ADD_ADD5,
	   ADD_PCOD, ADD_TELN, ADD_TEL3, 
        ADD_EMAD, ADD_DETS, ADD_TEMP, ADD_TTAC, ADD_ACTV, 
        ADD_UPDD, ADD_USRC)
     Values
       ('STU', i_stu_code, '0002', 'C', 
		i_add_uni.add1, i_add_uni.add2, i_add_uni.add3, i_add_uni.add4, i_add_uni.add5,	   
	    i_add_uni.postcode, i_add_uni.phone, i_add_uni.phone, 
        i_add_uni.email, 'C', 'N', '7', 'C', 
        add_months(SYSDATE,-1), 'INTUIT');
  END AddAddressTerm;
  
  PROCEDURE AddAddressContact(i_stu_code INTUIT.INS_STU.STU_CODE%type,
    i_add_contact ContactAddress,
    i_contact_landline INTUIT.MEN_ADD.ADD_TELN%type,
    i_email  INTUIT.MEN_ADD.ADD_EMAD%type
    ) AS
  BEGIN
    Insert into INTUIT.MEN_ADD
       (ADD_AENT, ADD_ADID, ADD_SEQN, ADD_ATYC, 
	    ADD_ADD1, ADD_ADD2, ADD_ADD3, ADD_ADD4, ADD_ADD5,
		ADD_PCOD, ADD_TELN, ADD_EMAD, 
        ADD_DETS, ADD_BEGD, ADD_TEMP, ADD_ACTV, ADD_UPDD, 
        ADD_USRC)
     Values
       ('STU', i_stu_code, '0003', 'HOL', 
	    i_add_contact.add1, i_add_contact.add2, i_add_contact.add3, i_add_contact.add4, i_add_contact.add5,	   
		i_add_contact.postcode, i_contact_landline, i_email, 
        'C', add_months(SYSDATE,-1), 'Y', 'C', add_months(SYSDATE,-1), 
        'INTUIT');  
  END AddAddressContact;

  PROCEDURE AddNextOfKin(i_stu_code INTUIT.INS_STU.STU_CODE%type, i_add_contact ContactAddress) AS
  BEGIN
    Insert into INTUIT.SRS_SNK
       (SNK_STUC, SNK_SEQN, SNK_NKRC, 
       SNK_HAD1, SNK_HAD2, SNK_HAD3, SNK_HAD4, SNK_HAD5, 
       SNK_HAPC, SNK_USTU)
     Values
       (i_stu_code, '001', 'PARENT', 
       i_add_contact.add1, i_add_contact.add2, i_add_contact.add3, i_add_contact.add4, i_add_contact.add5, 
       i_add_contact.postcode, 'N'); 
  END AddNextOfKin;

  PROCEDURE AddFees(i_stu Student, i_scj_seq INTUIT.SRS_FDU.FDU_SCES%Type, i_fee_seq INTUIT.SRS_FDU.FDU_SEQN%Type, 
                    i_course Course, i_ayr_cur INTUIT.SRS_FDU.FDU_AYRC%Type, i_fee_due_date INTUIT.SRS_FDU.FDU_DUED%Type, 
                    i_extracted_date INTUIT.SRS_FDU.FDU_EXTD%Type)  As
  BEGIN
    Insert into INTUIT.SRS_FDU
       (FDU_SCJC, FDU_SCES, FDU_SEQN, FDU_SRTN, FDU_CRSC, 
        FDU_BLOK, FDU_OCCL, FDU_AYRC, FDU_ACCC, FDU_FDTC, 
        FDU_FAPC, FDU_DUED, FDU_AMNT, FDU_CORD, FDU_SFTY, 
        FDU_EXTD, FDU_STAT, FDU_FSTC)
     Values
       (i_stu.scjc,i_scj_seq,i_fee_seq, i_stu.srtn, i_course.code, 
        i_course.cblock, i_course.occurrence, i_ayr_cur, 'TFN122270', 'CF', 
        '042003', i_fee_due_date, i_course.fee, 'D', 'B', 
        i_extracted_date, 'Y', 'PHU1N');  
  END AddFees;

  PROCEDURE AddSponsor(i_stu_code INTUIT.SRS_SSP.SSP_STUC%Type,i_sponsor_seq INTUIT.SRS_SSP.SSP_SEQ2%Type, i_stu_srtn INTUIT.SRS_SSP.SSP_SRTN%Type, i_amount INTUIT.SRS_SSP.SSP_AMNT%Type,
                      i_sponsor_ref INTUIT.SRS_SSP.SSP_SREF%Type, i_course_code INTUIT.SRS_SSP.SSP_CRSC%Type, i_ayr_cur INTUIT.SRS_SSP.SSP_AYRC%Type, i_sponsor_code INTUIT.SRS_SSP.SSP_SPOC%Type)  As
  BEGIN
    Insert into INTUIT.SRS_SSP
       (SSP_STUC, SSP_SEQ2, SSP_SRTN, SSP_SPOC, SSP_AMNT, 
        SSP_SREF, SSP_HOLD, SSP_LUPD, SSP_CRSC, SSP_AYRC, 
        SSP_DRYN, SSP_GFT3)
     Values
       (i_stu_code, i_sponsor_seq, i_stu_srtn, 'SLC', i_amount, 
        i_sponsor_ref, 'N', add_months(SYSDATE,-1), i_course_code, i_ayr_cur, 
        'Y', 'N');
  END AddSponsor;

  PROCEDURE AddALevel(i_stu_code INTUIT.SRS_SQE.SQE_STUC%Type, i_seqn INTUIT.SRS_SQE.SQE_SEQN%Type,
                      i_points INTUIT.SRS_SQE.SQE_PNTS%type, i_subject_code INTUIT.SRS_SQE.SQE_ESUC%type,
                      i_grade  INTUIT.SRS_SQE.SQE_ESGC%type,
                      i_ayr_achieved INTUIT.SRS_SQE.SQE_AYRC%type,i_verify_date Date, i_stu_srtn INTUIT.SRS_SQE.SQE_SRTN%Type)  As
  BEGIN
    Insert into INTUIT.SRS_SQE
       (SQE_STUC, SQE_SEQN, SQE_EQEC, SQE_DESC, SQE_SQTC, 
        SQE_SQLC, SQE_LPNT, SQE_PNTS, SQE_WGHT, SQE_ESUC, 
        SQE_ESMC, SQE_ESGC, SQE_RSLT, SQE_AORP, SQE_ESIT, 
        SQE_AYRC, SQE_SQSC, SQE_VRFD, SQE_SRTN, SQE_VALC, 
        SQE_EXTR, SQE_EXHR, SQE_SECF)
     Values
       (i_stu_code, i_seqn, 'GAL', 'GCE A-Level', 'ACAD', 
        '02', 0, i_points, 1, i_subject_code, 
        'ALT', i_grade, 'P', 'A', 'S', 
        i_ayr_achieved, 'VU', i_verify_date, i_stu_srtn, 'W', 
        '40', 'N', 'N');
  END AddALevel;
  
  PROCEDURE AddASLevel(i_stu_code INTUIT.SRS_SQE.SQE_STUC%Type, i_seqn INTUIT.SRS_SQE.SQE_SEQN%Type,
                      i_points INTUIT.SRS_SQE.SQE_PNTS%type, i_subject_code INTUIT.SRS_SQE.SQE_ESUC%type,
                      i_grade  INTUIT.SRS_SQE.SQE_ESGC%type,
                      i_ayr_achieved INTUIT.SRS_SQE.SQE_AYRC%type,i_verify_date Date, i_stu_srtn INTUIT.SRS_SQE.SQE_SRTN%Type)  As
  BEGIN
    Insert into INTUIT.SRS_SQE
       (SQE_STUC, SQE_SEQN, SQE_EQEC, SQE_DESC, SQE_SQTC, 
        SQE_SQLC, SQE_LPNT, SQE_PNTS, SQE_WGHT, SQE_ESUC, 
        SQE_ESMC, SQE_ESGC, SQE_RSLT, SQE_AORP, SQE_ESIT, 
        SQE_AYRC, SQE_SQSC, SQE_VRFD, SQE_SRTN, SQE_VALC, 
        SQE_EXTR, SQE_EXHR, SQE_SECF)
     Values
       (i_stu_code, i_seqn, 'GASL', 'GCE AS-Level', 'ACAD', 
        '01', 0, i_points, 0.5, i_subject_code, 
        'ASLT', i_grade, 'P', 'A', 'S', 
        i_ayr_achieved, 'NV', i_verify_date, i_stu_srtn, 'W', 
        '40', 'N', 'N');
  END AddASLevel;

  PROCEDURE AddBobsALevels(i_stu Student, i_ayr_achieved INTUIT.SRS_SQE.SQE_AYRC%type,i_verify_date Date)  As
  BEGIN
    AddALevel(i_stu.code, '0007', 60, 'PHYS', 'D', i_ayr_achieved, i_verify_date, i_stu.srtn);
    AddALevel(i_stu.code, '0008', 100, 'COMP', 'B', i_ayr_achieved, i_verify_date, i_stu.srtn);
    AddALevel(i_stu.code, '0009', 120, 'MATHS', 'A', i_ayr_achieved, i_verify_date, i_stu.srtn);
    AddALevel(i_stu.code, '0010', 100, 'MA-FU', 'B', i_ayr_achieved, i_verify_date, i_stu.srtn);
  END AddBobsALevels;
  
  PROCEDURE AddBobsASLevels(i_stu Student, i_ayr_achieved INTUIT.SRS_SQE.SQE_AYRC%type)  As
  BEGIN
    AddASLevel(i_stu.code, '0004', 40, 'PHYS', 'C', i_ayr_achieved, null, i_stu.srtn);
    AddASLevel(i_stu.code, '0005', 50, 'COMP', 'B', i_ayr_achieved, null, i_stu.srtn);
    AddASLevel(i_stu.code, '0006', 50, 'MA-AP', 'B', i_ayr_achieved, null, i_stu.srtn);
  END AddBobsASLevels;
    
  FUNCTION AddressMerthyr return ContactAddress AS
  v_out ContactAddress;
  BEGIN
    v_out.postcode:='CF47 8YU';
    v_out.add1:='1 Gwaelodygarth';
    v_out.add2:='';
    v_out.add3:='';
    v_out.add4:='Merthyr Tydfil';
    v_out.add5:='';
    v_out.email:='fakeHomeEmail@gmail.com';
    v_out.phone:='01685 123456';
    v_out.tta_code:='2';--from SRS_TTA 
    RETURN v_out;
  END AddressMerthyr;
  
  FUNCTION AddressTyBeck(i_stu_code INTUIT.INS_STU.STU_CODE%type) return ContactAddress AS
  v_out ContactAddress;
  BEGIN
    v_out.postcode:='SA2 0NF';
    v_out.add1:='Block A1, Ty Beck House';
    v_out.add2:='Sketty Road';
    v_out.add3:='';
    v_out.add4:='Swansea';
    v_out.add5:='';
    v_out.email:=i_stu_code||'@swansea.ac.uk';
    v_out.phone:='07890123456';
    v_out.tta_code:='1';--from SRS_TTA 
    RETURN v_out;
  END AddressTyBeck;
  
  FUNCTION AddressFrance return ContactAddress AS
  v_out ContactAddress;
  BEGIN
    v_out.postcode:='35720';
    v_out.add1:='R�SIDENCE CL� DES CHAMPS,';
    v_out.add2:='BATIMENT B CHAMBRE 423';
    v_out.add3:='LA CELLE SAINT CLOUD';
    v_out.add4:='RENNES';
    v_out.add5:='FRANCE';
    v_out.email:='fakeHomeEmail@gmail.com';
    v_out.phone:='+33 299 123 456';
    v_out.tta_code:='4';--from SRS_TTA 
    RETURN v_out;
  END AddressFrance;
  
  FUNCTION AddressChina return ContactAddress AS
  v_out ContactAddress;
  BEGIN
    v_out.postcode:='110032';
    v_out.add1:='UNIT 5-6-5,';
    v_out.add2:='52-6 FEN HE STREET HUANG GU DISTRICT';
    v_out.add3:='SHEN YANG CITY';
    v_out.add4:='LIAO NING PROVINCE';
    v_out.add5:='CHINA';
    v_out.email:='fakeHomeEmail@gmail.com';
    v_out.phone:='0086 24 31234567';
    v_out.tta_code:='4';--from SRS_TTA 
    RETURN v_out;
  END AddressChina;
  
  FUNCTION ModuleBasicDetails(i_Mod_Code INTUIT.CAM_MAV.MOD_CODE%type, ActualAyr INTUIT.INS_AYR.AYR_CODE%type, StartAyr INTUIT.INS_AYR.AYR_CODE%type) return Module AS
  v_out Module;
  BEGIN
    v_out.ActualAyr:=ActualAyr;
    v_out.StartAyr:=StartAyr;
    SELECT mod_code, mav_occur, psl_code, lev_code
           INTO  v_out.Code, v_out.Occurence, v_out.PSL, v_out.lvl_code
           FROM CAM_MAV 
          WHERE ayr_code = StartAyr
                AND mod_code = i_Mod_Code
                and rownum =1;
    SELECT INS_MOD.mod_crdt INTO  v_out.Credits FROM INS_MOD WHERE mod_code = i_Mod_Code and rownum =1;
    return v_out;
  END ModuleBasicDetails;
  
  PROCEDURE AddModuleSelection (i_stu Student, i_mod Module, i_DietId IN INTUIT.CAM_SMO.SMO_UDF2%TYPE) AS
  BEGIN
    INSERT INTO CAM_SMO (spr_code, mod_code, mav_occur, ayr_code, psl_code,
                         smo_surn, smo_evg1, smo_evg2, smo_agrp, smo_schc,
                         smo_levc, smo_mcrd, smo_udf2, SES_CODE)
    VALUES (i_stu.scjc, i_mod.code, i_mod.Occurence, i_mod.ActualAyr, i_mod.PSL,
            i_stu.srtn, NULL, NULL, NULL, 'MOD',
            i_mod.lvl_code, i_mod.Credits, i_DietId,
            CASE WHEN i_DietId = 'COMPULSORY' THEN 'C' ELSE 'O' END);
  END AddModuleSelection;
  
  PROCEDURE EnrolStudentOnCourse (i_stu Student, i_course Course, i_ayr Ayr) AS
  BEGIN
    UPDATE SRS_SCE
            SET SCE_STAC = SUBSTR (REPLACE (SCE_STAC, '-R', ''), 2),
                SCE_UDF3 = SYSDATE,
                SCE_STAD = SYSDATE
          WHERE     SCE_AYRC = i_ayr.ayr
                AND SCE_SCJC = i_stu.scjc
                AND SUBSTR (REPLACE (SCE_STAC, '-R', ''), 2) IN
                       (SELECT STA_CODE
                          FROM SRS_STA);
    STUDENT_PROFILE.INTRANET_ONLINE_ENROLMENT.build_smr_sas_from_smo(i_stu.scjc,i_ayr.ayr);
  END EnrolStudentOnCourse;
  
  FUNCTION CourseBasicDetails(i_uwcode INTUIT.UWSDATA_SCHEME.UWCODE%type) return Course AS
  v_out Course;
  v_count int;
  BEGIN
    select count(*) into v_count from uwsdata_scheme uws where uwcode=i_uwcode;
    if (v_count>0) then 
        select EXTC, PRGC, ROUC into v_out.ext_code, v_out.code,v_out.route
            from uwsdata_scheme uws where uwcode=i_uwcode and AYRC=(Select max(ayrc) from uwsdata_scheme where uwcode=i_uwcode);
    end if;
    select case to_char(crs_uomc) when '1' then CRS_LENG else CRS_YLEN end as ylen,crs_cgpc,crs_facc,crs_moac, case substr(CRS_CGPC,0,2) when u'PG' then '1' else '0' end --'
    into v_out.yrs, v_out.cgroup, v_out.faculty, v_out.attendance_method_code, v_out.is_postgrad
    from srs_crs crs where CRS_IUSE='Y' and crs_code=v_out.code;
    
    RETURN v_out;
  END CourseBasicDetails;

  FUNCTION CourseCompSci return Course AS
  v_out Course;
  BEGIN
    v_out:=CourseBasicDetails('BSHS3XX'||'XCSCS');
    v_out.cblock:='1';--SCE_BLOK
    v_out.dept:='CSCI';
    v_out.fee:=9000;--FDU_AMNT
    v_out.fee_status:='PHU1N';--SCE_FSTC
    v_out.fee_status_overseas:='POU1N';--SCE_FSTC
    v_out.loc_of_study:='PRK';--SCE_ELSC from SRS_ELS
    v_out.level_code:=1;--SPR_LEVC from CAM_LEV
    v_out.fee_profile:='';--SCE_FPTC
    v_out.occurrence:='A';--SCE_OCCL
    v_out.next_occurrence:='A';--SCE_NOCL
    RETURN v_out;
  END CourseCompSci;

   FUNCTION CourseNoPathway return Course AS
  v_out Course;
  BEGIN
    v_out:=CourseBasicDetails('BSHS3XX'||'XCSOS');
    v_out.cblock:='1';--SCE_BLOK
    v_out.dept:='CSCI';
    v_out.fee:=9000;--FDU_AMNT
    v_out.fee_status:='PHU1N';--SCE_FSTC
    v_out.fee_status_overseas:='POU1N';--SCE_FSTC
    v_out.loc_of_study:='PRK';--SCE_ELSC from SRS_ELS
    v_out.level_code:=1;--SPR_LEVC from CAM_LEV
    v_out.fee_profile:='';--SCE_FPTC
    v_out.occurrence:='A';--SCE_OCCL
    v_out.next_occurrence:='A';--SCE_NOCL
    RETURN v_out;
  END CourseNoPathway;

  PROCEDURE ModulesCompSci(i_ActualAyr in INTUIT.INS_AYR.AYR_CODE%type, i_StartAyr in INTUIT.INS_AYR.AYR_CODE%type, i_level in INTUIT.INS_MOD.LEV_CODE%Type, o_rs out SYS_REFCURSOR) AS
  v_module Module;
  v_modulelist SYS_REFCURSOR;
  v_ActualAyr Ayr;
  v_StartAyr Ayr;
  BEGIN
   OPEN o_rs FOR
     select i_ActualAyr as ActualAyr, i_StartAyr as StartAyr, Code, Occurence, PSL, lvl_code, Credits from (
       SELECT  distinct ins.MOD_CODE as Code,   mav.mav_occur as Occurence,mav.psl_code as PSL, mav.lev_code as lvl_code, ins.mod_crdt as Credits from INS_MOD ins join CAM_MAV mav on ins.MOD_CODE=mav.MOD_CODE
         where mav.lev_code=nvl(i_level ,mav.lev_code)
         and ayr_code = i_StartAyr
         and ins.MOD_CODE in ('CS-110','CS-115','CS-130','CS-135','CS-150','CS-155','CS-170','CS-175',
                              'CS-200','CS-205','CS-210','CS-230','CS-250','CS-255','CS-270','CS-275') 
         ) order by lvl_code, Code;
  END ModulesCompSci;
  
  FUNCTION ModulesCompSci(i_ActualAyr  INTUIT.INS_AYR.AYR_CODE%type, i_StartAyr  INTUIT.INS_AYR.AYR_CODE%type, i_level INTUIT.INS_MOD.LEV_CODE%Type) return ModuleList PIPELINED AS
  v_module Module;
  v_modulelist SYS_REFCURSOR;
  v_ActualAyr Ayr;
  v_StartAyr Ayr;
  BEGIN
   ModulesCompSci(i_ActualAyr, i_StartAyr, i_level, v_modulelist );
   LOOP
        FETCH v_modulelist INTO v_module;
        EXIT WHEN v_modulelist%NOTFOUND;
        PIPE ROW (v_module);
   END LOOP;
  END ModulesCompSci;
  
  PROCEDURE SelectCourseModules(i_stu Student, i_modulelist in SYS_REFCURSOR, i_ActualAyr  INTUIT.INS_AYR.AYR_CODE%type, i_StartAyr  INTUIT.INS_AYR.AYR_CODE%type, i_level INTUIT.INS_MOD.LEV_CODE%Type) AS
  v_mod Module;
  BEGIN
    LOOP
        FETCH i_modulelist INTO v_mod;
        EXIT WHEN i_modulelist%NOTFOUND;
        AddModuleSelection (i_stu, v_mod, 'COMPULSORY');
    END LOOP;
  END SelectCourseModules;
  
  PROCEDURE SelectCourseModules(i_stu Student, i_modules ModuleList, i_ActualAyr  INTUIT.INS_AYR.AYR_CODE%type, i_StartAyr  INTUIT.INS_AYR.AYR_CODE%type, i_level INTUIT.INS_MOD.LEV_CODE%Type) AS
  v_mod Module;
  BEGIN
    FOR v_mod in (select * from TABLE(i_modules))
    LOOP
      IF (i_level=v_mod.lvl_code) THEN
        AddModuleSelection (i_stu, v_mod, 'COMPULSORY');
      END IF;
    END LOOP;
  END SelectCourseModules;
    
  FUNCTION CourseNursing return Course AS
  v_out Course;
  BEGIN
    v_out:=CourseBasicDetails('BSHD3XA'||'XSHNAS');
    v_out.cblock:='1';--SCE_BLOK
    v_out.dept:='SHLS';
    v_out.fee:=1100;--FDU_AMNT
    v_out.fee_status:='WAGFEE';--SCE_FSTC
    v_out.fee_status_overseas:='WAGFEO';--SCE_FSTC
    v_out.loc_of_study:='PRK';--SCE_ELSC from SRS_ELS
    v_out.level_code:=1;--SPR_LEVC from CAM_LEV
    v_out.fee_profile:='ZERO FEE';--SCE_FPTC
    v_out.occurrence:='A';--SCE_OCCL
    v_out.next_occurrence:='A';--SCE_NOCL
    RETURN v_out;
  END CourseNursing;

  FUNCTION CourseHealthScienceAssociate return Course AS
  v_out Course;
  BEGIN
    v_out:=CourseBasicDetails('ROADUGX'||'DSHMOO');
    v_out.cblock:='1';--SCE_BLOK
    v_out.dept:='SHLS';
    v_out.fee:=1100;--FDU_AMNT
    v_out.fee_status:='PHU1N';--SCE_FSTC
    v_out.fee_status_overseas:='POU1N';--SCE_FSTC
    v_out.loc_of_study:='PRK';--SCE_ELSC from SRS_ELS
    v_out.level_code:=1;--SPR_LEVC from CAM_LEV
    v_out.fee_profile:='';--SCE_FPTC
    v_out.occurrence:='A';--SCE_OCCL
    v_out.next_occurrence:='A';--SCE_NOCL
    RETURN v_out;
  END CourseHealthScienceAssociate;

  FUNCTION CoursePostGrad return Course AS
  v_out Course;
  BEGIN
    v_out:=CourseBasicDetails('PHFS1CS'||'PRSRCHM');
    v_out.cblock:='1';--SCE_BLOK
    v_out.dept:='CSCI';
    v_out.fee:=3440;--FDU_AMNT
    v_out.fee_status:='PHP1R';--SCE_FSTC
    v_out.fee_status_overseas:='POP1R';--SCE_FSTC
    v_out.loc_of_study:='PRK';--SCE_ELSC from SRS_ELS
    v_out.level_code:=8;--SPR_LEVC from CAM_LEV
    v_out.fee_profile:='';--SCE_FPTC
    v_out.occurrence:='A';--SCE_OCCL
    v_out.next_occurrence:='A';--SCE_NOCL
    RETURN v_out;
  END CoursePostGrad;
    
  FUNCTION CourseLPC return Course AS
  v_out Course;
  BEGIN
    v_out:=CourseBasicDetails('DTFB1XX'||'PTDLA07');
    v_out.cblock:='1';--SCE_BLOK
    v_out.dept:='LLPC';
    v_out.fee:=9000;--FDU_AMNT
    v_out.fee_status:='PHP1R';--SCE_FSTC
    v_out.fee_status_overseas:='POP1R';--SCE_FSTC
    v_out.loc_of_study:='PRK';--SCE_ELSC from SRS_ELS
    v_out.level_code:='M';--SPR_LEVC from CAM_LEV
    v_out.fee_profile:='';--SCE_FPTC
    v_out.occurrence:='A';--SCE_OCCL
    v_out.next_occurrence:='A';--SCE_NOCL
    RETURN v_out;
  END CourseLPC;

  FUNCTION CourseCrossAyr return Course AS
  v_out Course;
  BEGIN
    v_out:=CourseBasicDetails('BSHD3MA'||'XSHNAS');
    v_out.cblock:='1';--SCE_BLOK
    v_out.dept:='NSNG';
    v_out.fee:=3440;--FDU_AMNT
    v_out.fee_status:='WAGFEE';--SCE_FSTC
    v_out.fee_status_overseas:='WAGFEO';--SCE_FSTC
    v_out.loc_of_study:='PRK';--SCE_ELSC from SRS_ELS
    v_out.level_code:=8;--SPR_LEVC from CAM_LEV
    v_out.fee_profile:='';--SCE_FPTC
    v_out.occurrence:='A';--SCE_OCCL
    v_out.next_occurrence:='B';--SCE_NOCL
    RETURN v_out;
  END CourseCrossAyr;
    

  FUNCTION CourseDACE return Course AS
  v_out Course;
  BEGIN
    v_out:=CourseBasicDetails('ROAA1DX'||'DDACCM');
    v_out.cblock:='1';--SCE_BLOK
    v_out.dept:='DACE';
    v_out.fee:=0;--FDU_AMNT
    v_out.fee_status:='HUP16C';--SCE_FSTC
    v_out.fee_status_overseas:='OUP16';--SCE_FSTC
    v_out.loc_of_study:='PRK';--SCE_ELSC from SRS_ELS
    v_out.level_code:='UX';--SPR_LEVC from CAM_LEV
    v_out.fee_profile:='ZERO FEE';--SCE_FPTC
    v_out.occurrence:='A';--SCE_OCCL
    v_out.next_occurrence:='A';--SCE_NOCL
    RETURN v_out;
  END CourseDACE;
  
  FUNCTION CourseICWS return Course AS
  v_out Course;
  BEGIN
    --v_out:=CourseBasicDetails('ROAA1DX'||'DDACCM');
    v_out:=CourseBasicDetails('BSXS2IX'||'RBISS');
    --v_out:=CourseBasicDetails('ROAICWX'||'DROAICWM');
    --BSXS2IX	RBISS
    v_out.cblock:='1';--SCE_BLOK
    v_out.dept:='TCSU';
    v_out.fee:=0;--FDU_AMNT
    v_out.fee_status:='TCSU';--SCE_FSTC
    v_out.fee_status_overseas:='TCSU';--SCE_FSTC
    v_out.loc_of_study:='PRK';--SCE_ELSC from SRS_ELS
    v_out.level_code:=0;--SPR_LEVC from CAM_LEV
    v_out.fee_profile:='';--SCE_FPTC
    v_out.occurrence:='A';--SCE_OCCL
    v_out.next_occurrence:='A';--SCE_NOCL
    RETURN v_out;
  END CourseICWS;
  
  FUNCTION CourseCOAH return Course AS
  v_out Course;
  BEGIN
    --v_out:=CourseBasicDetails('ROAA1DX'||'DDACCM');
--    SCE_PRGC	SCE_ROUC 	SCE_DPTC
--    'BAHA3XX'	'XPOIAHID'	'POLS'
    v_out:=CourseBasicDetails('BAHA3XX'||'XPOIAHID');
    --v_out:=CourseBasicDetails('ROAICWX'||'DROAICWM');
    --BSXS2IX	RBISS
    v_out.cblock:='1';--SCE_BLOK
    v_out.dept:='POLS';
    v_out.fee:=0;--FDU_AMNT
    v_out.fee_status:='PHU1N';--SCE_FSTC
    v_out.fee_status_overseas:='POU1N';--SCE_FSTC
    v_out.loc_of_study:='PRK';--SCE_ELSC from SRS_ELS
    v_out.level_code:=0;--SPR_LEVC from CAM_LEV
    v_out.fee_profile:='';--SCE_FPTC
    v_out.occurrence:='A';--SCE_OCCL
    v_out.next_occurrence:='A';--SCE_NOCL
    RETURN v_out;
  END CourseCOAH;
 
  FUNCTION CourseAccountingJanEntry return Course AS
  v_out Course;
  BEGIN
    --v_out:=CourseBasicDetails('ROAA1DX'||'DDACCM');
--    SCE_PRGC	SCE_ROUC 	SCE_DPTC
--    'BAHA3XX'	'XPOIAHID'	'POLS'
    v_out:=CourseBasicDetails('BSHB3JXXAFAJ');
    --v_out:=CourseBasicDetails('ROAICWX'||'DROAICWM');
    --BSXS2IX	RBISS
    v_out.cblock:='1';--SCE_BLOK
    v_out.dept:='SOMA';
    v_out.fee:=0;--FDU_AMNT
    v_out.fee_status:='PHU1N';--SCE_FSTC
    v_out.fee_status_overseas:='POU1N';--SCE_FSTC
    v_out.loc_of_study:='PRK';--SCE_ELSC from SRS_ELS
    v_out.level_code:=0;--SPR_LEVC from CAM_LEV
    v_out.fee_profile:='';--SCE_FPTC
    v_out.occurrence:='A';--SCE_OCCL
    v_out.next_occurrence:='A';--SCE_NOCL
    RETURN v_out;
  END CourseAccountingJanEntry;
 
  FUNCTION GetPersonaExamBoardName(Is_Postgrad in int,YearOffset in int,Is_Frozen in int, Is_Unpub in int, Is_Resit in int) return INTUIT.SRS_RDS.RDS_INCC%Type as
  v_ret VarChar2(20);
  BEGIN
    v_ret:='PER_';
    IF (Is_Postgrad<>0) then 
        v_ret:=v_ret||'P_';
    ELSE
        v_ret:=v_ret||'U_';
    END IF;
    v_ret:=v_ret||TO_CHAR(YearOffset);
    IF (Is_Resit<>0) then 
        v_ret:=v_ret||'_R_';
    ELSE
        v_ret:=v_ret||'_O_';
    END IF;
    IF (Is_Unpub<>0) then 
        IF (Is_Frozen<>0) then 
           v_ret:=v_ret||'F';
        ELSE
           v_ret:=v_ret||'U';
        END IF;
    ELSE
        v_ret:=v_ret||'P';
    END IF;
    return v_ret;
  END GetPersonaExamBoardName;
  
  PROCEDURE InsertFakeExamBoard(Base_Date in Date,Is_Postgrad in int,YearOffset in int,Is_Frozen in int, Is_Unpub in int, Is_Resit in int) as
  Board_Type VarChar2(4);
  Board_Name VarChar2(20);
  BEGIN
    IF (Is_Postgrad<>0) then 
        Board_Type:='PGT';
    ELSE
        Board_Type:='UGT';
    END IF;
    
    Board_Name:=GetPersonaExamBoardName(Is_Postgrad,YearOffset,Is_Frozen, Is_Unpub,Is_Resit);
      Insert into srs_ice (ICE_INCC,ICE_EVNO,ICE_EREF,ICE_ETYC,ICE_MEED,ICE_MEES,ICE_AGDS,ICE_BEGT,ICE_EENT,ICE_ROMC,ICE_ROMS,ICE_NOTE) values (Board_Name,'0010',null,Board_Type||'BRD',Base_Date,'Y','O',null,null,null,null,'This is a TEST only board, do not use.');
      Insert into srs_ice (ICE_INCC,ICE_EVNO,ICE_EREF,ICE_ETYC,ICE_MEED,ICE_MEES,ICE_AGDS,ICE_BEGT,ICE_EENT,ICE_ROMC,ICE_ROMS,ICE_NOTE) values (Board_Name,'0020',null,Board_Type||'PUB',add_months(Base_Date,Is_Unpub*240-1),'Y','O',null,null,null,null,'This is a TEST only board, do not use.');
      Insert into srs_ice (ICE_INCC,ICE_EVNO,ICE_EREF,ICE_ETYC,ICE_MEED,ICE_MEES,ICE_AGDS,ICE_BEGT,ICE_EENT,ICE_ROMC,ICE_ROMS,ICE_NOTE) values (Board_Name,'0030',null,Board_Type||'FRS',add_months(Base_Date,-1),'Y','O',null,null,null,null,'This is a TEST only board, do not use.');
      Insert into srs_ice (ICE_INCC,ICE_EVNO,ICE_EREF,ICE_ETYC,ICE_MEED,ICE_MEES,ICE_AGDS,ICE_BEGT,ICE_EENT,ICE_ROMC,ICE_ROMS,ICE_NOTE) values (Board_Name,'0040',null,Board_Type||'FRE',add_months(Base_Date,Is_Frozen*240-1),'Y','O',null,null,null,null,'This is a TEST only board, do not use.');
      Insert into srs_ice (ICE_INCC,ICE_EVNO,ICE_EREF,ICE_ETYC,ICE_MEED,ICE_MEES,ICE_AGDS,ICE_BEGT,ICE_EENT,ICE_ROMC,ICE_ROMS,ICE_NOTE) values (Board_Name,'0090',null,'ACT_YR',Base_Date,'Y','O',null,null,null,null,'This is a TEST only board, do not use.');
	  if (Is_Resit<>0) then
		Insert into srs_ice (ICE_INCC,ICE_EVNO,ICE_EREF,ICE_ETYC,ICE_MEED,ICE_MEES,ICE_AGDS,ICE_BEGT,ICE_EENT,ICE_ROMC,ICE_ROMS,ICE_NOTE) values (Board_Name,'0100',null,Board_Type||'SUP',Base_Date,'Y','O',null,null,null,null,'This is a TEST only board, do not use.');
	  end if;
  END InsertFakeExamBoard;
  
  PROCEDURE InsertFakeExamBoards(YearOffset in int,Base_Date in Date) as
  BEGIN
      InsertFakeExamBoard(Base_Date, 0, YearOffset, 0,0,0); InsertFakeExamBoard(Base_Date, 0, YearOffset, 0,0,1);
      InsertFakeExamBoard(Base_Date, 0, YearOffset, 0,1,0); InsertFakeExamBoard(Base_Date, 0, YearOffset, 0,1,1);
      InsertFakeExamBoard(Base_Date, 0, YearOffset, 1,1,0); InsertFakeExamBoard(Base_Date, 0, YearOffset, 1,1,1);
      
      InsertFakeExamBoard(Base_Date, 1, YearOffset, 0,0,0); InsertFakeExamBoard(Base_Date, 1, YearOffset, 0,0,1);
      InsertFakeExamBoard(Base_Date, 1, YearOffset, 0,1,0); InsertFakeExamBoard(Base_Date, 1, YearOffset, 0,1,1);
      InsertFakeExamBoard(Base_Date, 1, YearOffset, 1,1,0); InsertFakeExamBoard(Base_Date, 1, YearOffset, 1,1,1);
  END InsertFakeExamBoards;
  
  Procedure RegenerateFakeExamBoards AS
  v_Board_Type varchar2(3);
  BEGIN
      DELETE srs_ice where ICE_INCC like 'PER_%';
      InsertFakeExamBoards(0,SYSDATE);
      InsertFakeExamBoards(-1,ADD_MONTHS(SYSDATE,-12));
  END RegenerateFakeExamBoards; 
    
  PROCEDURE AssignToFakeExamBoard(i_stu_code INTUIT.SRS_SQE.SQE_STUC%Type,Is_Postgrad in int,YearOffset in int,Is_Frozen in int, Is_Unpub in int, Is_Resit in int) As
  v_exam_board INTUIT.SRS_RDS.RDS_INCC%Type;
  BEGIN
    v_exam_board:=GetPersonaExamBoardName(Is_Postgrad,YearOffset,Is_Frozen, Is_Unpub,Is_Resit);
    UPDATE srs_rds
    set RDS_INCC=v_exam_board
    where RDS_STUC=i_stu_code;
  END AssignToFakeExamBoard;
  
  PROCEDURE AssignGradeToModule(i_stu_scjc INTUIT.ins_smr.spr_code%type, i_mod_code INTUIT.ins_smr.MOD_CODE%type, i_grade INTUIT.ins_smr.smr_actg%type, i_mark INTUIT.ins_smr.smr_actm%type) AS
  BEGIN
    UPDATE ins_smr 
       SET smr_actm=i_mark,
           smr_actg=i_grade
           where spr_code = i_stu_scjc
            and MOD_CODE=i_mod_code;
  END AssignGradeToModule;
  
  PROCEDURE AssignGradeToYear(i_stu_scjc INTUIT.ins_smr.spr_code%type, i_ayr INTUIT.INS_SMR.AYR1_CODE%TYPE, i_grade INTUIT.ins_smr.smr_actg%type, i_mark INTUIT.ins_smr.smr_actm%type) AS
  BEGIN
    UPDATE ins_smr 
       SET smr_actm=i_mark,
           smr_actg=i_grade
           where spr_code = i_stu_scjc
            and AYR1_CODE=i_ayr;
  END AssignGradeToYear;
  
  PROCEDURE FreezeStudentMarks(i_spr_code in INTUIT.ins_smr.spr_code%type, i_ayr INTUIT.INS_SMR.AYR1_CODE%TYPE) as
  Is_Board_Unpub int;
  Is_Board_Resit int;
  BEGIN
    SELECT EXAMSYS.FREEZE.AreStudentResultsUnpublished(i_spr_code,i_ayr) into Is_Board_Unpub from dual;
    SELECT EXAMSYS.FREEZE.AreStudentResultsResits(i_spr_code,i_ayr) into Is_Board_Resit from dual;
    UPDATE ins_smr smr
        SET smr.smr_udf2 = smr.smr_actm,
            smr.smr_udf3 = smr.smr_actg,
            SMR.SMR_UDF4 = TO_CHAR (SYSDATE, 'DDMMYY HH24:MI:SS'),
            SMR.SMR_UDFI = smr.smr_cred
      WHERE smr.spr_code = i_spr_code;-- AND smr.ayr_code = vayr;
    
     -- UPDATE CAM_SAS
    UPDATE cam_sas sas
        SET sas.sas_udfi = sas.sas_actm,
            sas.sas_udfj = sas.sas_actg,
            SAS.sas_UDFK = TO_CHAR (SYSDATE, 'DDMMYY HH24:MI:SS')
      WHERE sas.spr_code = i_spr_code;-- AND sas.ayr_code = vayr;
    
     -- UPDATE CAM_SRA
    UPDATE cam_sra sra
        SET sra.sra_udfi = sra.sra_actm,
            sra.sra_udfj = sra.sra_actg,
            SRA.sra_UDFK = TO_CHAR (SYSDATE, 'DDMMYY HH24:MI:SS')
      WHERE sra.spr_code = i_spr_code;-- AND sra.ayr_code = vayr;
      
      -- UPDATE SRS_SCE - Board progression, for transcript purposes.
     UPDATE srs_sce sce
        SET SCE.sce_udff =sce.sce_pgsc
        where sce.sce_scjc = i_spr_code;
  END FreezeStudentMarks;
    
  PROCEDURE PublishStudentMarks(i_spr_code in INTUIT.ins_smr.spr_code%type) as
  BEGIN
    UPDATE ins_smr smr
        SET smr.smr_agrm = smr.smr_actm,
            smr.smr_agrg = smr.smr_actg,
            SMR.SMR_UDFI = smr.smr_cred
      WHERE smr.spr_code = i_spr_code;-- AND smr.ayr_code = vayr;
    
     -- UPDATE CAM_SAS
    UPDATE cam_sas sas
        SET sas.sas_agrm = sas.sas_actm,
            sas.sas_agrg = sas.sas_actg
      WHERE sas.spr_code = i_spr_code;-- AND sas.ayr_code = vayr;
    
     -- UPDATE CAM_SRA
    UPDATE cam_sra sra
        SET sra.sra_agrm = sra.sra_actm,
            sra.sra_agrg = sra.sra_actg
      WHERE sra.spr_code = i_spr_code;-- AND sra.ayr_code = vayr;
  END PublishStudentMarks;  
    
  PROCEDURE LazyPassYear(i_stu_scjc INTUIT.SRS_SCJ.SCJ_CODE%TYPE, i_ayr INTUIT.INS_SMR.AYR1_CODE%TYPE) AS
  BEGIN
    AssignGradeToYear(i_stu_scjc, i_ayr,'P','89'); FreezeStudentMarks(i_stu_scjc,i_ayr); --Freeze period marks
    AssignGradeToYear(i_stu_scjc, i_ayr,'P','90'); PublishStudentMarks(i_stu_scjc);--Final marks
    AssignGradeToYear(i_stu_scjc, i_ayr,'P','88'); -- prepublish, non freeze period marks
  END LazyPassYear;  
    
  FUNCTION StudentUK(i_stu_code INTUIT.INS_STU.STU_CODE%type, i_stu_gender INTUIT.INS_STU.STU_GEND%type, i_stu_title INTUIT.INS_STU.STU_TITL%type, i_stu_forname INTUIT.INS_STU.STU_FNM1%type, i_stu_midname INTUIT.INS_STU.STU_FNM2%type, i_stu_surname INTUIT.INS_STU.STU_SURN%type, i_stu_age int) return Student AS
  v_out Student;
  BEGIN
	v_out.code:=i_stu_code;
    v_out.scjc:=i_stu_code||'/1';
    v_out.title:=i_stu_title;
    v_out.forname:=i_stu_forname;
    v_out.midname:=i_stu_midname;
    v_out.surname:=i_stu_surname;
    v_out.initials:=SUBSTR(i_stu_forname,0,1)||SUBSTR(i_stu_midname,0,1);
    v_out.fullname:=REPLACE(i_stu_forname||' '||i_stu_midname||' '||i_stu_surname,'  ',' ');
    v_out.srtn:='TESTUSER    RP';--Sortname, used by SiTS
    v_out.gender:=i_stu_gender;--STU_GEND from SRS_GEN
    v_out.maritial_status:='NK';--STU_MARC from SRS_MAR
	v_out.age:=i_stu_age;--The age of the student, used in calculations
    v_out.birthday:=DateOnly(MoveDateToYearOffset(ADD_MONTHS(SYSDATE,-1),-v_out.age));--STU_DOB
    v_out.nationality:='000';--STU_NATC from SRS_NAT
    v_out.country_of_birth:='000';--STU_COBC from SRS_COB
    v_out.ethnic_origin:='15';--STU_ETHC from SRS_ETH
    v_out.lea:='516';--STU_LEAC from SRS_LEA
    v_out.disability:='A';--STU_DSBC from SRS_DSB
    v_out.highest_qual:='35';--STU_HQLC from SRS_HQL
    v_out.prev_school:='10797';--STU_SCLC from SRS_SCL
    v_out.home_overseas:='H';--STU_UHOC from SRS_UHO
    v_out.ext_id:='0000021185706';--STU_ESID
    v_out.domicile_code:='516';--STU_CODC from SRS_COD
    v_out.speaks_welsh:='2';--STU_WELS (1=fluent,2=nonfluent,3=not,9=unknown)
    v_out.national_id:='W';--STU_NID1 from W-=Welsh, O=Other
    v_out.pref_welsh_comm:='Y';--get comms in Welsh?
    v_out.uk_vi:='N';--STU_UKBA Report to UKVI?
    v_out.religion:='98';--STU_RELB 98=Info refused
    v_out.sex_orient:='98';--STU_SXOR 98=Info refused
    v_out.gender_id:='98';--STU_GNID 98=info refused
    v_out.welsh_mentor_pref:='N';
    v_out.gp_location:='UNIVERSITY';
    v_out.dependants:=0;
    v_out.car_access:='N';
    v_out.prev_care:='None';
    RETURN v_out;
  END StudentUK;

  FUNCTION StudentFrench(i_stu_code INTUIT.INS_STU.STU_CODE%type, i_stu_gender INTUIT.INS_STU.STU_GEND%type, i_stu_title INTUIT.INS_STU.STU_TITL%type, i_stu_forname INTUIT.INS_STU.STU_FNM1%type, i_stu_midname INTUIT.INS_STU.STU_FNM2%type, i_stu_surname INTUIT.INS_STU.STU_SURN%type, i_stu_age int) return Student AS
  v_out Student;
  BEGIN
	v_out.code:=i_stu_code;
    v_out.scjc:=i_stu_code||'/1';
    v_out.title:=i_stu_title;
    v_out.forname:=i_stu_forname;
    v_out.midname:=i_stu_midname;
    v_out.surname:=i_stu_surname;
    v_out.initials:=SUBSTR(i_stu_forname,0,1)||SUBSTR(i_stu_midname,0,1);
    v_out.fullname:=REPLACE(i_stu_forname||' '||i_stu_midname||' '||i_stu_surname,'  ',' ');
    v_out.srtn:='TESTUSER    RP';--Sortname, used by SiTS
    v_out.gender:=i_stu_gender;--STU_GEND from SRS_GEN
    v_out.maritial_status:='NK';--STU_MARC from SRS_MAR
	v_out.age:=i_stu_age;--The age of the student, used in calculations
    v_out.birthday:=DateOnly(MoveDateToYearOffset(ADD_MONTHS(SYSDATE,-1),-v_out.age));--STU_DOB
    v_out.nationality:='653';--STU_NATC from SRS_NAT
    v_out.country_of_birth:='653';--STU_COBC from SRS_COB
    v_out.ethnic_origin:='10';--STU_ETHC from SRS_ETH
    v_out.lea:=null;--STU_LEAC from SRS_LEA
    v_out.disability:='A';--STU_DSBC from SRS_DSB
    v_out.highest_qual:='12';--STU_HQLC from SRS_HQL
    v_out.prev_school:=null;--STU_SCLC from SRS_SCL
    v_out.home_overseas:='H';--STU_UHOC from SRS_UHO
    v_out.ext_id:='1611809112477';--STU_ESID
    v_out.domicile_code:='653';--STU_CODC from SRS_COD
    v_out.speaks_welsh:='3';--STU_WELS (1=fluent,2=nonfluent,3=not,9=unknown)
    v_out.national_id:='O';--STU_NID1 from W-=Welsh, O=Other
    v_out.pref_welsh_comm:='N';--get comms in Welsh?
    v_out.uk_vi:='N';--STU_UKBA Report to UKVI?
    v_out.religion:='98';--STU_RELB 98=Info refused
    v_out.sex_orient:='98';--STU_SXOR 98=Info refused
    v_out.gender_id:='98';--STU_GNID 98=info refused
    v_out.welsh_mentor_pref:='N';
    v_out.gp_location:='UNIVERSITY';
    v_out.dependants:=0;
    v_out.car_access:='N';
    v_out.prev_care:='None';
    RETURN v_out;
  END StudentFrench;


  FUNCTION StudentChinese(i_stu_code INTUIT.INS_STU.STU_CODE%type, i_stu_gender INTUIT.INS_STU.STU_GEND%type, i_stu_title INTUIT.INS_STU.STU_TITL%type, i_stu_forname INTUIT.INS_STU.STU_FNM1%type, i_stu_midname INTUIT.INS_STU.STU_FNM2%type, i_stu_surname INTUIT.INS_STU.STU_SURN%type, i_stu_age int) return Student AS
  v_out Student;
  BEGIN
	v_out.code:=i_stu_code;
    v_out.scjc:=i_stu_code||'/1';
    v_out.title:=i_stu_title;
    v_out.forname:=i_stu_forname;
    v_out.midname:=i_stu_midname;
    v_out.surname:=i_stu_surname;
    v_out.initials:=SUBSTR(i_stu_forname,0,1)||SUBSTR(i_stu_midname,0,1);
    v_out.fullname:=REPLACE(i_stu_forname||' '||i_stu_midname||' '||i_stu_surname,'  ',' ');
    v_out.srtn:='TESTUSER     P';--Sortname, used by SiTS
    v_out.gender:=i_stu_gender;--STU_GEND from SRS_GEN
    v_out.maritial_status:='NK';--STU_MARC from SRS_MAR
	v_out.age:=i_stu_age;--The age of the student, used in calculations
    v_out.birthday:=DateOnly(MoveDateToYearOffset(ADD_MONTHS(SYSDATE,-1),-v_out.age));--STU_DOB
    v_out.nationality:='631';--STU_NATC from SRS_NAT
    v_out.country_of_birth:='774';--STU_COBC from SRS_COB
    v_out.ethnic_origin:='34';--STU_ETHC from SRS_ETH
    v_out.lea:='';--STU_LEAC from SRS_LEA
    v_out.disability:='A';--STU_DSBC from SRS_DSB
    v_out.highest_qual:='93';--STU_HQLC from SRS_HQL
    v_out.prev_school:='';--STU_SCLC from SRS_SCL
    v_out.home_overseas:='O';--STU_UHOC from SRS_UHO
    v_out.ext_id:='';--STU_ESID
    v_out.domicile_code:='516';--STU_CODC from SRS_COD
    v_out.speaks_welsh:='3';--STU_WELS (1=fluent,2=nonfluent,3=not,9=unknown)
    v_out.national_id:='O';--STU_NID1 from W-=Welsh, O=Other
    v_out.pref_welsh_comm:='N';--get comms in Welsh?
    v_out.uk_vi:='Y';--Report to UKVI?
    v_out.religion:='98';--STU_RELB 98=Info refused
    v_out.sex_orient:='98';--STU_SXOR 98=Info refused
    v_out.gender_id:='98';--STU_GNID 98=info refused
    v_out.welsh_mentor_pref:='N';
    v_out.gp_location:='UNIVERSITY';
    v_out.dependants:=0;
    v_out.car_access:='N';
    v_out.prev_care:='None';
    RETURN v_out;
  END StudentChinese;
  
  FUNCTION StudentNewCode(i_stu Student, i_ayr Ayr, i_scj_count int) return Student AS
  v_out Student;
  BEGIN 
    v_out:=i_stu;
    v_out.code:=SUBSTR(to_char(i_ayr.yr),-2)||i_stu.code;
    v_out.scjc:=v_out.code||'/'||to_char(i_scj_count);
    RETURN v_out;
  END StudentNewCode;
  
  PROCEDURE AddFakeDoc(i_stu Student,i_doc_type INTUIT.MEN_DOC.DOC_DTYC%type,i_created Date, i_entity_code INTUIT.MEN_DRR.DRR_ENTC%type, i_suffix_char varchar) AS
  v_ctime Date;
  v_cdate Date;
  v_docu  INTUIT.MEN_DOC.DOC_DOCU%type;
  v_size  INTUIT.MEN_DOC.DOC_SIZE%type;
  BEGIN
      v_ctime:=TimeOnly(i_created);
      v_cdate:=DateOnly(i_created);
    select DOC_DOCU,DOC_SIZE into v_docu,v_size from INTUIT.MEN_DOC where DOC_CODE = '474825-V1' and rownum=1;
    Insert into INTUIT.MEN_DRR
       (DRR_DOCC, DRR_DCTC, DRR_ENTC, DRR_PKFV, DRR_PKF1, 
        DRR_PKF2, DRR_PKF3, DRR_MSTC, DRR_MRCC, DRR_CODE)
     Values
       (i_stu.code||'-'||i_suffix_char||'1', 'SRS', i_entity_code, i_stu.code||'~1', i_stu.code, 
        '1', '1', i_stu.code, 'STU', i_stu.code);    
    Insert into INTUIT.MEN_DOC
       (DOC_CODE, DOC_NAME, DOC_DTYC, DOC_IUSE, DOC_FILE, 
        DOC_FEXC, DOC_CRED, DOC_CRET, DOC_MST1, 
        DOC_UPDD, DOC_UPDT, DOC_MST2, DOC_DOCU, DOC_SIZE)
     Values
       (i_stu.code||'-'||i_suffix_char||'1', 'Test_Data', i_doc_type, 'Y', i_stu.code||'-'||i_suffix_char||'1.jpg', 
        'JPG', v_cdate, v_ctime, 'INTRANETTEST',
        v_cdate, v_ctime, 'INTRANETTEST',v_docu, v_size);
        
   
  END AddFakeDoc;

  PROCEDURE AddPassport(i_stu Student) AS
  BEGIN
    Insert into INTUIT.SRS_PPT
       (PPT_STUC, PPT_SEQN, PPT_PPTN, PPT_NAME, 
        PPT_ISSD, PPT_EXPD, 
        PPT_NATC, PPT_COBC, PPT_IUSE,
        PPT_VRFD, PPT_PRSC)
     Values
       (i_stu.code, 1, '508434366', i_stu.title||' '||i_stu.fullname, 
        add_months(sysdate,-3), add_months(sysdate,117), 
        i_stu.nationality, i_stu.country_of_birth, 'Y',
        add_months(sysdate,-1), '810057');
    AddFakeDoc(i_stu ,'PASSPORT' ,add_months(sysdate,-1), 'PPT', 'P');
  END AddPassport;
  
  PROCEDURE AddGraduationCeremony(i_stu Student, i_course Course, i_add ContactAddress, i_ayr INTUIT.SRS_CMY.CMY_AYRC%TYPE) AS
  BEGIN
    DELETE INTUIT.SRS_CMY where CMY_CODE='PERSONA_TEST';
    Insert into INTUIT.SRS_CMY (CMY_CODE,CMY_AYRC,CMY_PSLC,CMY_SNAM,CMY_NAME,
        CMY_DATE,CMY_STIM,CMY_CGST,CMY_USR2,CMY_IUSE) 
    values ('PERSONA_TEST',i_ayr,'TB1','CRASH','TEST RUN',
        DateOnly(add_months(sysdate,1)),TimeOnly(sysdate),1000,'OPEN','Y');
  
    Insert into SRS_SCY (SCY_STUC,SCY_SEQN,SCY_CMYS,SCY_ATTS,SCY_AWDS,SCY_CMYC,SCY_SNEED,SCY_GNEED,SCY_SCJC,SCY_AYRC,SCY_PSLC,
        SCY_TTLC,SCY_INIT,SCY_FNM1,SCY_FNM2,SCY_FNM3,SCY_SURN,SCY_SRTN,
        SCY_DPTC,SCY_CRSC,SCY_PRGC,SCY_ROUC,
        SCY_CAYN,SCY_CAD1,SCY_CAD2,SCY_CAD3,SCY_CAD4,SCY_CAD5,SCY_CAPC,SCY_CTEL,SCY_CAEM,SCY_SPAS,
        SCY_DISP,SCY_USR1,SCY_USR2,SCY_RESD,SCY_SPRC,SCY_PDEF,SCY_FACC) 
    values (i_stu.code,'001','N','I','N','PERSONA_TEST','N','N',i_stu.scjc,i_ayr,'TB1',
        i_stu.title,i_stu.initials,i_stu.forname,i_stu.midname,null,i_stu.surname,i_stu.srtn,
        i_course.dept,i_course.code,i_course.code,i_course.route,
        'N',i_add.add1,i_add.add2,i_add.add3,i_add.add4,i_add.add5,i_add.postcode,i_add.phone,i_add.email,'N',
        0,'Someday','9.30EM',null,i_stu.scjc,'N',i_course.faculty);   
  END AddGraduationCeremony;
  
  PROCEDURE AddDrivingLicence(i_stu Student) AS
  BEGIN
      Insert into INTUIT.SRS_SPD 
        (SPD_STUC,SPD_SEQN,SPD_PDTC,
        SPD_SRTN,SPD_USRC,SPD_UPDD,
        SPD_MSTC,SPD_UDFB,SPD_UDFC,SPD_UDFD,
        SPD_UDFE,SPD_UDFF,SPD_UDFG,SPD_UDFH)
      values 
        (i_stu.code,'1','PHOTO ID',
        i_stu.srtn,'INTUIT',add_months(sysdate,-1),
        'INTRANETTEST','DL','DE40012345',To_Char(add_months(sysdate,-5),'DD/MM/YYYY HH24:MI:SS'),
        To_Char(add_months(sysdate,120),'DD/MM/YYYY HH24:MI:SS'),To_Char(add_months(sysdate,-1),'DD/MM/YYYY HH24:MI:SS'),'810057',i_stu.nationality);
    AddFakeDoc(i_stu ,'PHOTO ID' ,add_months(sysdate,-1), 'SPD', 'PHOTO ID');
  END AddDrivingLicence;
  
  PROCEDURE AddVaildPhotoID (i_stu Student) AS 
  BEGIN
    AddDrivingLicence(i_stu);
    --Add Birth Certificate to same SPD
    AddFakeDoc(i_stu ,'BIRTH CERT' ,add_months(sysdate,-1), 'SPD', 'BIRTH CERT');
  END AddVaildPhotoID;

  PROCEDURE AddEuIdCard(i_stu Student) AS
  BEGIN
      Insert into INTUIT.SRS_SPD 
        (SPD_STUC,SPD_SEQN,SPD_PDTC,
        SPD_SRTN,SPD_USRC,SPD_UPDD,
        SPD_MSTC,SPD_UDFB,SPD_UDFC,SPD_UDFD,
        SPD_UDFE,SPD_UDFF,SPD_UDFG,SPD_UDFH,SPD_UDFI)
      values 
        (i_stu.code,'1','PHOTO ID',
        i_stu.srtn,'INTUIT',add_months(sysdate,-1),
        'INTRANETTEST','EUID','FR90012345',To_Char(add_months(sysdate,-5),'DD/MM/YYYY HH24:MI:SS'),
        To_Char(add_months(sysdate,120),'DD/MM/YYYY HH24:MI:SS'),To_Char(add_months(sysdate,-1),'DD/MM/YYYY HH24:MI:SS'),'810057',i_stu.nationality,i_stu.country_of_birth);
    AddFakeDoc(i_stu ,'PHOTO ID' ,add_months(sysdate,-1), 'SPD', 'PHOTO ID');
    --Add Birth Certificate to same SPD
    AddFakeDoc(i_stu ,'BIRTH CERT' ,add_months(sysdate,-1), 'SPD', 'BIRTH CERT');
  END AddEuIdCard;

  PROCEDURE AddVisa(i_stu Student) AS
  BEGIN
    Insert into INTUIT.SRS_VIS
       (VIS_STUC, VIS_PPTS, VIS_SEQN, VIS_VISN, VIS_IUSE, 
        VIS_ISSD, VIS_BEGD, VIS_EXPD, VIS_REND, VIS_VRFD, 
        VIS_PRSC, VIS_TYPE, VIS_SPYN)
     Values
       (i_stu.code, 1, 1, 'RE8289645', 'Y', 
        add_months(sysdate,-3), add_months(sysdate,-3), add_months(sysdate,18), add_months(sysdate,18), add_months(sysdate,-1),
        '810057', '4GS', 'N');
    AddFakeDoc(i_stu ,'VISAT4' ,add_months(sysdate,-1), 'VIS', 'V');
  END AddVisa;
  
  PROCEDURE AddValidateRtsPass(i_stu Student) AS
  BEGIN
    UPDATE srs_sce
    SET sce_udfg='Y'
    WHERE sce_stuc=i_stu.code;
  END AddValidateRtsPass;
  
  FUNCTION GetAyrs(i_course_yr int,i_course_duration int) return Ayrs AS
  v_out Ayrs;
  BEGIN
  --Need to get the current acedmic year and work out the previous years to populate the ayrs for compelted courses and qualifications
   	v_out.plus1.ayr:=To_Ayr(ADD_MONTHS(SYSDATE,+12));
    v_out.curr.ayr:=To_Ayr(SYSDATE);
    v_out.minus1.ayr:=To_Ayr(ADD_MONTHS(SYSDATE,-12));
    v_out.minus2.ayr:=To_Ayr(ADD_MONTHS(SYSDATE,-24));
    v_out.minus3.ayr:=To_Ayr(ADD_MONTHS(SYSDATE,-36));
	v_out.plus1.yr:=AyrEndYear(ADD_MONTHS(SYSDATE,+12));
    v_out.curr.yr:=EXTRACT(year from sysdate);
    v_out.minus1.yr:=EXTRACT(year from ADD_MONTHS(SYSDATE,-12));
    v_out.minus2.yr:=EXTRACT(year from ADD_MONTHS(SYSDATE,-24));
    v_out.minus3.yr:=EXTRACT(year from ADD_MONTHS(SYSDATE,-36));
 /*   v_out.curr.yr:=AyrEndYear(SYSDATE);
    v_out.minus1.yr:=AyrEndYear(ADD_MONTHS(SYSDATE,-12));
    v_out.minus2.yr:=AyrEndYear(ADD_MONTHS(SYSDATE,-24));
    v_out.minus3.yr:=AyrEndYear(ADD_MONTHS(SYSDATE,-36));*/
    v_out.crsstart.ayr:=To_Ayr(ADD_MONTHS(SYSDATE,-(12*i_course_yr)));
    --v_out.crsstart.yr:=AyrEndYear(ADD_MONTHS(SYSDATE,-(12*i_course_yr)));
    v_out.crsstart.yr:=EXTRACT(year from ADD_MONTHS(SYSDATE,-(12*i_course_yr)));
    v_out.crsend.ayr:=To_Ayr(ADD_MONTHS(SYSDATE,+(12*(i_course_duration-i_course_yr))));
    --v_out.crsend.yr:=AyrEndYear(ADD_MONTHS(SYSDATE,12*(i_course_duration-i_course_yr)));
    v_out.crsend.yr:=EXTRACT(year from ADD_MONTHS(SYSDATE,-(12*(i_course_duration-i_course_yr))));
    RETURN v_out;
  END GetAyrs;
  
  PROCEDURE CompleteCourseYear(i_stu Student, i_seq INTUIT.SRS_SCE.SCE_SEQ2%type,i_ayr INTUIT.SRS_SCE.SCE_AYRC%Type ) AS
  BEGIN
   UPDATE INTUIT.SRS_SCE SET SCE_STAC='E', SCE_PGSC='CL',SCE_RESC='P' WHERE SCE_SCJC=i_stu.scjc and SCE_SEQ2=i_seq and SCE_AYRC=i_ayr;     
  END CompleteCourseYear;
  
  
  PROCEDURE MakeNursingAssistant(i_stu Student, i_curr_ayr INTUIT.INS_AYR.AYR_CODE%type) AS
  BEGIN -- Should be a nursing assistant
   UPDATE INTUIT.INS_STU SET STU_OCBC='6111', STU_NREF=99999999 where STU_CODE=i_stu.code;
   UPDATE SRS_SCJ SET SCJ_COH3='S'||SUBSTR(i_curr_ayr,4,2)||'NA-S' WHERE SCJ_CODE=i_stu.scjc;
  END MakeNursingAssistant;
 
  PROCEDURE BuildPersonaUkNoPassport(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'M','Mr','Enzo','Matrix', 'Uk-No-Passport',18);
    v_course:=CourseCompSci();
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni,  v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'SLC');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus2.ayr);
    AddBobsALevels(v_stu, v_ayr.minus1.ayr, BuildDate(9,8,v_ayr.curr.yr));
    --No Passport
  END BuildPersonaUkNoPassport;
  
  PROCEDURE BuildPersonaUkPassport(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'M','Mr','Bob','', 'Uk-Pass',18);
    v_course:=CourseCompSci();
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni,  v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'SLC');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus2.ayr);
    AddBobsALevels(v_stu, v_ayr.minus1.ayr, BuildDate(9,8,v_ayr.curr.yr));
    AddPassport(v_stu);
  END BuildPersonaUkPassport;  
  
    PROCEDURE BuildPersonaEuFrench(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentFrench(i_stu_code,'M','Mr','C�cil','', 'Eu-French',18);
    v_course:=CourseCompSci();
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressFrance();
    v_sponsor_ref:='';
    v_ucas_num:='';
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni,  v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'SLC');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    -- No qualifications yet.
    AddEuIdCard(v_stu);
  END BuildPersonaEuFrench;  
  
  
  PROCEDURE BuildPersonaUkPhotoId(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'M','Mr','Mike','TV', 'Uk-Photo-ID',18);
    v_course:=CourseCompSci();
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni,  v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'SLC');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus2.ayr);
    AddBobsALevels(v_stu, v_ayr.minus1.ayr, BuildDate(9,8,v_ayr.curr.yr));
    AddVaildPhotoID(v_stu);
  END BuildPersonaUkPhotoId;  
  
  PROCEDURE BuildPersonaUkPostgradRet(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'F','Miss','Barbara','Gordon', 'Uk-PG-Returning',24);
    v_course:=CoursePostGrad();
    v_ayr:=GetAyrs(2,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';
    v_course.level_code:=8;
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni, v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, ADD_MONTHS(SYSDATE,-12), BuildDate(28,6,v_ayr.plus1.yr), v_stu.age-1, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.minus1.ayr, v_course, BuildDate(10,9,v_ayr.minus1.yr), v_ayr.plus1.yr, add_months(sysdate,-12), BuildDate(11,9,v_ayr.minus1.yr), BuildDate(1,1,v_ayr.minus1.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.minus1.ayr,BuildDate(1,11,v_ayr.minus1.yr),BuildDate(21,11,v_ayr.minus1.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.minus1.ayr,'SLC');
    CompleteCourseYear(v_stu,'01',v_ayr.minus1.ayr);
    v_course.cblock:='2'; -- 2nd year
    AddCourseEnrolment(v_stu, '02', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0002',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'02',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'SLC');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus3.ayr);
    AddBobsALevels(v_stu, v_ayr.minus2.ayr, BuildDate(9,8,v_ayr.minus1.yr));
    AddVaildPhotoID(v_stu);
  END BuildPersonaUkPostgradRet;
  
  PROCEDURE BuildPersonaUkNurseRet(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'F','Miss','Mercy','', 'Uk-Nurse-Ret',19);
    v_course:=CourseNursing();
    v_ayr:=GetAyrs(2,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';
    v_course.level_code:=2;
    
    AddBaseRecord(v_stu, null, v_add_home, v_add_uni, v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, ADD_MONTHS(SYSDATE,-12), BuildDate(28,6,v_ayr.plus1.yr), v_stu.age-1, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    MakeNursingAssistant(v_stu,v_ayr.curr.ayr); -- Should be a nursing assistant
    AddCourseEnrolment(v_stu, '01', v_ayr.minus1.ayr, v_course, BuildDate(10,9,v_ayr.minus1.yr), v_ayr.plus1.yr, add_months(sysdate,-12), BuildDate(11,9,v_ayr.minus1.yr), BuildDate(1,1,v_ayr.minus1.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.minus1.ayr,BuildDate(1,11,v_ayr.minus1.yr),BuildDate(21,11,v_ayr.minus1.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.minus1.ayr,'SLC');
    CompleteCourseYear(v_stu,'01',v_ayr.minus1.ayr);
    v_course.cblock:='2'; -- 2nd year
    AddCourseEnrolment(v_stu, '02', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0002',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'02',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'SLC');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus3.ayr);
    AddBobsALevels(v_stu, v_ayr.minus2.ayr, BuildDate(9,8,v_ayr.minus1.yr));
    AddVaildPhotoID(v_stu);
  END BuildPersonaUkNurseRet;
  
  PROCEDURE BuildPersonaUkShlsRet(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'F','Miss','Rinoa','Heartilly', 'Uk-Shls-Ret',19);
    v_course:=CourseHealthScienceAssociate();
    v_ayr:=GetAyrs(2,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';
    v_course.level_code:=2;
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni, v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, ADD_MONTHS(SYSDATE,-12), BuildDate(28,6,v_ayr.plus1.yr), v_stu.age-1, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    MakeNursingAssistant(v_stu,v_ayr.curr.ayr); -- Should be a nursing assistant
    AddCourseEnrolment(v_stu, '01', v_ayr.minus1.ayr, v_course, BuildDate(10,9,v_ayr.minus1.yr), v_ayr.plus1.yr, add_months(sysdate,-12), BuildDate(11,9,v_ayr.minus1.yr), BuildDate(1,1,v_ayr.minus1.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.minus1.ayr,BuildDate(1,11,v_ayr.minus1.yr),BuildDate(21,11,v_ayr.minus1.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.minus1.ayr,'SLC');
    CompleteCourseYear(v_stu,'01',v_ayr.minus1.ayr);
    v_course.cblock:='2'; -- 2nd year
    AddCourseEnrolment(v_stu, '02', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0002',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'02',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'SLC');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus3.ayr);
    AddBobsALevels(v_stu, v_ayr.minus2.ayr, BuildDate(9,8,v_ayr.minus1.yr));
    AddVaildPhotoID(v_stu);
  END BuildPersonaUkShlsRet;
  
  PROCEDURE BuildPersonaUkRetNoId(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
    o_modules SYS_REFCURSOR;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'M','Mr','Wally','', 'Uk-Ret-No-Id',19);
    v_course:=CourseCompSci();
    v_ayr:=GetAyrs(2,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni, v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, ADD_MONTHS(SYSDATE,-12), BuildDate(28,6,v_ayr.plus1.yr), v_stu.age-1, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);

    ModulesCompSci(v_ayr.minus1.ayr, v_ayr.minus1.ayr,v_course.level_code, o_modules);
    SelectCourseModules(v_stu , o_modules, v_ayr.minus1.ayr, v_ayr.minus1.ayr,  v_course.level_code);
    EnrolStudentOnCourse(v_stu, v_course, v_ayr.minus1);
    LazyPassYear(v_stu.scjc, v_ayr.minus1.ayr);
    AssignToFakeExamBoard(v_stu.code,v_course.is_postgrad,-1,0,0,0);
    v_course.level_code:=2;
    ModulesCompSci(v_ayr.curr.ayr, v_ayr.curr.ayr,v_course.level_code, o_modules);
    SelectCourseModules(v_stu , o_modules, v_ayr.curr.ayr, v_ayr.curr.ayr,  v_course.level_code);
    AddCourseEnrolment(v_stu, '01', v_ayr.minus1.ayr, v_course, BuildDate(10,9,v_ayr.minus1.yr), v_ayr.plus1.yr, add_months(sysdate,-12), BuildDate(11,9,v_ayr.minus1.yr), BuildDate(1,1,v_ayr.minus1.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.minus1.ayr,BuildDate(1,11,v_ayr.minus1.yr),BuildDate(21,11,v_ayr.minus1.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.minus1.ayr,'SLC');
    CompleteCourseYear(v_stu,'01',v_ayr.minus1.ayr);
    v_course.cblock:='2'; -- 2nd year
    AddCourseEnrolment(v_stu, '02', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0002',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'02',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'SLC');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus3.ayr);
    AddBobsALevels(v_stu, v_ayr.minus2.ayr, BuildDate(9,8,v_ayr.minus1.yr));
    --No Passport
  END BuildPersonaUkRetNoId;
  
  PROCEDURE BuildPersonaUkReturning(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
    o_modules SYS_REFCURSOR;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'F','Miss','Dot','Matrix', 'Uk-Returning',19);
    v_course:=CourseCompSci();
    v_ayr:=GetAyrs(2,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';

    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni, v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, ADD_MONTHS(SYSDATE,-12), BuildDate(28,6,v_ayr.plus1.yr), v_stu.age-1, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);

    ModulesCompSci(v_ayr.minus1.ayr, v_ayr.minus1.ayr,v_course.level_code, o_modules);
    SelectCourseModules(v_stu , o_modules, v_ayr.minus1.ayr, v_ayr.minus1.ayr,  v_course.level_code);
    EnrolStudentOnCourse(v_stu, v_course, v_ayr.minus1);
    LazyPassYear(v_stu.scjc, v_ayr.minus1.ayr);
    AssignToFakeExamBoard(v_stu.code,v_course.is_postgrad,-1,0,0,0);
    v_course.level_code:=2;
    ModulesCompSci(v_ayr.curr.ayr, v_ayr.curr.ayr,v_course.level_code, o_modules);
    SelectCourseModules(v_stu , o_modules, v_ayr.curr.ayr, v_ayr.curr.ayr,  v_course.level_code);
    AddCourseEnrolment(v_stu, '01', v_ayr.minus1.ayr, v_course, BuildDate(10,9,v_ayr.minus1.yr), v_ayr.plus1.yr, add_months(sysdate,-12), BuildDate(11,9,v_ayr.minus1.yr), BuildDate(1,1,v_ayr.minus1.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.minus1.ayr,BuildDate(1,11,v_ayr.minus1.yr),BuildDate(21,11,v_ayr.minus1.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.minus1.ayr,'SLC');
    --SelectCourseModules(v_stu, i_modules, v_ayr.minus1.ayr, v_ayr.minus1.ayr, '1');
    CompleteCourseYear(v_stu,'01',v_ayr.minus1.ayr);

    v_course.cblock:='2'; -- 2nd year
    AddCourseEnrolment(v_stu, '02', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0002',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'02',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'SLC');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus3.ayr);
    AddBobsALevels(v_stu, v_ayr.minus2.ayr, BuildDate(9,8,v_ayr.minus1.yr));
    AddVaildPhotoID(v_stu);
  END BuildPersonaUkReturning;
 
 
   PROCEDURE BuildPersonaUkNoPathway(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
    o_modules SYS_REFCURSOR;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'F','Dr','Marcus','Brody', 'NoPathway',19);
    v_course:=CourseNoPathway();
    v_ayr:=GetAyrs(2,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';

    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni, v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, ADD_MONTHS(SYSDATE,-12), BuildDate(28,6,v_ayr.plus1.yr), v_stu.age-1, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);

    ModulesCompSci(v_ayr.minus1.ayr, v_ayr.minus1.ayr,v_course.level_code, o_modules);
    SelectCourseModules(v_stu , o_modules, v_ayr.minus1.ayr, v_ayr.minus1.ayr,  v_course.level_code);
    EnrolStudentOnCourse(v_stu, v_course, v_ayr.minus1);
    LazyPassYear(v_stu.scjc, v_ayr.minus1.ayr);
    AssignToFakeExamBoard(v_stu.code,v_course.is_postgrad,-1,0,0,0);
    v_course.level_code:=2;
    ModulesCompSci(v_ayr.curr.ayr, v_ayr.curr.ayr,v_course.level_code, o_modules);
    SelectCourseModules(v_stu , o_modules, v_ayr.curr.ayr, v_ayr.curr.ayr,  v_course.level_code);
    AddCourseEnrolment(v_stu, '01', v_ayr.minus1.ayr, v_course, BuildDate(10,9,v_ayr.minus1.yr), v_ayr.plus1.yr, add_months(sysdate,-12), BuildDate(11,9,v_ayr.minus1.yr), BuildDate(1,1,v_ayr.minus1.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.minus1.ayr,BuildDate(1,11,v_ayr.minus1.yr),BuildDate(21,11,v_ayr.minus1.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.minus1.ayr,'SLC');
    --SelectCourseModules(v_stu, i_modules, v_ayr.minus1.ayr, v_ayr.minus1.ayr, '1');
    CompleteCourseYear(v_stu,'01',v_ayr.minus1.ayr);

    v_course.cblock:='2'; -- 2nd year
    AddCourseEnrolment(v_stu, '02', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0002',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'02',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'SLC');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus3.ayr);
    AddBobsALevels(v_stu, v_ayr.minus2.ayr, BuildDate(9,8,v_ayr.minus1.yr));
    AddVaildPhotoID(v_stu);
    UPDATE CAM_SMO SET SMO_UDF2='BSHS3XXXCSOS' WHERE SPR_CODE=v_stu.scjc AND AYR_CODE=v_ayr.curr.ayr;
  END BuildPersonaUkNoPathway;
 
 PROCEDURE BuildPersonaCOAH(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
    o_modules SYS_REFCURSOR;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'F','Miss','COAH','Student', 'COAH Student',19);
    v_course:=CourseCOAH();
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';

    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni,  v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'MKB511');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    --Qualifications --not listed for now.
    AddPassport(v_stu);
    AddVisa(v_stu);
    AddValidateRtsPass(v_stu);
  END BuildPersonaCOAH;
  
 PROCEDURE BuildPersonaAccountingJanEntry(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
    o_modules SYS_REFCURSOR;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'F','Miss','Accounting','Student', 'Accounting Student',19);
    v_course:=CourseAccountingJanEntry();
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';

    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni,  v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.ayr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddCourseEnrolment(v_stu, '02', v_ayr.plus1.ayr, v_course, BuildDate(10,9,v_ayr.plus1.yr), v_ayr.plus1.ayr, sysdate, BuildDate(11,9,v_ayr.plus1.yr), BuildDate(1,1,v_ayr.plus1.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'MKB511');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    --Qualifications --not listed for now.
    AddPassport(v_stu);
    AddVisa(v_stu);
    AddValidateRtsPass(v_stu);
  END BuildPersonaAccountingJanEntry;
 
  PROCEDURE BuildPersonaIntReturning(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentChinese(i_stu_code,'M','Mr','Lo','Pan', 'Int-Returning',18);
    v_course:=CourseCompSci();
    v_course.fee_status:=v_course.fee_status_overseas;
    v_ayr:=GetAyrs(2,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressChina();
    v_sponsor_ref:='';
    v_ucas_num:='';
    v_course.level_code:=2;
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni, v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, ADD_MONTHS(SYSDATE,-12), BuildDate(28,6,v_ayr.plus1.yr), v_stu.age-1, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.minus1.ayr, v_course, BuildDate(10,9,v_ayr.minus1.yr), v_ayr.plus1.yr, add_months(sysdate,-12), BuildDate(11,9,v_ayr.minus1.yr), BuildDate(1,1,v_ayr.minus1.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.minus1.ayr,BuildDate(1,11,v_ayr.minus1.yr),BuildDate(21,11,v_ayr.minus1.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.minus1.ayr,'MKB511');
    CompleteCourseYear(v_stu,'01',v_ayr.minus1.ayr);
    v_course.cblock:='2'; -- 2nd year
    AddCourseEnrolment(v_stu, '02', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0002',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'02',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'MKB511');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    --Qualifications --not listed for now.
    AddPassport(v_stu);
    AddVisa(v_stu);
    AddValidateRtsPass(v_stu);
  END BuildPersonaIntReturning;
  
  PROCEDURE BuildPersonaUkNurse(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'F','Miss','Joy','', 'Uk-Nurse',18);
    v_course:=CourseNursing();
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';
    
    AddBaseRecord(v_stu, null, v_add_home, v_add_uni, v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    MakeNursingAssistant(v_stu,v_ayr.curr.ayr); -- Should be a nursing assistant
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    --AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'SLC');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus2.ayr);
    AddBobsALevels(v_stu, v_ayr.minus1.ayr, BuildDate(9,8,v_ayr.curr.yr));
    AddVaildPhotoID(v_stu);
  END BuildPersonaUkNurse;

  PROCEDURE BuildPersonaUkShlsAssociate(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'F','Miss','Aerith','Gainsborough', 'Uk-SHLS-Associate',18);
    v_course:=CourseHealthScienceAssociate();
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';
    
    AddBaseRecord(v_stu, null, v_add_home, v_add_uni, v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    MakeNursingAssistant(v_stu,v_ayr.curr.ayr); -- Should be a nursing assistant
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'SLC');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus2.ayr);
    AddBobsALevels(v_stu, v_ayr.minus1.ayr, BuildDate(9,8,v_ayr.curr.yr));
    CloseScj(v_stu, 5);
    --No Identification required as associates do not need Id.
  END BuildPersonaUkShlsAssociate;


  PROCEDURE BuildPersonaUkPostgrad(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'M','Captain','Gavin','Capacitor', 'Uk-Postgrad',23);
    v_course:=CoursePostGrad();
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni,  v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'SLC');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus3.ayr);
    AddBobsALevels(v_stu, v_ayr.minus2.ayr, BuildDate(9,8,v_ayr.minus1.yr));
    AddVaildPhotoID(v_stu);
  END BuildPersonaUkPostgrad;
  
  PROCEDURE BuildPersonaIntNoPassport(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentChinese(i_stu_code,'M','Mr','Guardian','Matrix', 'Int-No-Pass',18);
    v_course:=CourseCompSci();
    v_course.fee_status:=v_course.fee_status_overseas;
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressChina();
    v_sponsor_ref:='';
    v_ucas_num:='';
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni,  v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'MKB511');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    --Qualifications --not listed for now.
    --No Passport/ Visa
  END BuildPersonaIntNoPassport;

  PROCEDURE BuildPersonaIntPassedRTS(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentChinese(i_stu_code,'M','Mr','Phong','', 'Int-Pass-RTS',18);
    v_course:=CourseCompSci();
    v_course.fee_status:=v_course.fee_status_overseas;
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressChina();
    v_sponsor_ref:='';
    v_ucas_num:='';
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni,  v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'MKB511');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    --Qualifications --not listed for now.
    AddPassport(v_stu);
    AddVisa(v_stu);
    AddValidateRtsPass(v_stu);
  END BuildPersonaIntPassedRTS;

  PROCEDURE BuildPersonaIntNoVisa(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentChinese(i_stu_code,'M','Mr','Chen','', 'Int-No-Visa',18);
    v_course:=CourseCompSci();
    v_course.fee_status:=v_course.fee_status_overseas;
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressChina();
    v_sponsor_ref:='';
    v_ucas_num:='';
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni,  v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'MKB511');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    --Qualifications --not listed for now.
    AddPassport(v_stu);
  END BuildPersonaIntNoVisa;

  PROCEDURE BuildPersonaFeeQuery(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'F','Mr','Al','', 'Uk-Ret-FeeIssue',19);
    v_course:=CourseCompSci();
    v_ayr:=GetAyrs(2,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';
    v_course.level_code:=2;
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni, v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    Update INTUIT.INS_STU SET STU_UDF7='W' WHERE STU_CODE=v_stu.code;              
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, ADD_MONTHS(SYSDATE,-12), BuildDate(28,6,v_ayr.plus1.yr), v_stu.age-1, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.minus1.ayr, v_course, BuildDate(10,9,v_ayr.minus1.yr), v_ayr.plus1.yr, add_months(sysdate,-12), BuildDate(11,9,v_ayr.minus1.yr), BuildDate(1,1,v_ayr.minus1.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.minus1.ayr,BuildDate(1,11,v_ayr.minus1.yr),BuildDate(21,11,v_ayr.minus1.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.minus1.ayr,'SLC');
    CompleteCourseYear(v_stu,'01',v_ayr.minus1.ayr);
    v_course.cblock:='2'; -- 2nd year
    AddCourseEnrolment(v_stu, '02', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0002',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'02',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'SLC');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus3.ayr);
    AddBobsALevels(v_stu, v_ayr.minus2.ayr, BuildDate(9,8,v_ayr.minus1.yr));
    --No Passport
  END BuildPersonaFeeQuery;    
  
  PROCEDURE BuildPersonaUkMissFee(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'M','Mr','Glitch','', 'Uk-Missing-Fee',18);
    v_course:=CourseCompSci();
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni,  v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    --No Fee/sponsor entered 
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus2.ayr);
    AddBobsALevels(v_stu, v_ayr.minus1.ayr, BuildDate(9,8,v_ayr.curr.yr));
    AddPassport(v_stu);
  END BuildPersonaUkMissFee;  
  
  PROCEDURE BuildPersonaUkLateEnrol(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'M','Mr','White','Rabbit', 'Uk-Late',19);
    v_course:=CourseCompSci();
    v_ayr:=GetAyrs(2,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';
    v_course.level_code:=2;
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni, v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr), v_stu.age-1, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.minus1.ayr, v_course, BuildDate(10,9,v_ayr.minus1.yr), v_ayr.plus1.yr, add_months(sysdate,-12), BuildDate(11,9,v_ayr.minus1.yr), BuildDate(1,1,v_ayr.minus1.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.minus1.ayr,BuildDate(1,11,v_ayr.minus1.yr),BuildDate(21,11,v_ayr.minus1.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.minus1.ayr,'SLC');
    UPDATE SRS_SCE set SCE_UDFE='Y' where SCE_STUC=v_stu.code;
    UPDATE SRS_SCJ set SCJ_UDFH='01-JAN-1999', SCJ_UDFI='01-JAN-1999' where SCJ_STUC=v_stu.code;
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus3.ayr);
    AddBobsALevels(v_stu, v_ayr.minus2.ayr, BuildDate(9,8,v_ayr.minus1.yr));
    AddVaildPhotoID(v_stu);
  END BuildPersonaUkLateEnrol;
  
  PROCEDURE BuildPersonaUkAdmissionsQuery(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'M','Mr','Edward','Nygma', 'Uk-Admission-Query',18);
    v_course:=CourseCompSci();
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni,  v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    UPDATE INS_STU set STU_IACC='CO' where STU_CODE=i_stu_code;-- Add admissions query.
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'SLC');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus2.ayr);
    AddBobsALevels(v_stu, v_ayr.minus1.ayr, BuildDate(9,8,v_ayr.curr.yr));
    AddPassport(v_stu);
  END BuildPersonaUkAdmissionsQuery;  
  
  PROCEDURE BuildPersonaUkYearAbroad(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_add_abroad ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'F','Miss','Dot','Matrix', 'Uk-Yr-Abroad',19);
    v_course:=CourseCompSci();
    v_ayr:=GetAyrs(2,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_add_abroad:=AddressFrance();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';
    v_course.level_code:=2;
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_abroad, v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, ADD_MONTHS(SYSDATE,-12), BuildDate(28,6,v_ayr.plus1.yr), v_stu.age-1, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.minus1.ayr, v_course, BuildDate(10,9,v_ayr.minus1.yr), v_ayr.plus1.yr, add_months(sysdate,-12), BuildDate(11,9,v_ayr.minus1.yr), BuildDate(1,1,v_ayr.minus1.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.minus1.ayr,BuildDate(1,11,v_ayr.minus1.yr),BuildDate(21,11,v_ayr.minus1.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.minus1.ayr,'SLC');
    CompleteCourseYear(v_stu,'01',v_ayr.minus1.ayr);
    v_course.cblock:='2'; -- 2nd year
    v_course.loc_of_study:='T';--Year abroad in France
    AddCourseEnrolment(v_stu, '02', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0002',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'02',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'SLC');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_abroad);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus3.ayr);
    AddBobsALevels(v_stu, v_ayr.minus2.ayr, BuildDate(9,8,v_ayr.minus1.yr));
  END BuildPersonaUkYearAbroad;


  PROCEDURE BuildPersonaUkSelfFunded(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'M','Mr','Scrooge','McDuck', 'Uk-Self-Fund',18);
    v_course:=CourseCompSci();
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='';
    v_ucas_num:='021185706';
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni,  v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus2.ayr);
    AddBobsALevels(v_stu, v_ayr.minus1.ayr, BuildDate(9,8,v_ayr.curr.yr));
    AddPassport(v_stu);
  END BuildPersonaUkSelfFunded;  
  
  PROCEDURE BuildPersonaUkSelfFundLPC(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'M','Mr','Pheonix','Wright', 'Uk-Self-Fund-LPC',18);
    v_course:=CourseLPC();
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='';
    v_ucas_num:='021185706';
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni,  v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus2.ayr);
    AddBobsALevels(v_stu, v_ayr.minus1.ayr, BuildDate(9,8,v_ayr.curr.yr));
    AddPassport(v_stu);
  END BuildPersonaUkSelfFundLPC;  
  
    PROCEDURE BuildPersonaIntSelfFunded(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentChinese(i_stu_code,'M','Mr','F�','W�ng', 'Int-Self-Fund',18);
    v_course:=CourseCompSci();
    v_course.fee_status:=v_course.fee_status_overseas;
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressChina();
    v_sponsor_ref:='';
    v_ucas_num:='';
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni,  v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    --Qualifications --not listed for now.
    AddPassport(v_stu);
    AddVisa(v_stu);
    AddValidateRtsPass(v_stu);
  END BuildPersonaIntSelfFunded;  
  
    PROCEDURE BuildPersonaUkDaceNoPass(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'M','Mr','Rupert','Giles', 'Uk-Dace-No-Pass',18);
    v_course:=CourseDace();
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='';
    v_ucas_num:='021185706';
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni,  v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'SLC');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus2.ayr);
    AddBobsALevels(v_stu, v_ayr.minus1.ayr, BuildDate(9,8,v_ayr.curr.yr));
    --No Passport
  END BuildPersonaUkDaceNoPass;
  
  PROCEDURE BuildPersonaUkMissingFeeStatus(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'M','Mr','Dave','Lister', 'Uk-MissngFeeStatus',18);
    v_course:=CourseLPC();
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='';
    v_ucas_num:='021185706';
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni,  v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus2.ayr);
    AddBobsALevels(v_stu, v_ayr.minus1.ayr, BuildDate(9,8,v_ayr.curr.yr));
    AddPassport(v_stu);
    
    --Set SCE_FSTC to null to simulate a missing fee status
    UPDATE INTUIT.SRS_SCE
    SET SCE_FSTC = NULL
    WHERE SCE_AYRC =v_ayr.curr.ayr
    AND SCE_STUC = i_stu_code;
    
  END BuildPersonaUkMissingFeeStatus;
  
  PROCEDURE BuildPersonaUkOpenHealthSAB(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'F','Ms','Kristine','Kochanski', 'Uk-UkOpenHealthSAB',18);
    v_course:=CourseLPC();
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='';
    v_ucas_num:='021185706';
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni,  v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus2.ayr);
    AddBobsALevels(v_stu, v_ayr.minus1.ayr, BuildDate(9,8,v_ayr.curr.yr));
    AddPassport(v_stu);
    
    --Add Open Health SAB
    INSERT INTO SRS_SAB(SAB_STUC, SAB_SEQ2, SAB_SRTN, SAB_RAAC, SAB_BEGD)
    VALUES(i_stu_code, '01', 'Kristine      K', '04', SYSDATE);
    -- Add FITNESS doc attached to health SAB
    AddFakeDoc(v_stu,'FITNESS',add_months(sysdate,-1), 'SAB', 'F');
    
  END BuildPersonaUkOpenHealthSAB; 
    
  PROCEDURE BuildPersonaUkICWS(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'M','Mr','Pheonix','', 'Uk-ICWS',18);
    v_course:=CourseICWS();
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni,  v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'SLC');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus2.ayr);
    AddBobsALevels(v_stu, v_ayr.minus1.ayr, BuildDate(9,8,v_ayr.curr.yr));  
  END BuildPersonaUkICWS;

  PROCEDURE BuildPersonaIntICWS(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentChinese(i_stu_code,'M','Mr','Ryuu','', 'Int-ICWS',18);
    v_course:=CourseICWS();
    v_course.fee_status:=v_course.fee_status_overseas;
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressChina();
    v_sponsor_ref:='';
    v_ucas_num:='';
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni,  v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'MKB511');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    --Qualifications --not listed for now.
    AddPassport(v_stu);
    AddVisa(v_stu);
    AddValidateRtsPass(v_stu);
  END BuildPersonaIntICWS;

PROCEDURE BuildPersonaUkModFreeze(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
    o_modules SYS_REFCURSOR;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'F','Mr','Victor','Fries', 'Uk-Mod-Freeze',19);
    v_course:=CourseCompSci();
    v_ayr:=GetAyrs(2,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';

    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni, v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, ADD_MONTHS(SYSDATE,-12), BuildDate(28,6,v_ayr.plus1.yr), v_stu.age-1, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);

    ModulesCompSci(v_ayr.minus1.ayr, v_ayr.minus1.ayr,v_course.level_code, o_modules);
    SelectCourseModules(v_stu , o_modules, v_ayr.minus1.ayr, v_ayr.minus1.ayr,  v_course.level_code);
    EnrolStudentOnCourse(v_stu, v_course, v_ayr.minus1);
    LazyPassYear(v_stu.scjc, v_ayr.minus1.ayr);
    AssignToFakeExamBoard(v_stu.code,v_course.is_postgrad,-1,1,1,0);
    v_course.level_code:=2;
    AddCourseEnrolment(v_stu, '01', v_ayr.minus1.ayr, v_course, BuildDate(10,9,v_ayr.minus1.yr), v_ayr.plus1.yr, add_months(sysdate,-12), BuildDate(11,9,v_ayr.minus1.yr), BuildDate(1,1,v_ayr.minus1.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.minus1.ayr,BuildDate(1,11,v_ayr.minus1.yr),BuildDate(21,11,v_ayr.minus1.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.minus1.ayr,'SLC');
    --SelectCourseModules(v_stu, i_modules, v_ayr.minus1.ayr, v_ayr.minus1.ayr, '1');
    UPDATE INTUIT.SRS_SCE SET SCE_PGSC='CL' WHERE SCE_SCJC=v_stu.scjc and SCE_AYRC=v_ayr.minus1.ayr;     
--    CompleteCourseYear(v_stu,'01',v_ayr.minus1.ayr);
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus3.ayr);
    AddBobsALevels(v_stu, v_ayr.minus2.ayr, BuildDate(9,8,v_ayr.minus1.yr));
    AddVaildPhotoID(v_stu);
  END BuildPersonaUkModFreeze;

PROCEDURE BuildPersonaUkModUnpublished(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
    o_modules SYS_REFCURSOR;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'F','Mr','Allen','Appel', 'Uk-Mod-Unpub',19);
    v_course:=CourseCompSci();
    v_ayr:=GetAyrs(2,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';

    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni, v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, ADD_MONTHS(SYSDATE,-12), BuildDate(28,6,v_ayr.plus1.yr), v_stu.age-1, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);

    ModulesCompSci(v_ayr.minus1.ayr, v_ayr.minus1.ayr,v_course.level_code, o_modules);
    SelectCourseModules(v_stu , o_modules, v_ayr.minus1.ayr, v_ayr.minus1.ayr,  v_course.level_code);
    EnrolStudentOnCourse(v_stu, v_course, v_ayr.minus1);
    LazyPassYear(v_stu.scjc, v_ayr.minus1.ayr);
    AssignToFakeExamBoard(v_stu.code,v_course.is_postgrad,-1,0,1,0);
    v_course.level_code:=2;
    AddCourseEnrolment(v_stu, '01', v_ayr.minus1.ayr, v_course, BuildDate(10,9,v_ayr.minus1.yr), v_ayr.plus1.yr, add_months(sysdate,-12), BuildDate(11,9,v_ayr.minus1.yr), BuildDate(1,1,v_ayr.minus1.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.minus1.ayr,BuildDate(1,11,v_ayr.minus1.yr),BuildDate(21,11,v_ayr.minus1.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.minus1.ayr,'SLC');
    --SelectCourseModules(v_stu, i_modules, v_ayr.minus1.ayr, v_ayr.minus1.ayr, '1');
    UPDATE INTUIT.SRS_SCE SET SCE_PGSC='CL' WHERE SCE_SCJC=v_stu.scjc and SCE_AYRC=v_ayr.minus1.ayr;     
--    CompleteCourseYear(v_stu,'01',v_ayr.minus1.ayr);

    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus3.ayr);
    AddBobsALevels(v_stu, v_ayr.minus2.ayr, BuildDate(9,8,v_ayr.minus1.yr));
    AddVaildPhotoID(v_stu);
  END BuildPersonaUkModUnpublished;

  PROCEDURE BuildPersonaUkModPublished(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
    o_modules SYS_REFCURSOR;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'F','Mr','JR','Tolkien', 'Uk-Mod-Pub',19);
    v_course:=CourseCompSci();
    v_ayr:=GetAyrs(2,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';

    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni, v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, ADD_MONTHS(SYSDATE,-12), BuildDate(28,6,v_ayr.plus1.yr), v_stu.age-1, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);

    ModulesCompSci(v_ayr.minus1.ayr, v_ayr.minus1.ayr,v_course.level_code, o_modules);
    SelectCourseModules(v_stu , o_modules, v_ayr.minus1.ayr, v_ayr.minus1.ayr,  v_course.level_code);
    EnrolStudentOnCourse(v_stu, v_course, v_ayr.minus1);
    LazyPassYear(v_stu.scjc, v_ayr.minus1.ayr);
    AssignToFakeExamBoard(v_stu.code,v_course.is_postgrad,-1,0,0,0);
    v_course.level_code:=2;
    AddCourseEnrolment(v_stu, '01', v_ayr.minus1.ayr, v_course, BuildDate(10,9,v_ayr.minus1.yr), v_ayr.plus1.yr, add_months(sysdate,-12), BuildDate(11,9,v_ayr.minus1.yr), BuildDate(1,1,v_ayr.minus1.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.minus1.ayr,BuildDate(1,11,v_ayr.minus1.yr),BuildDate(21,11,v_ayr.minus1.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.minus1.ayr,'SLC');
    --SelectCourseModules(v_stu, i_modules, v_ayr.minus1.ayr, v_ayr.minus1.ayr, '1');
    UPDATE INTUIT.SRS_SCE SET SCE_PGSC='CL' WHERE SCE_SCJC=v_stu.scjc and SCE_AYRC=v_ayr.minus1.ayr;     
--    CompleteCourseYear(v_stu,'01',v_ayr.minus1.ayr);

    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus3.ayr);
    AddBobsALevels(v_stu, v_ayr.minus2.ayr, BuildDate(9,8,v_ayr.minus1.yr));
    AddVaildPhotoID(v_stu);
  END BuildPersonaUkModPublished;

  PROCEDURE SetModuleResult(i_scj_code in ins_smr.spr_code%type, i_mod_code in ins_smr.MOD_CODE%type,
  i_agreed_mark in ins_smr.SMR_AGRM%type,i_actual_mark in ins_smr.smr_actm%type,i_freeze_mark in ins_smr.SMR_UDF2%type,
  i_agreed_grade in ins_smr.SMR_AGRG%type,i_actual_grade in ins_smr.smr_actg%type,i_freeze_grade in ins_smr.SMR_UDF3%type,
  i_result in ins_smr.SMR_RSLT%type) AS
  v_int int;
  BEGIN
  UPDATE ins_smr 
       SET smr_actm=i_actual_mark,SMR_AGRM=i_agreed_mark,SMR_UDF2=i_freeze_mark,
           smr_actg=i_actual_grade,SMR_AGRG=i_agreed_grade,SMR_UDF3=i_freeze_grade,SMR_RSLT=i_result
           where spr_code = i_scj_code
            and MOD_CODE=i_mod_code;
  END SetModuleResult;

  PROCEDURE BuildPersonaExamFrozen(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
    o_modules SYS_REFCURSOR;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'F','Queen','Elsa','Of Arendelle', 'Uk-Exam-Frozen',18);
    v_course:=CourseCompSci();
    v_ayr:=GetAyrs(2,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';

    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni, v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, ADD_MONTHS(SYSDATE,-12), BuildDate(28,6,v_ayr.plus1.yr), v_stu.age-1, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);

    ModulesCompSci(v_ayr.curr.ayr, v_ayr.curr.ayr,v_course.level_code, o_modules);
    SelectCourseModules(v_stu , o_modules, v_ayr.curr.ayr, v_ayr.curr.ayr,  v_course.level_code);
    EnrolStudentOnCourse(v_stu, v_course, v_ayr.curr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, add_months(sysdate,-12), BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'SLC');
    AssignToFakeExamBoard(v_stu.code,v_course.is_postgrad,0,
                          1,--Is_Frozen
                          1,--Is_Unpub
                          0);--Is_Resit
    SetModuleResult(v_stu.scjc,'CS-110','77','66','55','zz','zz','zz','P');--Invalid grade triggers RESULT field being shown.
    SetModuleResult(v_stu.scjc,'CS-115','77','66','55','P','P','P','P');--Pass
    SetModuleResult(v_stu.scjc,'CS-130','33','22','11','F','F','F','F');--Fail
    SetModuleResult(v_stu.scjc,'CS-135','77','66','55','MC','MC','MC','P');--Mitigating Circs
    SetModuleResult(v_stu.scjc,'CS-150','0','0','0','UP','UP','UP','F');--Unfair practice (hidden on transcripts)
    SetModuleResult(v_stu.scjc,'CS-155','0','0','0','AM','AM','AM','F');--Academic Misconduct (hidden on transcripts)
    SetModuleResult(v_stu.scjc,'CS-170','77','66','55','ME','ME','ME','P');--Merit
    SetModuleResult(v_stu.scjc,'CS-175','99','98','97','PD','PD','PD','P');--Distinction
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus3.ayr);
    AddBobsALevels(v_stu, v_ayr.minus2.ayr, BuildDate(9,8,v_ayr.curr.yr));
    AddVaildPhotoID(v_stu);    
    --No Passport
  END BuildPersonaExamFrozen;

  PROCEDURE BuildPersonaExamUnFrozen(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
    o_modules SYS_REFCURSOR;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'F','Princess','Anna','Of Arendelle', 'Uk-Exam-UnFrozen',18);
    v_course:=CourseCompSci();
    v_ayr:=GetAyrs(2,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';

    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni, v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, ADD_MONTHS(SYSDATE,-12), BuildDate(28,6,v_ayr.plus1.yr), v_stu.age-1, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);

    ModulesCompSci(v_ayr.curr.ayr, v_ayr.curr.ayr,v_course.level_code, o_modules);
    SelectCourseModules(v_stu , o_modules, v_ayr.curr.ayr, v_ayr.curr.ayr,  v_course.level_code);
    EnrolStudentOnCourse(v_stu, v_course, v_ayr.curr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, add_months(sysdate,-12), BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'SLC');
    AssignToFakeExamBoard(v_stu.code,v_course.is_postgrad,0,
                          0,--Is_Frozen
                          1,--Is_Unpub
                          0);--Is_Resit
    SetModuleResult(v_stu.scjc,'CS-110','77','66','55','zz','zz','zz','P');--Invalid grade triggers RESULT field being shown.
    SetModuleResult(v_stu.scjc,'CS-115','77','66','55','P','P','P','P');--Pass
    SetModuleResult(v_stu.scjc,'CS-130','33','22','11','F','F','F','F');--Fail
    SetModuleResult(v_stu.scjc,'CS-135','77','66','55','MC','MC','MC','P');--Mitigating Circs
    SetModuleResult(v_stu.scjc,'CS-150','0','0','0','UP','UP','UP','F');--Unfair practice (hidden on transcripts)
    SetModuleResult(v_stu.scjc,'CS-155','0','0','0','AM','AM','AM','F');--Academic Misconduct (hidden on transcripts)
    SetModuleResult(v_stu.scjc,'CS-170','77','66','55','ME','ME','ME','P');--Merit
    SetModuleResult(v_stu.scjc,'CS-175','99','98','97','PD','PD','PD','P');--Distinction
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus3.ayr);
    AddBobsALevels(v_stu, v_ayr.minus2.ayr, BuildDate(9,8,v_ayr.curr.yr));
    AddVaildPhotoID(v_stu);    
    --No Passport
  END BuildPersonaExamUnFrozen;
  
  PROCEDURE BuildPersonaExamPublished(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
    o_modules SYS_REFCURSOR;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'M','Mr','Oaken','', 'Uk-Exam-Published',18);
    v_course:=CourseCompSci();
    v_ayr:=GetAyrs(2,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';

    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni, v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, ADD_MONTHS(SYSDATE,-12), BuildDate(28,6,v_ayr.plus1.yr), v_stu.age-1, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);

    ModulesCompSci(v_ayr.curr.ayr, v_ayr.curr.ayr,v_course.level_code, o_modules);
    SelectCourseModules(v_stu , o_modules, v_ayr.curr.ayr, v_ayr.curr.ayr,  v_course.level_code);
    EnrolStudentOnCourse(v_stu, v_course, v_ayr.curr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, add_months(sysdate,-12), BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'SLC');
    AssignToFakeExamBoard(v_stu.code,v_course.is_postgrad,0,
                          0,--Is_Frozen
                          0,--Is_Unpub
                          0);--Is_Resit
    SetModuleResult(v_stu.scjc,'CS-110','77','66','55','zz','zz','zz','P');--Invalid grade triggers RESULT field being shown.
    SetModuleResult(v_stu.scjc,'CS-115','77','66','55','P','P','P','P');--Pass
    SetModuleResult(v_stu.scjc,'CS-130','33','22','11','F','F','F','F');--Fail
    SetModuleResult(v_stu.scjc,'CS-135','77','66','55','MC','MC','MC','P');--Mitigating Circs
    SetModuleResult(v_stu.scjc,'CS-150','0','0','0','UP','UP','UP','F');--Unfair practice (hidden on transcripts)
    SetModuleResult(v_stu.scjc,'CS-155','0','0','0','AM','AM','AM','F');--Academic Misconduct (hidden on transcripts)
    SetModuleResult(v_stu.scjc,'CS-170','77','66','55','ME','ME','ME','P');--Merit
    SetModuleResult(v_stu.scjc,'CS-175','99','98','97','PD','PD','PD','P');--Distinction
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus3.ayr);
    AddBobsALevels(v_stu, v_ayr.minus2.ayr, BuildDate(9,8,v_ayr.curr.yr));
    AddVaildPhotoID(v_stu);    
    --No Passport
  END BuildPersonaExamPublished;

  PROCEDURE BuildPersonaUkCrossAyr(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'F','Miss','Abby','', 'AB',23);
    v_course:=CourseCrossAyr();
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni,  v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    v_course.next_occurrence:='B';
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'SLC');
    v_course.occurrence:='B';v_course.next_occurrence:='A';
    AddCourseEnrolment(v_stu, '02', v_ayr.plus1.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, add_months(sysdate,12), BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus3.ayr);
    AddBobsALevels(v_stu, v_ayr.minus2.ayr, BuildDate(9,8,v_ayr.minus1.yr));
    AddVaildPhotoID(v_stu);
  END BuildPersonaUkCrossAyr;

  PROCEDURE BuildPersonaUkDualEnrol(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course1 Course;
    v_course2 Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'M','Mr','Didymus','', 'DualEnrol',18);
    v_course1:=CourseCompSci();
    v_course2:=CourseICWS();
    v_ayr:=GetAyrs(1,v_course1.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni,  v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course1, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course1.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course1, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course1,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course1.fee,v_sponsor_ref,v_course1.code,v_ayr.curr.ayr,'SLC');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus2.ayr);
    AddBobsALevels(v_stu, v_ayr.minus1.ayr, BuildDate(9,8,v_ayr.curr.yr));
    AddPassport(v_stu);
    -- 2nd Course
    v_stu.scjc:=v_stu.code||'/2';
    AddCourseJoin(v_stu, '02', v_ayr.curr.ayr, v_course2, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course2.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course2, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course2,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'02',v_stu.srtn,v_course2.fee,v_sponsor_ref,v_course2.code,v_ayr.curr.ayr,'SLC');


  END BuildPersonaUkDualEnrol;

  PROCEDURE BuildPersonaIntJVQuery(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentChinese(i_stu_code,'M','Mr','Iroh','', 'Int-ICWS-Finance-Query',18);
    v_course:=CourseICWS();
    v_course.fee_status:=v_course.fee_status_overseas;
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressChina();
    v_sponsor_ref:='';
    v_ucas_num:='';
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni,  v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    UPDATE INS_STU set STU_IACC='NQ' where STU_CODE=i_stu_code;-- Add admissions (Joint Venture) query.
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'MKB511');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    --Qualifications --not listed for now.
    AddPassport(v_stu);
    AddVisa(v_stu);
    AddValidateRtsPass(v_stu);
  END BuildPersonaIntJVQuery;

  PROCEDURE BuildPersonaUkCeremonial(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'M','Sir','Humphrey','Appleby', 'Uk-Ceremony',18);
    v_course:=CourseCompSci();
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni,  v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'SLC');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus2.ayr);
    AddBobsALevels(v_stu, v_ayr.minus1.ayr, BuildDate(9,8,v_ayr.curr.yr));
    AddPassport(v_stu);
    LazyPassYear(v_stu.scjc, v_ayr.curr.ayr);
    AddGraduationCeremony(v_stu,v_course,v_add_home,v_ayr.curr.ayr);
    
  END BuildPersonaUkCeremonial;  
  
  PROCEDURE BuildPersonaNewCodes(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'M','Mr','Bob','', 'Uk-NewCode',18);
    v_course:=CourseCompSci();
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';
    v_stu:=StudentNewCode(v_stu,v_ayr.curr,1);
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni,  v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'SLC');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus2.ayr);
    AddBobsALevels(v_stu, v_ayr.minus1.ayr, BuildDate(9,8,v_ayr.curr.yr));
    AddPassport(v_stu);
  END BuildPersonaNewCodes;
  
  PROCEDURE BuildPersonaUkEnroledNoDecs(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
    v_ayr Ayrs;
    v_course Course;
    v_add_uni ContactAddress;
    v_add_home ContactAddress;
    v_stu Student;
    v_sponsor_ref INTUIT.INS_STU.STU_SSIN%type;
    v_ucas_num INTUIT.INS_STU.STU_UCID%type;
  BEGIN
    v_stu:=StudentUK(i_stu_code,'M','Mr','Bob','', 'Uk-EnroledNoDecs',18);
    v_course:=CourseCompSci();
    v_ayr:=GetAyrs(1,v_course.yrs);
    v_add_uni:=AddressTyBeck(v_stu.code);
    v_add_home:=AddressMerthyr();
    v_sponsor_ref:='WMER02102827V';
    v_ucas_num:='021185706';
    
    AddBaseRecord(v_stu, BuildDate(23,9,v_ayr.curr.yr), v_add_home, v_add_uni,  v_sponsor_ref, v_ayr.crsstart.yr, v_ucas_num);
    AddCourseJoin(v_stu, '01', v_ayr.curr.ayr, v_course, SYSDATE, BuildDate(28,6,v_ayr.plus1.yr),v_stu.age, BuildDate(25,9,v_ayr.curr.yr+v_course.yrs),v_add_home.postcode,v_ucas_num,v_ayr.crsstart.ayr,v_ayr.crsend.ayr);
    AddCourseEnrolment(v_stu, '01', v_ayr.curr.ayr, v_course, BuildDate(10,9,v_ayr.curr.yr), v_ayr.plus1.yr, sysdate, BuildDate(11,9,v_ayr.curr.yr), BuildDate(1,1,v_ayr.curr.yr));
    AddFees(v_stu,'01','0001',v_course,v_ayr.curr.ayr,BuildDate(1,11,v_ayr.curr.yr),BuildDate(21,11,v_ayr.curr.yr));
    AddSponsor(v_stu.code,'01',v_stu.srtn,v_course.fee,v_sponsor_ref,v_course.code,v_ayr.curr.ayr,'SLC');
    AddNextOfKin(v_stu.code,v_add_home);
    AddAddressHome(v_stu.code,v_add_home,v_add_uni.phone);
    AddAddressTerm(v_stu.code,v_add_uni);
    AddAddressContact(v_stu.code,v_add_uni,v_add_uni.phone,v_add_home.email);
    AddBobsASLevels(v_stu, v_ayr.minus2.ayr);
    AddBobsALevels(v_stu, v_ayr.minus1.ayr, BuildDate(9,8,v_ayr.curr.yr));
    AddPassport(v_stu);
    --Set the student as enroled but dont update the decs fields on ins_stu recored
    UPDATE SRS_SCE
    SET SCE_STAC = 'E'
    WHERE SCE_STUC = i_stu_code;
  END BuildPersonaUkEnroledNoDecs;  


  PROCEDURE WipeReservedRange AS
  BEGIN
    FOR loop_counter IN 0..9 
    LOOP
       ErasePersona('00010'||TO_CHAR(loop_counter));   
    END LOOP;
    FOR loop_counter IN 10..99 
    LOOP
       ErasePersona('0001'||TO_CHAR(loop_counter));   
    END LOOP;
  END WipeReservedRange;

  PROCEDURE ErasePersona(i_stu_code INTUIT.INS_STU.STU_CODE%type) AS
  BEGIN
    --IF (i_stu_code not like '0001%') then return; end if;
    -- We Reserve the 000100-000199 range for ourselves
    DELETE from INTUIT.INS_STU where STU_CODE = i_stu_code;
    DELETE from INTUIT.SRS_SCJ where SCJ_STUC = i_stu_code;
    DELETE from INTUIT.SRS_SCE where SCE_STUC = i_stu_code;
    DELETE from INTUIT.SRS_FDU where FDU_SCJC like i_stu_code||'%';
    DELETE from INTUIT.SRS_SSP where SSP_STUC = i_stu_code;
    DELETE from INTUIT.SRS_SNK where SNK_STUC = i_stu_code;
    DELETE from INTUIT.MEN_ADD where ADD_ADID = i_stu_code;
    DELETE from INTUIT.SRS_SQE where SQE_STUC = i_stu_code;
    DELETE from INTUIT.SRS_PPT where PPT_STUC = i_stu_code;
    DELETE from INTUIT.SRS_SPD where SPD_STUC = i_stu_code;
    DELETE from INTUIT.SRS_VIS where VIS_STUC = i_stu_code;
    DELETE from INTUIT.CAM_SMO where SPR_CODE like i_stu_code||'%';
    DELETE from INTUIT.INS_SPR where SPR_STUC = i_stu_code;
    DELETE from INTUIT.MEN_DOC where 
        DOC_CODE in (SELECT DRR_DOCC from INTUIT.MEN_DRR where DRR_ENTC in ('PPT','SPD','VIS','SAB') and DRR_PKF1 = i_stu_code)
        and DOC_DTYC in ('VISAT4','VISA','PASSPORT','PHOTO ID','VISANONT4','VISALET','VIGNETE','VIGNETTE','BIRTH CERT', 'FITNESS');
    DELETE from INTUIT.MEN_DRR where DRR_ENTC in ('PPT','SPD','VIS', 'SAB') and DRR_PKF1  = i_stu_code;  
    DELETE FROM INTUIT.SRS_SAB WHERE SAB_STUC = i_stu_code;
    DELETE FROM INTUIT.INS_SMR WHERE SPR_CODE like i_stu_code||'%';
    DELETE FROM INTUIT.SRS_RDS WHERE RDS_STUC = i_stu_code;
    DELETE FROM INTUIT.SRS_SCY WHERE SCY_STUC = i_stu_code;
    DELETE FROM STUDENT_PROFILE.ENGAGEMENT_METHOD WHERE STU_CODE = i_stu_code;
    DELETE FROM STUDENT_PROFILE.ENGAGEMENT_ONLINE_INFO WHERE STU_CODE = i_stu_code;
    -- Remove module selections
    DELETE FROM CAM_SMO WHERE SPR_CODE LIKE i_stu_code||'%';
    DELETE FROM INS_SMR WHERE SPR_CODE LIKE i_stu_code||'%';
    DELETE FROM CAM_SAS WHERE SPR_CODE LIKE i_stu_code||'%';
    -- Remove Decs
    DELETE FROM STUDENT_PROFILE.ENROL_DECLARATION_ANSWERS WHERE SCJ_CODE LIKE i_stu_code||'%';
    -- Remove Carreer Answers
    DELETE FROM STUDENT_PROFILE.ENROL_WORK_EXP_ANSWERS WHERE SCJ_CODE LIKE i_stu_code||'%';
    -- Remove Work Exp Answers
    DELETE FROM STUDENT_PROFILE.ENROL_CAREER_REG_ANSWERS WHERE SCJ_CODE LIKE i_stu_code||'%';
  END ErasePersona;

  PROCEDURE BuildStudents AS
  BEGIN
    ErasePersona('000100');
    BuildPersonaUkNoPassport('000100');
    ErasePersona('000101');
    BuildPersonaUkReturning('000101');
    ErasePersona('000102');
    BuildPersonaUkNurse('000102');
    ErasePersona('000103');
    BuildPersonaUkPostgrad('000103');
    ErasePersona('000104');
    BuildPersonaIntNoPassport('000104');
    ErasePersona('000105');
    BuildPersonaIntPassedRTS('000105');
    ErasePersona('000106');
    BuildPersonaUkPassport('000106');
    ErasePersona('000107');
    BuildPersonaFeeQuery('000107');
    ErasePersona('000108');
    BuildPersonaUkMissFee('000108');
    ErasePersona('000109');
    BuildPersonaUkShlsAssociate('000109');
    ErasePersona('000110');
    BuildPersonaUkPhotoId('000110');
    ErasePersona('000111');
    BuildPersonaIntNoVisa('000111');
    ErasePersona('000112');
    BuildPersonaIntReturning('000112');
    ErasePersona('000113');
    BuildPersonaUkPostgradRet('000113');
    ErasePersona('000114');
    BuildPersonaUkNurseRet('000114');
    ErasePersona('000115');
    BuildPersonaUkShlsRet('000115');
    ErasePersona('000116');
    BuildPersonaUkRetNoId('000116');
    ErasePersona('000117');
    BuildPersonaEuFrench('000117');
    ErasePersona('000118');
    BuildPersonaUkLateEnrol('000118');
    ErasePersona('000119');
    BuildPersonaUkAdmissionsQuery('000119');
    ErasePersona('000120');
    BuildPersonaUkYearAbroad('000120');
    ErasePersona('000121');
    BuildPersonaUkSelfFunded('000121');
    ErasePersona('000122');
    BuildPersonaUkICWS('000122');
    ErasePersona('000123');
    BuildPersonaUkOpenHealthSAB('000123');
    ErasePersona('000124');
    BuildPersonaUkMissingFeeStatus('000124');
    ErasePersona('000125');
    BuildPersonaUkDaceNoPass('000125');
    ErasePersona('000126');
    BuildPersonaUkModFreeze('000126');
    ErasePersona('000127');
    BuildPersonaUkModUnpublished('000127');
    ErasePersona('000128');
    BuildPersonaUkModPublished('000128');
    ErasePersona('000129');
    BuildPersonaIntICWS('000129');
    ErasePersona('000130');
    BuildPersonaExamFrozen('000130');
    ErasePersona('000131');
    BuildPersonaExamUnFrozen('000131');
    ErasePersona('000132');
    BuildPersonaExamPublished('000132');
    ErasePersona('000133');
    BuildPersonaUkCrossAyr('000133');
    ErasePersona('000134');
    BuildPersonaUkDualEnrol('000134');
    ErasePersona('000135');
    BuildPersonaUkSelfFundLPC('000135');
    ErasePersona('000136');
    BuildPersonaIntSelfFunded('000136');
    ErasePersona('000137');
    BuildPersonaIntJVQuery('000137');
    ErasePersona('000138');
    BuildPersonaUkCeremonial('000138');
    ErasePersona('000139');
    BuildPersonaUkEnroledNoDecs('000139');
    ErasePersona('000140');
    BuildPersonaCOAH('000140');
    ErasePersona('000141');
    BuildPersonaAccountingJanEntry('000141');
    ErasePersona('000142');
    BuildPersonaUkNoPathway('000142');
    COMMIT;

  END BuildStudents;
  
  /* -- Required Grants for this package
  grant INSERT, UPDATE, DELETE on "INTUIT"."CAM_SRA" to "STUDENT_PROFILE" ;
  grant INSERT, UPDATE, DELETE on "INTUIT"."SRS_RDS" to "STUDENT_PROFILE" ;
  grant EXECUTE on "EXAMSYS"."FREEZE" to "STUDENT_PROFILE" ;
  grant INSERT, UPDATE, DELETE on "INTUIT"."SRS_CMY" to "STUDENT_PROFILE" ;
  grant INSERT, UPDATE, DELETE on "INTUIT"."SRS_SCY" to "STUDENT_PROFILE" ;

  */
  

END;