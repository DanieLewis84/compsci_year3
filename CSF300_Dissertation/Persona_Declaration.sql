CREATE OR REPLACE PACKAGE STUDENT_PROFILE.PERSONAS AS 

    
    
    
  /* TODO enter package declarations (types, exceptions, methods etc) here */ 
    -- Procedures to create each of the personas:
    -- Can enrol
    PROCEDURE BuildPersonaUkPassport(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaUkReturning(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaUkNurse(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaUkShlsAssociate(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaUkPostgrad(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaIntReturning(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaIntPassedRTS(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaUkPhotoId(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaUkPostgradRet(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaUkNurseRet(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaUkShlsRet(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaEuFrench(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaUkLateEnrol(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaUkYearAbroad(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaUkDaceNoPass(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaUkCrossAyr(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaUkDualEnrol(i_stu_code INTUIT.INS_STU.STU_CODE%type);
	PROCEDURE BuildPersonaCOAH(i_stu_code INTUIT.INS_STU.STU_CODE%type);
	PROCEDURE BuildPersonaAccountingJanEntry(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    -- Can enrol, but required to take update details to complete
    PROCEDURE BuildPersonaUkSelfFunded(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaUkSelfFundLPC(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaIntSelfFunded(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    -- Can't enrol
    PROCEDURE BuildPersonaUkNoPassport(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaIntNoPassport(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaIntNoVisa(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaFeeQuery(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaIntJVQuery(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaUkMissFee(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaUkRetNoId(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaUkAdmissionsQuery(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaUkMissingFeeStatus(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaUkOpenHealthSAB(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaUkICWS(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaIntICWS(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaExamFrozen(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaExamUnFrozen(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaExamPublished(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    -- Non enrolment related personas
    PROCEDURE BuildPersonaUkCeremonial(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaNewCodes(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaUkEnroledNoDecs(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    PROCEDURE BuildPersonaUkNoPathway(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    -- Remove a persona from the system
    PROCEDURE ErasePersona(i_stu_code INTUIT.INS_STU.STU_CODE%type);
    
    -- Wipe and reinitialise all presonas starting at 000100
    PROCEDURE BuildStudents;
    Procedure RegenerateFakeExamBoards;
    -- Wipe all presonas matching '0001%'
    PROCEDURE WipeReservedRange;
    PROCEDURE AssignToFakeExamBoard(i_stu_code INTUIT.SRS_SQE.SQE_STUC%Type,Is_Postgrad in int,YearOffset in int,Is_Frozen in int, Is_Unpub in int, Is_Resit in int) ;
    
    -- Internal functions to deal with date manipulation
    --FUNCTION DateOnly(i_Date date) return date;
    --FUNCTION TimeOnly(i_Date date) return date;
    --FUNCTION BuildDate(i_day int,i_month int, i_year int) return date;
    --FUNCTION AyrEndYear(i_Date date) return int;
    FUNCTION To_Ayr(i_Date date) return varchar2;
    --FUNCTION MoveDateToYearOffset(i_Date date,i_YearOffset int) return date
    
    --Recordsets used in this package
    TYPE Ayr IS RECORD(
        ayr INTUIT.INS_AYR.AYR_CODE%type,
        yr int
    );
    TYPE Ayrs IS RECORD(
        plus1 Ayr,
        curr Ayr,
        minus1 Ayr,
        minus2 Ayr,
        minus3 Ayr,
        crsstart Ayr,
        crsend  Ayr
    );
    TYPE ContactAddress IS RECORD(
        postcode INTUIT.MEN_ADD.ADD_PCOD%type,
        add1 INTUIT.MEN_ADD.ADD_ADD1%type,
        add2 INTUIT.MEN_ADD.ADD_ADD2%type,
        add3 INTUIT.MEN_ADD.ADD_ADD3%type,
        add4 INTUIT.MEN_ADD.ADD_ADD4%type,
        add5 INTUIT.MEN_ADD.ADD_ADD5%type,
        email INTUIT.MEN_ADD.ADD_EMAD%type,
        phone INTUIT.MEN_ADD.ADD_TELN%type,
        tta_code INTUIT.SRS_TTA.TTA_CODE%type
    );
    TYPE Module IS RECORD(
         ActualAyr INTUIT.INS_AYR.AYR_CODE%type,
         StartAyr INTUIT.INS_AYR.AYR_CODE%type,
         Code INTUIT.CAM_MAV.MOD_CODE%type,
         Occurence INTUIT.CAM_MAV.MAV_OCCUR%type,
         PSL INTUIT.CAM_MAV.PSL_CODE%type,
         lvl_code INTUIT.CAM_MAV.lev_code%type,
         Credits INTUIT.INS_MOD.mod_crdt%type
    );
    TYPE ModuleList IS TABLE of Module;
    TYPE Course IS RECORD(
        yrs INTUIT.SRS_CRS.CRS_YLEN%type,
        code INTUIT.SRS_SCJ.SCJ_CRSC%type,
        ext_code INTUIT.SRS_SCE.SCE_ESB1%type,
        faculty INTUIT.SRS_SCE.SCE_FACC%type,
        fee INTUIT.SRS_FDU.FDU_AMNT%type,
        route INTUIT.SRS_SCE.SCE_ROUC%type,
        dept INTUIT.SRS_SCE.SCE_DPTC%type,
        cgroup INTUIT.SRS_SCJ.SCJ_CGPC%type,
        cblock INTUIT.SRS_SCE.SCE_BLOK%type,
        fee_status INTUIT.SRS_SCE.SCE_FSTC%type,
        fee_status_overseas INTUIT.SRS_SCE.SCE_FSTC%type,
        level_code INTUIT.INS_SPR.SPR_LEVC%type,
        attendance_method_code INTUIT.SRS_SCJ.SCJ_MOAC%type,
        loc_of_study INTUIT.SRS_ELS.ELS_CODE%type,
        fee_profile INTUIT.SRS_SCE.SCE_FPTC%type,
        is_postgrad int,
        occurrence INTUIT.SRS_SCE.SCE_OCCL%type,
        next_occurrence INTUIT.SRS_SCE.SCE_NOCL%type
    );
    TYPE Student IS RECORD(
        code INTUIT.INS_STU.STU_CODE%type,
        srtn INTUIT.INS_STU.STU_SRTN%type,
        scjc INTUIT.SRS_SCJ.SCJ_CODE%type,
        title INTUIT.INS_STU.STU_TITL%type,
        forname INTUIT.INS_STU.STU_FNM1%type,
        midname INTUIT.INS_STU.STU_FNM2%type,
        surname INTUIT.INS_STU.STU_SURN%type,
        fullname VARCHAR(250),
        initials INTUIT.INS_STU.STU_INIT%type,
		birthday INTUIT.INS_STU.STU_DOB%type,
        gender  INTUIT.INS_STU.STU_GEND%type,
        maritial_status INTUIT.INS_STU.STU_MARC%type,
        nationality INTUIT.SRS_NAT.NAT_CODE%type,
        country_of_birth INTUIT.SRS_COB.COB_CODE%type,
        ethnic_origin INTUIT.SRS_ETH.ETH_CODE%type,
        lea INTUIT.SRS_LEA.LEA_CODE%type,
        disability INTUIT.SRS_DSB.DSB_CODE%type,
        highest_qual INTUIT.INS_STU.STU_HQLC%type,
        prev_school INTUIT.INS_STU.STU_SCLC%type,
        home_overseas INTUIT.INS_STU.STU_UHOC%type,
        ext_id INTUIT.INS_STU.STU_ESID%type,
        domicile_code INTUIT.INS_STU.STU_CODC%type,
        speaks_welsh INS_STU.STU_WELS%type,
        national_id INS_STU.STU_NID1%type,
        pref_welsh_comm INS_STU.STU_WEYN%type,
        uk_vi INS_STU.STU_UKBA%type,
        religion INS_STU.STU_RELB%type,
        sex_orient INS_STU.STU_SXOR%type,
        gender_id INS_STU.STU_GNID%type,
        age int,
        welsh_mentor_pref INS_STU.STU_UDF2%type,
        gp_location INS_STU.STU_UDF3%type,
        dependants INS_STU.STU_NDEP%type,
        car_access INS_STU.STU_MVLN%type,
        prev_care  INS_STU.STU_UDFK%type
    );

    --Internal fuunctions unsuited to external use
    FUNCTION ModuleBasicDetails(i_Mod_Code INTUIT.CAM_MAV.MOD_CODE%type, ActualAyr INTUIT.INS_AYR.AYR_CODE%type, StartAyr INTUIT.INS_AYR.AYR_CODE%type) return Module;
    FUNCTION ModulesCompSci(i_ActualAyr  INTUIT.INS_AYR.AYR_CODE%type, i_StartAyr  INTUIT.INS_AYR.AYR_CODE%type, i_level INTUIT.INS_MOD.LEV_CODE%Type) return ModuleList PIPELINED;
    FUNCTION DateOnly(i_Date date) return date;
    FUNCTION TimeOnly(i_Date date) return date;
    
END;