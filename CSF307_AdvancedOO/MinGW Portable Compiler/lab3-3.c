#include <stdio.h>
#include <stdlib.h>


int main(int argc, char* argv[]) {
	unsigned int rows = 3;
	unsigned int columns = 5;

	int** arr = malloc(columns * sizeof(int*)); 
	unsigned int i;
	for (i = 0; i < columns; i++) { 
		arr[i] = malloc(rows * sizeof(int)); 
	}
	arr[0][0] = 1; arr[0][1] = 1; arr[0][2] = 2; arr[0][3] = 2; arr[0][4] = 3;
	arr[1][0] = 99; arr[1][1] = 88; arr[1][2] = 77; arr[1][3] = 66; arr[1][4] = 55;
	arr[2][0] = 0; arr[2][1] = 0; arr[2][2] = 0; arr[2][3] = 2; arr[2][4] = 2;

	unsigned int j;
	for (j = 0; j < rows; j++) {
		for (i = 0; i < columns; i++) {
			printf("%d\t", arr[j][i]);
		}
		printf("\n");
	}

	for (i = 0; i < columns; i++) {
		free(arr[i]);
	}
	free(arr);
	return 0;
}

/*
1  1  2  2  3 
99 88 77 66 55 
0  0  0  2  2
*/