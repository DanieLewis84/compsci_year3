typedef struct password_chars_t {
	char* array;
	int length;
} password_chars_t;

char* generateRandomPassword(unsigned int length, password_chars_t charArray);

char** generateRandomPasswordList(unsigned int count, unsigned int length, password_chars_t charArray);

void writePasswordsToFile(char** passwords, unsigned int count, char* filePath);

long charFrequencies(char* inputStr, unsigned  int*** tableOfFreq);