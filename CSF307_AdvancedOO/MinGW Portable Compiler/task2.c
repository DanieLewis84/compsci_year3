#include "task2.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>


int main(int argc, char* argv[]) {
	srand(time(0)); 
	//#######################={Part 1 Testing}=#######################
	//Allocates a password_chars_t struct in stack memory
	password_chars_t charSet;
	//This password_chars_t will contain 26 letters + 10 digits, so 36 characters overall
	charSet.length = 36;
	//Allocate some memory to put in our characters. This is now allocated in heap memory.
	charSet.characters = malloc(sizeof(char) * charSet.length);
	//Copy each character from the string "abcd ..." into our 'characters' array. This is a short-cut
	//to manually filling this array. You could also assign it to a string as:
	//charSet.characters = "abcdef...";. My solution just shows how it an be done using heap.
	strncpy(charSet.characters, "abcdefghijklmnopqrstuvwxyz0123456789", charSet.length);


	//#######################={Part 2 Testing}=#######################
	//Generate a few passwords to test!
	char* pwdOne = generateRandomPassword(2, &charSet);
	char* pwdTwo = generateRandomPassword(10, &charSet);
	char* pwdThree = generateRandomPassword(20, &charSet);
	char* pwdFour = generateRandomPassword(30, &charSet);

	printf("Part 2: I generated the following four passwords: [%s %s %s %s]\n", 	
																pwdOne, pwdTwo, pwdThree, pwdFour);

	//Free all the passwords we generated for testing. Could do in the end, but they're not
	//useful anymore so no reason to keep them...
	free(pwdOne);
	free(pwdTwo);
	free(pwdThree);
	free(pwdFour);


	//#######################={Part 4 Testing}=#######################
	//Generate a password list for testing
	unsigned int passwordListSize = 50000;
	char** passwordList = generatePasswordList(passwordListSize, 5, &charSet);
	printf("Part 3: The first password is: [%s] and the last is: [%s]\n", 
														passwordList[0], passwordList[passwordListSize-1]);


	//#######################={Part 5 Testing}=#######################
	char* outputFile = "myPasswordList.txt";
	printf("Part 5: Writing passwords to the file: %s\n", outputFile);
	writePasswordsToFile(passwordList, passwordListSize, outputFile);
	printf("Part 5: Writing completed.\n");


	//#######################={Part 6 Testing}=#######################
	
	unsigned int newListLength;
	char** newList = removeDuplicates(passwordList, passwordListSize, &newListLength);
	printf("Part 6: %u duplicates removed. First password: [%s] last password: [%s].\n", 
														passwordListSize - newListLength,  
														newList[0], newList[newListLength - 1]);

	//Clean up after ourselves. The passwordList has its own function to free it!
	freePasswordList(passwordList, passwordListSize);
	//Note: since in the previous line, I free'd the passwords in the passwordList,
	//I should NOT free them again in newList, as the pointers point to the same locations in memory
	//and I'd end up in a double-free situation. Instead, freeing the list alone is sufficient. 
	free(newList);
	free(charSet.characters); //Remember to free the array of chars in the struct too, that's easy to miss.


	//#######################={Part 7 Testing}=#######################
	printf("Part 7: \n");
	testCharFrequencies("");
	testCharFrequencies("?PaLAa-a?Bc1.1p@p");
	testCharFrequencies("YouHaveReadAllTheWayToTheEndHorraaayy!!");
}

//#######################={Part 2}=#######################
char* generateRandomPassword(unsigned int length, password_chars_t* charSet) {
	//Allocate memory for the string to be stored in + 1 character for null termination.
	//REMEMBER! A string is an array of characters. We can't create a local array and 
	//pass a pointer to it, that would be a pointer to a local variable that won't exist 
	//once the function returns!
	char* result = malloc(sizeof(char) * (length + 1));

	for (unsigned int i = 0; i < length; i++) {
		result[i] = selectRandomCharacter(charSet);
	}

	//Append the null terminator at the end of the result string. Do not forget this!
	result[length] = '\0';
	//Generally speaking, a better way to do strings is to zero-out the whole memory block
	//such that, if your string stops at any point (Within the bounds of the array, and without touching the last
	//element) you are guaranteed that a null terminator will exist in the end. 

	//#######################={Part 3}=#######################
	//Let's ensure there are no duplicate characters in the result.
	//Technicaly one could put this check in the above for loop with little change,
	//but for now let's see how it can be acheived as a separate step. 
	for (unsigned int i = 0; i < length - 1; i++) {
		if (result[i+1] == result[i]) {
			//This and the next character are identical. We'll change the next character to be different!
			char toAvoid = result[i + 1];
			//Find a random character that is not the same as what is already there 
			char replacement;
			do {
				replacement = selectRandomCharacter(charSet);
			} while(replacement == toAvoid);
			//Set the next character to the new replacement and continue.
			result[i+1] = replacement;
		}
	}
	return result;
}

//#######################={Part 4}=#######################
char** generatePasswordList(unsigned int listLength, unsigned int pwdLength, password_chars_t* charSet) {
	char** pwdArray = malloc(sizeof(char*) * listLength);
	for (unsigned int i = 0; i < listLength; i++) {
		pwdArray[i] = generateRandomPassword(pwdLength, charSet);
	}
	return pwdArray;
}

//#######################={Part 5}=#######################
void writePasswordsToFile(char** list, unsigned int listLength, char* pathToFile) {
	FILE* out = fopen(pathToFile, "w");
	if (out) { //Important! Ensure fopen actually opened the file. Same as: if (out != NULL){...}
 		for (unsigned int i = 0; i < listLength; i++) {
			fprintf(out, "%s\n", list[i]);
		}
		//Only close the file if the pointer 'out' isn't null.
		fclose(out);
	}
}


//#######################={Part 6}=#######################
/* This function could be implemented in a number of ways.
 * I chose to sort the input array (listFrom), and use the fact that in the sorted list
 * duplicate entries will appear next to each-other, to count them using a single loop. 
 * I then allocate the memory I need for the new list (just enough) and populate it
 * with non-duplicate elements from the input list. 
 */
char** removeDuplicates(char** listFrom, unsigned int listLength, unsigned int* newLength) {
	//First, sort the input list. Now duplicates are adjacent to each-other!
	//qsort is a standard function from stdlib.h 
	qsort(listFrom, listLength, sizeof(char*), &compareStrings);

	//Now, count how many adjacent elements are the same. This way we'll know
	//how many duplicates there are.
	unsigned int duplicateCount = 0;
	for (unsigned int i = 1; i < listLength; i++) {
		if (!strcmp(listFrom[i], listFrom[i-1])) { //if listFrom at i and listFrom at i-1 are the same, it's a duplicate.
			duplicateCount++;
		}
	}

	//Let's allocate the new array minus the duplicates
	char** newList = malloc(sizeof(char*) * (listLength - duplicateCount));
	
	//And let's go over the sorted input array once again, selecting only
	//non-duplicate elements to add to the new list.
	unsigned int newIndex = 0;
	for (unsigned int i = 1; i < listLength; i++) {
		if (strcmp(listFrom[i], listFrom[i - 1])) { //if the strings at i and i-1 are different, add the string at i-1
			newList[newIndex++] = listFrom[i - 1];
		}
	}

	//Because we're doing a pair-wise comparison in the loop, the last element will
	//always be missed. We have to add it in manually.
	newList[newIndex] = listFrom[listLength - 1];
	*newLength = newIndex + 1;
	return newList;
}


/* This function is not used by the program, but I put it in as a second way you could do Part 6 :-)
 * This function is using extra memory in the name of time efficiency. Still complexity wise it's no better
 * than the other way though. There are many alternatives but I chose to show 2 I thought would be more the most common!
 */
char** alternative_removeDuplicates(char** listFrom, unsigned int listLength, unsigned int* newLength) {
	//fist allocate extra memory. We need this to know if an element of the list listFrom was touched or not!
	//Now some will say, if listLength is big enough, I will overflow stack memory with this array.
	//That's true, it's better to allocate as: bool* flags = (bool*) malloc(sizeof(bool) * listLength);
	//But I wanted to show a stack allocation example too.
	bool flags[listLength];
	//set all elements in flag to 0. Could have also had a loop but memset does it for us (same thing here)
	memset(flags, 0, sizeof(char) * listLength);


	unsigned int duplicates = 0;
	//Go through the list of passwords.
	for (unsigned int i = 0; i < listLength; i++) {
		//We only need to check if we haven't flagged this index as "visited".
		if (flags[i] != 1) {
			char* pwdOne = listFrom[i];
			//For every password we find, go search if there are any duplicates in the list
			for (unsigned int j = i + 1; j < listLength; j++) {
				char* pwdTwo = listFrom[j];
				if (strcmp(pwdOne, pwdTwo) == 0) {
					//If we find a duplicate we 'flag' it to basically say, we are not going to 
					//visit it again in the future as to not double-count anything.
					flags[j] = 1;
					duplicates++;
				}
			}
			
		}
	}


	//We now know how many duplicates exist. Let's allocate the new array:
	char** newList = malloc(sizeof(char*) * (listLength - duplicates));
	//And set the newLength to the new value: 
	*newLength = listLength - duplicates;

	//Now we modify the algorithm we used to count the duplicates, to actually populate the new list.
	//Notice how when flagging duplicates, we haven't flagged 1 of them! (outer loop) That means all we have to do now
	//Is copy over all passwords that haven't been flagged:

	unsigned int newListIndex = 0;
	for (unsigned int i = 0; i < listLength; i++) {
		if (!flags[i]) { //if flags[i] == 0
			newList[newListIndex++] = listFrom[i];
		}
	}

	//We don't need to de-allocate flags, its an array in stack memory which we can leave there be.
	//If it was in heap however, we would have to free(flags) at this point.
	return newList;
}




//#######################={Part 7}=#######################
long charFrequencies(char* inputStr, unsigned int*** tableOfFreq) {
	unsigned int inLen = strlen(inputStr);
	if (inLen == 0) {
		return -1; //empty string can't be computed.
	}

	char clone[inLen+1]; //Local array of chars. Using malloc here is also fine, but you must free before returning!

	//Step 1: Clone the input string to a local variable where we can tinker with it as much as we like ;-)
	strcpy(clone, inputStr);

	//Step 2: Sort the characters (can use qsort ofcourse, i decided to do bubble sort for a change)
	for (unsigned int i = 0 ; i < inLen - 1; i++) {
	    for (unsigned int j = 0 ; j < inLen - i - 1; j++) {
	 	    if (clone[j] > clone[j + 1]) {
	    		char c = clone[j + 1];
	    		clone[j + 1] = clone[j];
	    		clone[j] = c; 
	    	}
		}
	}

	//Step 3: Count the number of distinct characters in 'clone'
	unsigned int distinct = 1;
	for (unsigned int i = 1; i < inLen; i++) {
		if (clone[i] != clone[i-1]) {
			distinct++;
		}
	}


	//Step 4: Allocate the 2D table (I didn't say in which orrientation you have to do this allocation
	//to give you some freedom to do it however you like. This is one of 2 possible orrientations.)
	unsigned int** table = malloc(sizeof(unsigned int*) * distinct);
	for (unsigned int i = 0; i < distinct; i++) {
		table[i] = malloc(sizeof(unsigned int) * 2);
	}

	//Step 5: Populate the table with counts
	for (unsigned int i = 0, d = 0; i < distinct; i++) {
		table[i][0] = clone[d];
		unsigned int nextDistinct = d;
		do {
			nextDistinct++;
		} while(clone[nextDistinct] == clone[nextDistinct - 1]);
		table[i][1] = nextDistinct - d;
		d = nextDistinct;
	}

	//Step 6: Pass back the pointer to the table and return the length
	*tableOfFreq = table;
	return distinct;

}


//#######################={Helper Functions}=#######################
//This function is like a Comparator would be in Java. It takes two pointers and compares the elements they point to
//void pointers are essentially pointers without a specific type - they can be of any type. 
//More info about qsort here: https://man7.org/linux/man-pages/man3/qsort.3.html
int compareStrings(const void* strOne, const void* strTwo) {
    return strcmp(*(char**) strOne, *(char**) strTwo);
}

char selectRandomCharacter(password_chars_t* charSet) {
	//Generate a random integer between 0 and the number of chars in charSet
	unsigned int randomCharIndex = rand() % charSet->length;
	//Select the character at that index from the charSet
	char randomChar = charSet->characters[randomCharIndex];
	//Set the corresponding character in the result to that random character
	return randomChar;
}

void freePasswordList(char** pwdList, unsigned int listLength) {
	//Free each password from the list
	for (unsigned int i = 0; i < listLength; i++) {
		free(pwdList[i]);
	}
	//And then free the list itself.
	free(pwdList);
}

void testCharFrequencies(char* strIn) {
	//Kinda "do all" function, but it's basically a wrapper for testing the charFrequencies
	//function. It handles result collection, pretty-printing, and freeing the returned
	//results.
	unsigned int** table;
	long status = charFrequencies(strIn, &table);
	printf("charFrequencies returned %ld for the input: [%s].\n", status, strIn);
	if (status < 0) {
		return;
	}

	//The bellow is a crude effort to pretty-print the data. Going a bit over the top but oh well.
	for (long l = 0; l < status; l++) {
		printf("| %c ", table[l][0]);
	}
	printf("|\n");
	for (long l = 0; l < status; l++) {
		printf("+---");
	}
	printf("+\n");
	for (long l = 0; l < status; l++) {
		printf("|%3u", table[l][1]);
	}
	printf("|\n\n");

	//Important! 
	for (long l = 0; l < status; l++) {
		free(table[l]);
	}
	free(table);

}