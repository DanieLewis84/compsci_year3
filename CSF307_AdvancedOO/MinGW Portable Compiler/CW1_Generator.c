#include<stdio.h>
#include<string.h>
#include<ctype.h>
#include<stdlib.h>
#include<time.h>
#include<stdbool.h>
#include "CW1_Generator.h"

char* generateRandomPassword(unsigned int length, password_chars_t charArray) {
	unsigned int arraySize = sizeof(char)*(length+1);//+1 for terminator
	//printf("Assigning mem for array of size %d\n", arraySize);
	char* pwd = (char*)malloc(arraySize);
	if (pwd == NULL) {
		printf("Could not allocated required memory block for password of %d characters.\n", arraySize);
		exit(1);
	}

	unsigned int i;
	for (i = 0; i < length; i++) {
		pwd[i] = charArray.array[(rand() % charArray.length)];
		// technically this can end up looking just outside the array bound when i=0
		while (pwd[i] == pwd[i - 1]) {
			// Quick and dirty way to grab a random index from charArray.
			//  Works fine so long as charArray.length is small compared to the upper bound for int.
			pwd[i] = charArray.array[(rand() % charArray.length)];
		}
	}
	pwd[length] = 0; // slot in the string terminator

	return pwd;
}

char** generateRandomPasswordList(unsigned int count, unsigned int length, password_chars_t allowedChars) {
	allowedChars.length = strlen(allowedChars.array);
	unsigned int i;
	char** passwords = (char**)malloc(count*sizeof(char*));
	if (passwords == NULL) {
		printf("Could not allocated required memory block for password of %d characters.\n", count * sizeof(char*));
		exit(1);
	}
	for (i = 0; i < count; i++) {
		passwords[i] = generateRandomPassword(length, allowedChars);
//		printf("Password[%d]=\"%s\"\n", i, passwords[i]);
	}
	return passwords;
}

void writePasswordsToFile(char** passwords, unsigned int count, char* filePath) {
	const char* WRITE_MODE = "w";
	FILE *to;
	to = fopen(filePath, WRITE_MODE);
	if (to) {
		printf("Writing %d passwords to \"%s\"...", count, filePath);
		unsigned int i;
		for (i = 0; i < count; i++) {
			fprintf(to, "%s\n", passwords[i]);
		}
		fclose(to);
		printf(" done.\n");
	} else
	{
		printf("Could not open target file \"%s\" for writing.", filePath);
		exit(-1);
	}
}

char** removeDuplicates(char** from, unsigned int fromLength, unsigned int* newLength) {
	unsigned int i;
	unsigned int j;
	unsigned int duplicates = 0;
	char* tmp;
	for (i = 0; i < fromLength;i++) {
		for (j = duplicates; j < i; j++) {
			if (strcmp(from[i],from[j])==0) {
				// Shove duplicates to the first non-duplicate entry in the list.
				//printf("Swapping from[%d] and from[%d] (\"%s\" & \"%s\")\n", duplicates, j, from[duplicates], from[j]);
				tmp = from[duplicates];
				from[duplicates] = from[j];
				from[j] = tmp;
				duplicates++;
				break;
			}
		}
	}
	//printf("%d duplicate passwords from %d total.\n", duplicates, fromLength);
	*newLength = fromLength - duplicates;
	char** uniquePasswords = (char**)malloc(*newLength * sizeof(char*));
	for (i = 0; i < *newLength; i++) {
		uniquePasswords[i] = (char*)malloc(strlen(from[i + duplicates])*sizeof(char));
		//clone values, rather than pointers, as we want a copy of the data.
		strcpy(uniquePasswords[i], from[i + duplicates]);
	}
	printf("%d unique passwords found from %d total.\n", *newLength, fromLength);
	return uniquePasswords;
}

long charFrequencies(char* inputStr, unsigned int*** tableOfFreq) {
	long length = 0;
	long i;
	long j;
	unsigned int** table;
	bool b;
	int inputStrLen = strlen(inputStr);
	printf("charFrequencies(\"%s\"), length=%d\n", inputStr, inputStrLen);
	//Quickly count the number of unique characters, so can calculate the size of tableOfFreq
	//Sometimes seems to fail silently on spaces in this loop. Not sure why.
	for (i = 0; i < inputStrLen; i++) {
		b = true;
		for (j = 0; j < i; j++) {
			if (inputStr[i]==inputStr[j]) {
				b= false;
			}
		}
		if (true == b) {
			length++;
		}
	}
	printf("charFrequencies, Doing memory allocation\n");
	//printf(" for length %ld\n", length);
	table = (unsigned int**)malloc(2 * sizeof(unsigned int*));// 2 subarray pointers
	if (table == NULL) {
		printf("Could not allocated required memory block for table of %d characters.\n", 2 * sizeof(unsigned int*));
		exit(1);
	}
	table[0] = (unsigned int*)malloc(length * sizeof(char));// 1 subarray of characters
	if (table[0] == NULL) {
		printf("Could not allocated required memory block for character table of %d characters.\n", length * sizeof(char));
		exit(1);
	}
	table[1] = (unsigned int*)malloc(length * sizeof(unsigned int));// 1 subarray for the count
	if (table[1] == NULL) {
		printf("Could not allocated required memory block for unsigned int table of %d characters.\n", 2 * sizeof(unsigned int*));
		exit(1);
	}
	printf("charFrequencies, memory allocation done\n");
	unsigned int count=0;
	for (i = 0; i < inputStrLen; i++) {
		b = true;
		for (j = 0; j < count; j++) {
			if (inputStr[i] == table[0][j]) {
				//printf("%c+1 ", table[0][j]);
				b = false;
				table[1][j]= table[1][j]+1;
				//printf("count=%d\t", count);
				break;
			}
		}
		if (true == b) {
			table[0][count] = inputStr[i];
			table[1][count] = 1;
			count++;
			//printf("count=%d\t%c=0 ", count, inputStr[i]);
		}
	}
	*tableOfFreq = table;
	return length;
}

int main(int argc, char* argv[]) {
	srand(time(0)); // Initiate the randomiser
	password_chars_t allowedChars;
	allowedChars.array = "abcdefghijklmnopqrstuvwxyz1234567890@#$";
	long l;
	unsigned int i;

	unsigned int count = 50000;
	unsigned int length = 5;
	char** passwords=generateRandomPasswordList(count, length, allowedChars);
	printf("Passwords generated, checking output\n");
	printf("Password[%d]=\"%s\"\n", 0, passwords[0]);
	printf("Password[%d]=\"%s\"\n", count - 1, passwords[count-1]);
	char* filePath = "passwords.txt";
	writePasswordsToFile(passwords, count, filePath);
	unsigned int uniqueLength;
	printf("Removing duplicates\n");
	char** uniquePasswords=removeDuplicates(passwords, count, &uniqueLength);
	//printf("new length = %d\n", uniqueLength);
	filePath = "passwords_uniq.txt";
	printf("Writing to uniq file\n");
	writePasswordsToFile(uniquePasswords, uniqueLength, filePath);

	char* inputStr1 = "223851@#$";
//	char* inputStr1 = "abbcccddddeeeeeffffffggggggghhhhhhhh";
	unsigned int** tableOne;
	printf("Testing character frequencies for : \"%s\"\n", inputStr1);
	long charFreqLen1 = charFrequencies(inputStr1, &tableOne);
	printf("Frequencies:\n");
	for (l = 0; l < charFreqLen1; l++) {
		printf("\"%c\"=%d\t", tableOne[0][l], tableOne[1][l]);
	}
	printf("\n");
	char* inputStr2 = "All Roads Lead To Rome";
	unsigned int** tableTwo;
	printf("Testing character frequencies for : \"%s\"\n", inputStr2);
	long charFreqLen2 = charFrequencies(inputStr2, &tableTwo);
	printf("Frequencies:\n");
	for (l = 0; l < charFreqLen2; l++) {
		printf("\"%c\"=%d\t", tableTwo[0][l], tableTwo[1][l]);
	}
	printf("\n");


	// Memory Cleanup
	printf("Clean original passwords\n");
	for (i = 0; i < count; i++) {
		//printf("\rPassword[%d]=\"%s\"", i, passwords[i]);
		free(passwords[i]);
	}
	free(passwords);

	printf("Clean unique passwords\n");
	for (i = 0; i < uniqueLength; i++) {
		//printf("\runiquePasswords[%d]=\"%s\"", i, uniquePasswords[i]);
		free(uniquePasswords[i]);
	}
	free(uniquePasswords);

	printf("\nCleanup table one\n");
	printf("tableOne[0]\n");
	free(tableOne[0]);
	printf("tableOne[1]\n");
	free(tableOne[1]);
	printf("tableOne\n");
	free(tableOne);

	printf("\nCleanup table two:\n");
	//printf("tableTwo[0]\n");
	//free(tableTwo[0]); // Crashes on windows, not sure why, as using the same pattern for tableOne, which works.
	//printf("tableTwo[1]\n");
	//free(tableTwo[1]); // Crashes on windows, not sure why, as using the same pattern for tableOne, which works.
	printf("tableTwo\n");
	free(tableTwo);
	

	printf("Run completed successfully");// Added because of silent errors on windows
	return 0;
}