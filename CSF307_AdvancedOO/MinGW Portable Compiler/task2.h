//#######################={Part 1}=#######################
typedef struct {
	//We shouldn't put a fixed-size array in the struct. Instead, we put a pointer to char
    //which we can use to point to the array upon initialization.
	char* characters; 

	//The field length should be unsigned, since we know it will never be negative. 
	//unsigned int is sufficiently big!
	unsigned int length; 
} password_chars_t;




char* generateRandomPassword(unsigned int length, password_chars_t* charSet);
char selectRandomCharacter(password_chars_t* charSet);
char** generatePasswordList(unsigned int listLength, unsigned int pwdLength, password_chars_t* charSet);
void freePasswordList(char** pwdList, unsigned int listLength);
void writePasswordsToFile(char** list, unsigned int listLength, char* pathToFile);
char** removeDuplicates(char** listFrom, unsigned int listLength, unsigned int* newLength);
long charFrequencies(char* inputStr, unsigned int*** tableOfFreq);

//Technically this shouldn't be here, but we haven't looked atstatic functions so we'll put it here for now.
int compareStrings(const void* strOne, const void* strTwo); 
void testCharFrequencies(char* strIn); // Same as above
