#include<stdio.h>
#include "lab2-1.h"

unsigned long long factorial(int i) {
	if (1 == i) {
		return 1;
	}
	else if (1 > i) {
		return 0;
	}
	return i * factorial(i - 1);
}

int main(int argc, char* argv[]) {
	int i;
	int n;
	unsigned long long nFactorial;
	n = atoi(argv[1]);
	if (n > 20)
	{ 
		printf("Input %d is out of range, must be between 1 and 20.", n); 
		return -1; 
	}
	if (n < 1)
	{
		printf("Input %d is out of range, must be between 1 and 20.", n);
		return -1;
	}
	nFactorial = factorial(n);
	printf("%d! = %llu", n, nFactorial);
	return 0;
}
