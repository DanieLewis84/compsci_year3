#include <stdio.h>
void writeIntsToFile(char* path, int* array, unsigned int arraySize);
int main(int argc, char* argv[]) {
	unsigned int arraySize = 9;
	int array[] = {-1, 99, 2, 684, 1, 60, -90, 0, -1};
	writeIntsToFile("MyFile.txt", array, arraySize);
	return 0;
}

void writeIntsToFile(char* path, int* array, unsigned int arraySize) {
	FILE *to;
	to = fopen(path, "w");  //write mode
	if (to) {
		unsigned int i;
		for (i = 0; i < arraySize; i++) {
			printf("Writing: %d\n", array[i]);
			fprintf(to, "%d\n", array[i]);
		}
		fclose(to);
	}
}