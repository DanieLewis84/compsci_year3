#include "ExamRough.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
#define AREA_OF_TRIANGLE(base, height) ((base)*(height)*0.5)


long* findNegative(long* arr, unsigned int arrSize, unsigned int* returnedSize);


int compareLongs(const void * a, const void * b)
{
	long ret = (*(long*)a - *(long*)b);
	// concatinate output size, as long is bigger than int.
	if (ret > 0) return 1;
	if (ret < 0) return -1;
	return 0;
}

long* findNegative(long* arr, unsigned int arrSize, unsigned int* returnedSize) {
	// Move all negatives to start of the list, this will speed up the copy step
	qsort(arr, arrSize,sizeof(long),&compareLongs);

	unsigned int negativeCount = 0;
	unsigned int i;
	for (i = 0; i < arrSize; i++) {
		if (arr[i]<0) {
			negativeCount++;
		}
	}
	*returnedSize = arrSize - negativeCount;
	long* newList = malloc(sizeof(int) * (*returnedSize));
	
	for (i = 0; i < *returnedSize; i++) {
		newList[i] = arr[i+negativeCount];
	}
	return newList;
}

int main(int argc, char* argv[]) {
	//return 0; //Successful termination
	/*

	find

	typedef struct {
		unsigned int employeeId;
		char* fullName;
		float* payments;// Could also represent as using unsigned int* to represent the number of pennies, which provides better accuracy, but more formatting.
		unsigned int paymentCount;
	} employee_t;


	employee_t aEmployee = malloc(sizeof(employee_t)*);
	aEmployee->employeeId = 96049;
	aEmployee->fullName = "Jon Smith";
	aEmployee->payments = malloc(sizeof(float) * 3);
	aEmployee->payments[0] = 1200;
	aEmployee->payments[1] = 1100;
	aEmployee->payments[2] = 1300;
	aEmployee->paymentCount = 3;
	*/

	/*



	Fruit_t apple = { .name = "Stack Apple",.flavour = 1,.price = 0.23 };
	Fruit_t* banana = (Fruit_t*)malloc(sizeof(Fruit_t));
	banana->name = "Banana Heap";
	banana->flavour = 2;
	banana->price = 0.10;

	banana->name = "Banana Heap";
	banana->flavour = 2;
	banana->price = 0.10;

	char* localString = "Hello";
	int stringLength = strlen(localString)+1;
	char* copyOfLocal = (char*)malloc(sizeof(char)*stringLength); 
	int i;
	for (i = 0; i < stringLength; i++) { 
		copyOfLocal[i] = localString[i]; 
	}
	printf("%sWorld", copyOfLocal);

	*/
}