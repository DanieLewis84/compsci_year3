#include <string.h>
#include <stdio.h>
#include <ctype.h>

//Declaring the checkStrong function. Not using a header file!
unsigned long checkStrong(char* pwd);

int main(int argc, char* argv[]) {
	if (argc > 1) {
		//Go through each of the user-provided program arguments
		for (unsigned int i = 1; i < argc; i++) {
			char* password = argv[i];
			unsigned long score = checkStrong(password);
			//Print the score using %lu since it's an unsigned long.
			printf("%s: %lu\n", password, score);
		}
		return 0; //Successful termination
	} else {
		printf("Error: No arguments provided!");
		return -1; //Error!
	}
}

unsigned long checkStrong(char* pwd) {
	unsigned long passwordLength = strlen(pwd);
	unsigned long symbolCount = 0;
	unsigned long digitCount = 0;
	unsigned long letterCount = 0;

	for (unsigned long i = 0; i < passwordLength; i++) {
		char c = pwd[i];
		if (isalpha(c)) {
			letterCount++;
		} else if (isdigit(c)) {
			digitCount++;
		} else if (ispunct(c)) {
			symbolCount++;
		}
	}
	
	unsigned long score = 0;
	score += 30 * passwordLength;
	score += 11 * symbolCount;
	score += 3  * digitCount;
	score +=      letterCount;
	return score;
}