#include<stdio.h>
#include<string.h>
#include<ctype.h>
#include "CW1_Checker.h"

unsigned long checkStrong(char* pwd ) {
	int length = strlen(pwd);

	// Calculate totals by character type
	int i;
	int symbolCount = 0;
	int digitCount = 0;
	int letterCount = 0;
	for (i = 0; i < strlen(pwd); i++) {
		if (isalpha(pwd[i])) {
			letterCount++;
		}
		else if (isdigit(pwd[i])) {
			digitCount++;
		}
		else if(ispunct(pwd[i])){
			symbolCount++;
		}
	}

	// Generate score
	unsigned long score = (length * 30);
	//printf("l=%d s=%d d=%d a=%d\t", length, symbolCount, digitCount, letterCount);
	score += (symbolCount * 11);
	score += (digitCount * 3);
	score += letterCount;

	return score;
}

int main(int argc, char* argv[]) {
	int i;
	int iTotal = 0;
	if (argc < 2) {
		printf("%s ERROR: No arguments supplied, expecting at least 1.", argv[0]);
		return -1;
	}
	for (i = 1; i < argc; i++) {
		printf("%s: %u\n", argv[i], checkStrong(argv[i]));
	}
	return 0;
}