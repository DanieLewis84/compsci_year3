#include<stdio.h>
#include<stdlib.h>
#include "lab3-1.h"


int main(int argc, char* argv[]) {
	Fruit_t apple = { .name = "Stack Apple",.flavour = 1,.price = 0.23 };
	Fruit_t* banana = (Fruit_t*) malloc(sizeof(Fruit_t));
	banana->name = "Banana Heap";
	banana->flavour = 2;
	banana->price = 0.10;
	printf("An %s is of flavour %d, and costs %f\n", apple.name, apple.flavour, apple.price);
	printf("An %s is of flavour %d, and costs %f\n", banana->name, banana->flavour, banana->price);
	free(banana);
	return 0;
}