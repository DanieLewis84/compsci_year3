#include <stdio.h>
int main(int argc, char* argv[]) {
   int i;
   int n;
   unsigned long long nFactorial;
   n=atoi(argv[1]);
   nFactorial=1;
   if (0==n) {
	   nFactorial=0;
   }
   for (i=1; i<=n; i++) {
	   nFactorial*=i;
   }
   printf ("%d! = %llu", n, nFactorial);
   return 0;
}