#include<stdio.h>

void solve(int len, unsigned int arr[], unsigned int x) {
	int i, j;

	printf("x=%d\narr = [", x);
	for (i = 0; i < len; i++) {
		printf("%u, ", arr[i]);
	}
	printf("]\n");

	for (i = 0; i < len; i++) {
		for (j = 0; j < i; j++) {
			if (arr[i] + arr[j] == x) { 
				printf("Solution found %d + %d = %d\n", arr[i], arr[j], x);
				printf("Index %d and %d of the array arr sum up to %d\n", i, j, x);
				return;
			}
		}
	}
	printf("No solution found for %d\n", x);
}

int main(int argc, char* argv[]) {
	int i;
	int iTotal = 0;
	unsigned int x=atoi(argv[1]);
	unsigned int arr[argc-2];
	for (i = 2; i < argc; i++) {
		arr[i-2] = atoi(argv[i]);
	}

	solve(argc - 2, arr, x);
	return 0;
}