//enum flavour { SOUR = 0, SWEET = 1, BITTER = 2 };

typedef struct Fruit
{
	char* name;
	int flavour;
	float price;
} Fruit_t;
