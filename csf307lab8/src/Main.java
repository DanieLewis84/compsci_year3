import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

public class Main {
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		Person person = new Person(18,200.0,"Bob Bobson" );
		ByteArrayOutputStream baos= new ByteArrayOutputStream();
		ObjectOutputStream oos= new ObjectOutputStream(baos);

		oos.writeObject(person);
		//person.writeObject(oos);
		System.out.println(person.toString());
		
		byte[] serializedObject= baos.toByteArray();
		System.out.println(new String(serializedObject, StandardCharsets.UTF_8));
		ByteArrayInputStream i= new ByteArrayInputStream(serializedObject); 
		ObjectInputStream oi = new ObjectInputStream(i);
		Person personFromFile = (Person) oi.readObject();
		
		//personFromFile.readExternal(oi);	
		System.out.println(personFromFile.toString());
		
		

		
		Gson g = new GsonBuilder().create(); 
		URL api;
		try {
			api = new URL("https://ipapi.co/1.1.1.1/json/");
			IPQuery data = g.fromJson(new InputStreamReader(api.openStream()), IPQuery.class);
			System.out.println( "IP: " + data.ip);
			System.out.println( "Country: " + data.country);
			System.out.println( "Region: " + data.region);
			System.out.println( "Country Name: " + data.countryName);
			System.out.println( "Is in the EU: " + (data.inEU?"Yes":"No"));
			System.out.println( "Latitude: " + data.latitude);
			System.out.println( "Longitude: " + data.longitude);
			System.out.println( "Organization: " + data.org);
		} catch (JsonSyntaxException | JsonIOException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
}
