import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class Person implements Externalizable {
	private int age;
	private String fullName;
	private double weightKg;

	public Person() {
		
	}
	
	public Person(int age, double weight, String fullName) {
		this.age = age;
		this.weightKg = weight;
		this.fullName = fullName;
	}
	


	@Override
	public String toString() {
		return "Person [age=" + age + ", fullName=" + fullName + ", weightKg=" + weightKg + "]";
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public double getWeightKg() {
		return weightKg;
	}

	public void setWeightKg(double weightKg) {
		this.weightKg = weightKg;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		this.age=in.readInt();
		this.weightKg = in.readDouble();
		this.fullName = in.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeInt(this.age);
		out.writeDouble(this.weightKg);
		out.writeUTF(this.fullName);
	}

}
