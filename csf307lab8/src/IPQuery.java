import com.google.gson.annotations.SerializedName;

public class IPQuery {

    public String ip;
    public String region;
    public String country;
    @SerializedName("country_name")
    public String countryName;
    @SerializedName("in_eu")
    public boolean inEU;
    public double latitude;
    public double longitude;
    public String org;
	
	
}
