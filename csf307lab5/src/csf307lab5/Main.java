package csf307lab5;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
	public static int fib(int n) {
		if (n <= 1) {
			return n;
		}
		return fib(n - 2) + fib(n - 1);
	}
	

	public static void main(String[] args) {
		ArrayList<Integer> fibList=new ArrayList<Integer>();
		for (int i=1;i<=31;i++) {
			fibList.add((i));
			//System.out.println("fib("+i+")="+fib(i));
		}
		
		List<Integer> outList=fibList.stream().map(Main::fib).collect(Collectors.toList());
		
		List<Integer> outListParallel=fibList.stream().parallel().map(Main::fib).collect(Collectors.toList());

		System.out.println("FibList:");
		for (int i=1;i<outList.size();i++) {
			System.out.println(outList.get(i));
		}
		System.out.println("FibList (parallel):");
		for (int i=1;i<outListParallel.size();i++) {
			System.out.println(outListParallel.get(i));
		}
		
	}
}
