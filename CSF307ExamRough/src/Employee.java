public class Employee {
	private final int employeeID;
	private String preferedName;

	public Employee(int id, String preferedName) {
		this.employeeID = id;
		this.preferedName = preferedName;
	}

	public int getEmployeeID() {
		return employeeID;
	}

	public String getPreferedName() {
		return preferedName;
	}

	public void setPreferedName(String preferedName) {
		this.preferedName = preferedName;
	}

	@Override
	public int hashCode() {
		int result;
		result = employeeID;
		if (preferedName!=null) {
			for (int i = 0; i < preferedName.length(); i++) { 
				result += (int)preferedName.charAt(i);
			}
		}
		return result;
	}
	
	
	public boolean objectEquality(String a, String b) {
		return a==b;
	}
	
	

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (employeeID != other.employeeID)
			return false;
		if (preferedName == null) {
			if (other.preferedName != null)
				return false;
		} else if (!preferedName.equals(other.preferedName))
			return false;
		return true;
	}
	
	


}
