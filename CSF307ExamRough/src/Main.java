import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Consumer<String> printer = (a) -> System.out.println((a.isEmpty()?"Empty":"Non-empty"));
		
		
		List<Integer> nums = new ArrayList<>();

		nums.stream().filter(b -> b>20).distinct().forEach(b -> System.out.println(b));
		
		Callable<Boolean> checkPrime = MyClass::isPrime;
		ExecutorService ex = Executors.newSingleThreadExecutor();
		//Write Java code to execute checkPrime once, and print the result when the computation finishes
		ex.submit(checkPrime);
		ex.shutdown();
		while(!ex.awaitTermination(500,  TimeUnit.MILLISECONDS));
		System.out.println("Result: " + MyClass.isPrime);
	}

	
	public static <T> T method(List<T> t)
	{
		return null;
		
	}
}
