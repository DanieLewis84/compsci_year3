import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class TwinPrimes {
	public static int slowCountTwinPrimes(int upTo) {
		int count = 0;
		for (int i = 3; i <= upTo - 2; i++) {
			boolean areTwins = true;
			for (int j = 2; j < i; j++) {
				if (i % j == 0 || (i + 2) % j == 0) {
					areTwins = false;
					break;
				}
			}
			if (areTwins) {
				count++;
			}
		}
		return count;
	}
	
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		ExecutorService service = Executors.newFixedThreadPool(2);
	
		for(int i = 100000; i <= 200000; i = i + 20000) {
			int temp=i;
			service.execute(new Runnable() {
			    public void run() {
			    	System.out.println(slowCountTwinPrimes(temp));
			    }
			});
		}
		// Tell the ExecutorService I'm done adding tasks. Can technically skip in this case, but seems bad form.
		service.shutdown();
		// Wait on everything coming back before continuing onto any dependent methods.
		while(!service.awaitTermination(500, TimeUnit.MILLISECONDS));
		
	}
	
}
