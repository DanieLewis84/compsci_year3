import javax.crypto.SecretKey;

public class Main {

	public static void main(String[] args) {
		String plaintext = "Hello World";
		try {
			DES des = new DES();
			SecretKey sessionKey = des.generateRandomKey();
			String ciphertext = des.encrypt(plaintext,sessionKey);
			System.out.println("Ciphertext: "+ciphertext);
			String message = des.decrypt(ciphertext,  sessionKey);
			System.out.println("Plaintext: "+message);
		}
		catch (Exception e)
		{
			System.out.println("Exception occured");
		}

	}

}
