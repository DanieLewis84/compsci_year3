
public class SimplePair<T extends Comparable<T>> implements Comparable<SimplePair<T>> {

	private T left;
	private T right;
	
	public SimplePair(T left, T right) {
		super();
		this.left = left;
		this.right = right;
	}
	
	public void testSimpleCompare(SimplePair<T> obj2, int expected) {
		int compare = this.compareTo(obj2);
		boolean b = (compare == expected);
		if (b) {
			System.out.println("Result of comparing " + this + " to " + obj2 + " = " + compare);
		} else {
			System.out.println("ERROR: Unexpected result when comparing " + this + " to " + obj2 + ", expected: "
					+ expected + " actual: " + compare);
		}

	}

	public T getLeft() {
		return left;
	}

	public void setLeft(T left) {
		this.left = left;
	}

	public T getRight() {
		return right;
	}

	public void setRight(T right) {
		this.right = right;
	}

	@Override
	public String toString() {
		return "SimplePair(" + left + ", " + right + ")";
	}

	@Override
	public int compareTo(SimplePair<T> other) {
		int c = getLeft().compareTo(other.getLeft());
		if (c == 0) {
			c = getRight().compareTo(other.getRight());
		}
		return c;
	}

}
