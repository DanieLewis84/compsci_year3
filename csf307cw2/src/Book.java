
public class Book {
	private String title;
	private String mainAuthor;
	private String iSBN;
	private double launchPrice;
	private int copiesSold;// Most ever sold is 5'094'805, Vs. int limit of
							// 2'147'483'647.


	public Book(String title, String mainAuthor, String iSBN, double launchPrice, int copiesSold) {
		super();
		this.title = title;
		this.mainAuthor = mainAuthor;
		this.iSBN = iSBN;
		this.launchPrice = launchPrice;
		this.copiesSold = copiesSold;
	}

	@Override
	public String toString() {
		String t = (title==null)?"null":'"'+title+'"';
		String a = (mainAuthor==null)?"null":'"'+mainAuthor+'"';
		String i = (iSBN==null)?"null":'"'+iSBN+'"';
		return "Book [title=" + t + ", mainAuthor=" + a + ", ISBN=" + i + ", launchPrice=" + launchPrice
				+ ", copiesSold=" + copiesSold + "]";
	}
	
	// BOOKTITLE, AUTHOR, ISBN, LAUNCHPRICE, COPIESSOLD
	public String toCSV() {
		return String.format("%s, %s, %s, %f, %d",title,mainAuthor,iSBN, launchPrice,copiesSold);
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((iSBN == null) ? 0 : iSBN.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		// if exactly 1, is null, then they can't be equal
		if ((iSBN == null) != (other.iSBN == null))
			return false;
		if (!iSBN.equals(other.iSBN))
			return false;
		if ((title == null) != (other.title == null))
			return false;
		if (!title.equals(other.title))
			return false;
		return true;
	}

	public String getTitle() {
		return title;
	}

	public String getMainAuthor() {
		return mainAuthor;
	}

	public String getISBN() {
		return iSBN;
	}

	public double getLaunchPrice() {
		return launchPrice;
	}

	public int getCopiesSold() {
		return copiesSold;
	}
	
	
	

}
