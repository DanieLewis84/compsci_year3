
public class AuthorProfit implements Comparable<AuthorProfit> {
	private String authorName;
	private double totalProfit;

	public AuthorProfit(String authorName, double totalProfit) {
		super();
		this.authorName = authorName;
		this.totalProfit = totalProfit;
	}

	public AuthorProfit(String authorName) {
		this(authorName, 0.0);
	}

	public AuthorProfit() {
		this(null, 0.0);
	}

	public double getTotalProfit() {
		return totalProfit;
	}

	public void addProfit(double newProfit) {
		// System.out.printf("Adding profit of %f to %f for author\"%s\"\n", newProfit, this.totalProfit, this.authorName);
		this.totalProfit += newProfit;
	}

	public String getAuthorName() {
		return authorName;
	}

	@Override
	public String toString() {
		return "AuthorProfit [authorName=" + authorName + ", totalProfit=" + totalProfit + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((authorName == null) ? 0 : authorName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthorProfit other = (AuthorProfit) obj;
		if (authorName == null) {
			if (other.authorName != null)
				return false;
		} else if (!authorName.equals(other.authorName))
			return false;
		return true;
	}

	@Override
	public int compareTo(AuthorProfit arg0) {
		return this.authorName.compareTo(arg0.getAuthorName());
	}

}
