import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BookManager {

	private File file;

	private static final double MINIMUM_SIMILARITY = 0.65;

	Set<Book> bookList = new HashSet<>();// Enforces uniqueness

	public Set<Book> getBookList() {
		return bookList;
	}

	public int getBookCount() {
		return bookList.size();
	}

	public Book getBookByISBN(String isbn) {
		return bookList.stream().filter(b -> b.getISBN().equals(isbn)).findAny().orElse(null);
	}

	public BookManager(File file) {
		super();
		this.file = file;
	}

	public void loadBooks() throws FileNotFoundException {
		// CSV format is:
		// BOOKTITLE, AUTHOR, ISBN, LAUNCHPRICE, COPIESSOLD
		// Skip blank lines
		Scanner in;
		in = new Scanner(this.file);

		while (in.hasNextLine()) {
			String line = in.nextLine();
			if (!line.isEmpty()) {
				String[] fields = line.split(", ");
				if (fields.length == 5) {
					String title = fields[0];
					String author = fields[1];
					String isbn = fields[2];
					String launchPriceString = fields[3];
					String copiesSoldString = fields[4].replaceAll("[^0-9]", "");
					double launchPrice = Double.parseDouble(launchPriceString);
					int copiesSold = Integer.parseInt(copiesSoldString);
					this.bookList.add(new Book(title, author, isbn, launchPrice, copiesSold));
				} else {
					System.out.println(String.format("Discarded malformed line: \"%s\", total characters: %d", line,
							line.length()));
				}
			}
		}
		in.close();
	}

	public static double checkSimilar(String a, String b) {
		// We want this to be very fast, so use char[], rather than String
		// The whole procedure is in O(n log n), because of calls to
		// Array.sort()
		char[] sortedA = a.toCharArray();
		char[] sortedB = b.toCharArray();
		Arrays.sort(sortedA);
		Arrays.sort(sortedB);
		// Make a copy of the 2 strings sorted together
		char[] sortedAB = new char[sortedA.length + sortedB.length];
		int i;
		for (i = 0; i < sortedA.length; i++) {
			sortedAB[i] = sortedA[i];
		}
		for (i = 0; i < sortedB.length; i++) {
			sortedAB[i + sortedA.length] = sortedB[i];
		}
		Arrays.sort(sortedAB);
		// For each character in the combined array count how many are in each
		// of the other 2 arrays.
		// Note: I don't reset the k and j, this avoids going into O(n^3)
		int offsetA, offsetB;
		int j = 0;
		int k = 0;
		int diff = 0;
		for (i = 0; i < sortedAB.length; i++) {
			if (i == 0 || sortedAB[i] != sortedAB[i - 1]) {
				offsetA = j;
				offsetB = k;
				while (j < sortedA.length && sortedA[j] == sortedAB[i]) {
					j++;
				}
				while (k < sortedB.length && sortedB[k] == sortedAB[i]) {
					k++;
				}
				diff += Math.abs((offsetA - j) - (offsetB - k));
			}
		}
		// diff is equivalent to (D1+D2)
		// Similarly, sortedAB.length is equal to sortedA.length+sortedB.length
		return 1.0 - ((double) diff / (double) sortedAB.length);
	}

	private static boolean checkSimilarKeyword(String[] title, String[] keyword) {
		for (int i = 0; i < title.length; i++) {
			for (int j = 0; j < keyword.length; j++) {
				if (checkSimilar(title[i], keyword[j]) > MINIMUM_SIMILARITY)
					return true;
			}
		}
		return false;
	}

	public List<Book> findBooksByTitle(String[] keywords) {
		return bookList.stream().filter(b -> checkSimilarKeyword(b.getTitle().split(" "), keywords)).parallel()
				.collect(Collectors.toList());
	}

	public Stream<AuthorProfit> streamAuthorProfits() {
		// Translate the Set<Book> into a List<AuthorProfit>, keeping duplicate
		// authors at this stage.
		List<AuthorProfit> authors = bookList.stream()
				.map(b -> (new AuthorProfit(b.getMainAuthor(), b.getLaunchPrice() * b.getCopiesSold())))
				.collect(Collectors.toList());
		// Sort it, so we only can avoid searching the entire list for
		// duplicates
		// This takes us into O(n log n) time.
		Collections.sort(authors);

		// This loop should stay comfortably within O(n).
		Stack<AuthorProfit> agg = new Stack<AuthorProfit>();
		if (authors.size() > 0)
			agg.push(authors.get(0));
		for (int i = 1; i < authors.size(); i++) {
			AuthorProfit current = authors.get(i);
			if (agg.peek().equals(current)) {
				agg.peek().addProfit(current.getTotalProfit());
			} else {
				agg.push(current);
			}
		}
		// The slowest part of this code is O(n log n), so that's our overall
		// time complexity.
		return agg.stream();
	}

}
