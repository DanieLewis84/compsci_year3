import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

public class Main {

	private static final String TEST_FILE_CUSTOM = "10.csv";
	@SuppressWarnings("unused")
	private static final String TEST_FILE_1 = "100.csv";
	private static final String TEST_FILE_2 = "1000.csv";
	@SuppressWarnings("unused")
	private static final String TEST_FILE_3 = "10000.csv";
	@SuppressWarnings("unused")
	private static final String TEST_FILE_4 = "100000.csv";
	@SuppressWarnings("unused")
	private static final String TEST_FILE_5 = "1000000.csv";

	// I've tested up to the TEST_FILE_5 (1,000,000 lines) and the run completed
	// in about 5-6 seconds.
	// For readability, I'd recommend running TEST_FILE_2 or lower.
	private static final String TEST_FILE_SELECTED = TEST_FILE_2;

	// For layout.
	private static final String CONSOLE_BAR = "################################################################################";

	private static void testBookEqual(Book book1, Book book2, boolean expected) {
		boolean equal = book1.equals(book2);
		if (equal == expected) {
			System.out.println(book1 + " and " + book2 + (equal ? " are" : " aren't") + " equal");
		} else {
			System.out.println("ERROR: Unexpected result when checking equality for " + book1 + " and " + book2
					+ (equal ? " are" : " aren't") + " equal, but they " + (expected ? " should" : " shouldn't")
					+ " be.");
		}
	}

	private static void printSectionDivider(String sectionName) {
		System.out.println();
		System.out.println(CONSOLE_BAR.substring(0, sectionName.length() + 4));
		System.out.println("- " + sectionName + " -");
		System.out.println(CONSOLE_BAR.substring(0, sectionName.length() + 4));
	}

	private static void testCheckSimilar(String a, String b) {
		double diff = BookManager.checkSimilar(a, b);
		System.out.printf("Similarity between \"%s\" and \"%s\" = %f\n", a, b, diff);
	}

	private static void printRunTime(String messsage, long startTime) {
		long totalTime = (System.nanoTime() - startTime) / 1000000000;
		System.out.println(messsage + totalTime + " seconds.");
	}

	public static void main(String[] args) {
		long startTime = System.nanoTime();
		// Task 1 tests
		printSectionDivider("Task 1 Tests");
		SimplePair<Integer> zero = new SimplePair<Integer>(0, 0);
		SimplePair<Integer> one = new SimplePair<Integer>(1, 1);
		SimplePair<Integer> secondZero = new SimplePair<Integer>(0, 0);

		zero.testSimpleCompare(secondZero, 0);
		zero.testSimpleCompare(one, -1);
		one.testSimpleCompare(one, 0);
		one.testSimpleCompare(zero, 1);

		/*
		 * // Removed this test, as it won't get past the compiler. try {
		 * SimplePair<String> alsoZero = new SimplePair<String>("0", "0");
		 * zero.testSimpleCompare(alsoZero, 0); System.out.println(
		 * "ERROR: Just compared a SimplePair<Integer> to SimplePair<String>, this shouldn't happen."
		 * ); } catch (Exception e) { System.out.
		 * println("Couldn't compare SimplePair<Integer> to SimplePair<String>."
		 * ); }
		 */
		printRunTime("Task 1 Tests completed at ", startTime);

		// Task 2 tests
		printSectionDivider("Task 2, Part 2 Tests");
		// BOOKTITLE, AUTHOR, ISBN, LAUNCHPRICE, COPIESSOLD
		Book hobbit = new Book("The Hobbit", "J.R.R. Tolkien", "9780048232731", 20.99, 100000000);
		Book hobbitProperTitle = new Book("The Hobbit, or There and Back Again", "J.R.R. Tolkien", "9780048232731",
				20.99, 100000000);
		Book hobbitVer2 = new Book("The Hobbit", "Tolkien, J.R.R.", "9780048232731", 20.99, 100000000);
		Book nullBook = new Book(null, null, null, 0.99, 0);
		testBookEqual(hobbit, hobbit, true);
		testBookEqual(hobbit, hobbitProperTitle, false);
		testBookEqual(hobbitProperTitle, hobbit, false);
		testBookEqual(hobbit, hobbitVer2, true);
		testBookEqual(nullBook, nullBook, true);
		testBookEqual(nullBook, hobbit, false);
		// Should throw a null pointer exception.

		try {
			testBookEqual(null, hobbit, false);
			System.out.println("ERROR: Just successfuly called .equals() on a null, this shouldn't happen.");
		} catch (NullPointerException e) {
			System.out.println("null.equals(book) fails, as expected.");
		}

		// Should fail
		testBookEqual(hobbit, null, false);

		printRunTime("Task 2, Part 2 tests completed at ", startTime);

		printSectionDivider("Task 2, Part 3&4 Tests");
		BookManager bookManagerCustomTests = new BookManager(new File(TEST_FILE_CUSTOM));
		BookManager bookManager = new BookManager(new File(TEST_FILE_SELECTED));

		try {
			bookManagerCustomTests.loadBooks();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Set<Book> bookList = bookManagerCustomTests.getBookList();
		bookList.forEach(a -> System.out.println("Successfuly read a book: " + a));
		System.out.println(String.format("Successfully read %d unique books from first file.",
				bookManagerCustomTests.getBookCount()));
		printRunTime("Task 2, Parts 3 & 4 tests completed at ", startTime);

		// Part 5
		printSectionDivider("Task 2, Part 5 Tests");
		try {
			bookManager.loadBooks();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		String correctISBN = "8-330-89615-1";
		System.out.println("Result for search on correct IBSN \"" + correctISBN + "\" = "
				+ bookManager.getBookByISBN(correctISBN));
		String wrongISBN = "8-330-896151";
		System.out.println(
				"Result for search on wrong IBSN \"" + wrongISBN + "\" = " + bookManager.getBookByISBN(wrongISBN));

		printRunTime("Task 2, Part 5 tests completed at ", startTime);

		// Part 6
		printSectionDivider("Task 2, Part 6 Tests");
		testCheckSimilar("abrakadabra", "kadabra");
		testCheckSimilar("kadabra", "abrakadabra");
		testCheckSimilar("aa", "a");
		testCheckSimilar("ac", "ba");
		testCheckSimilar("Unsettling", "Fleeting");

		printRunTime("Task 2, Part 6 tests completed at ", startTime);

		// Part 7
		printSectionDivider("Task 2, Part 7 Tests");
		String[] keywords = { "Unsettling", "Concern" };
		List<Book> booksFound = bookManager.findBooksByTitle(keywords);
		booksFound.forEach(book -> System.out.println(book));
		printRunTime("Task 2, Part 7 tests completed at ", startTime);

		// Part 8
		printSectionDivider("Task 2, Part 8 Tests");
		Stream<AuthorProfit> stream = bookManager.streamAuthorProfits();
		stream.forEach(a -> System.out.println(a));
		printRunTime("Task 2, Part 8 tests completed at ", startTime);
	}

}
