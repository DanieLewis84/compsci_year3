public class CoffeeApp {

    private CoffeeMachine coffeeMachine;
    private FetchPrices fetchPrices;

    public CoffeeApp(){
    	//Empty constructor
    }
    
    public CoffeeApp(CoffeeMachine coffeeMachine,FetchPrices fetchPrices){
    	this.coffeeMachine=coffeeMachine;
    	this.fetchPrices=fetchPrices;
    }
    
    public void setCoffeeMachine(CoffeeMachine coffeeMachine){
    	this.coffeeMachine=coffeeMachine;
    }
    
    public void prepareCoffee(){
        coffeeMachine.brew();
        fetchPrices.fetchData();
        // Logic to charge the price
        // Logic to dispense the coffee
        System.out.println("Coffee is ready");
    }

	public void setFetchPrices(FetchPrices fetchPrices) {
		this.fetchPrices = fetchPrices;
	}
    
    
}
