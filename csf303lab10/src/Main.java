public class Main {

    public static void main(String[] args) {

        CoffeeApp coffeeApp = new CoffeeApp(new BasicCoffeeMachine(),new FetchPriceDatabase());
        coffeeApp.prepareCoffee();
        
        CoffeeApp setBasedCoffeeApp = new CoffeeApp();
        setBasedCoffeeApp.setCoffeeMachine(new BasicCoffeeMachine());
        setBasedCoffeeApp.setFetchPrices(new FetchPriceDatabase());
        setBasedCoffeeApp.prepareCoffee();
    }
}
