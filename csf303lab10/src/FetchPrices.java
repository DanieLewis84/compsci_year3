import java.util.ArrayList;
import java.util.List;

public interface FetchPrices {

    public List<Object[]> fetchData() ;

}
